import React from "react"
import { graphql } from "gatsby"
import { MDXProvider } from "@mdx-js/react"
import { MDXRenderer } from "gatsby-plugin-mdx"
import { Link } from "gatsby"
import { Divider } from "@mui/material"
import "@fontsource/meera-inimai";
import './post-page-template.scss'
import Sidebar from "../components/Sidebar"
import Hero from "../components/Hero"
import TableOfContents from '../components/TableOfContents';
import Layout from '../Layouts/commonLayout';
import SimpleList from '../components/SimpleList';
import SEO from '../components/SEO';
import {getSrc} from 'gatsby-plugin-image';
import { getFormattedDate } from "../../utils";

const shortcodes = { Link } // Provide common components here

export default function PageTemplate({ data }) {
  var mdx = data.currentPost;
  var {thumbnail, title, publishedDate, category, thumbnailCredit, slug, keywords} = mdx.frontmatter;
  var isRelatedPosts = data.relatedPosts.edges.length ? true : false;

  return (
    <Layout>
      <SEO title={title} keywords={keywords} image={getSrc(thumbnail)} url={category + '/' + slug} />
      <div>
        <div className="wrapper">
          <div className="post-page-grid">
            <div className="content-wrapper">
              <Hero title={title} thumbnailCredit={thumbnailCredit} image={thumbnail} publishedDate={getFormattedDate(publishedDate)} category={category} />
              {mdx.tableOfContents.items[0].items && <TableOfContents items={mdx.tableOfContents.items[0].items} />}
              <MDXProvider components={shortcodes}>
                <MDXRenderer frontmatter={mdx.frontmatter}>{mdx.body}</MDXRenderer>
              </MDXProvider>
              <Divider />
              {isRelatedPosts && <SimpleList posts={data.relatedPosts.edges} heading='மேலும் சில பதிவுகள்'/>} 
            </div>
            <div className="sidebar-wrapper">
              <Sidebar></Sidebar>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query BlogPostQuery($slug: String, $category: String) {
    currentPost : mdx(frontmatter: {slug: {eq: $slug}}) {
      body
      tableOfContents
      frontmatter {
        title
        publishedDate
        thumbnailCredit
        category
        slug
        keywords
        thumbnail {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH)
          }
        }        
      }
    },
    relatedPosts : allMdx(filter: {frontmatter: {category: {eq: $category}, slug: {ne: $slug}}}) {
      edges {
        node {
          timeToRead
          frontmatter {
            slug
            publishedDate
            title
            category
            thumbnail {
              childImageSharp {
                gatsbyImageData
              }
            }
          }
        }
      }
    },
  }
`