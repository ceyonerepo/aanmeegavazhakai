import React from 'react'
import Layout from '../Layouts/commonLayout'
import { graphql } from 'gatsby'
import ListCard from '../components/ListCard'
import { getSrc } from 'gatsby-plugin-image'
import '../styles/categoryPage.scss'
import {getFormattedDate} from '../../utils';

function CategoryPage(props) {
  const { data } = props;
  const posts = data.allMdx.nodes;

  return (
    <Layout>
      <div className='category-title'>
        <h3>{props.pageContext.category.split('-').join(' ').toUpperCase()}</h3>
      </div>
      <div className="categories-container">
        {posts.map(node => {
            var {title, publishedDate, category, slug} = node.frontmatter;
          return <ListCard title={title} 
                           publishedDate={getFormattedDate(publishedDate)} 
                           imageURL={getSrc(node.frontmatter.thumbnail)}
                           slug={slug}
                           category={category}
                />
        })}
      </div>
    </Layout>
  )
}

export default CategoryPage

export const tagsQuery = graphql`
query categoryQuery($category: String!) {
  allMdx(
    filter: { frontmatter: { category: { eq: $category } } }
  ) {
    nodes {
      frontmatter {
        title
        publishedDate
        category
        slug
        thumbnail {
          childImageSharp {
            gatsbyImageData(layout: FULL_WIDTH)
          }
        }
      }
    }
  }
}
`
