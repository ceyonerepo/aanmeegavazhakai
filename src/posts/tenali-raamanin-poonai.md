---
title : 'தெனாலி ராமனின் பூனை'
publishedDate: 'Tue Mar 08 2022 12:21:33 GMT+0530'
category: 'tenali-raman-stories'
slug: 'tenali-raamanin-poonai'
thumbnail: '../thumbnails/cat.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - thenali ramang
    - siru kadhaigal
keywords: 
    - tenali raman
    - tenali raman stories
    - tenali rama krishna
    - tenaliraman
    - tenali ramakrishna stories
    - tenali raman stories in tamil
    - tenali raman short stories
---
 
# தெனாலி ராமன்
தெனாலி ராமன் என்றாலே அனைவருக்கும் நினைவிற்கு வருவது அவருடைய நகைச்சுவை பேச்சு தான். பேச்சு நகைச்சுவையாக இருக்குமே தவிர அவரது சிந்தனையும் திறன்களும் அளவிட முடியாத அளவுக்கு உயர்ந்தவையாகவே இருந்து இருக்கிறது. அவரது எந்த ஒரு நகைச்சுவை  பேச்சும் யாரையும் காயப்பட வைத்தது  இல்லை. மேலும் அவரது ஒவ்வொரு நகைச்சுவை கதைகளிலும் ஏதேனும் ஒரு உள் அர்த்தம் மற்றும் நமது அன்றாட வாழ்க்கையோடு தொடர்புடைய பல சிந்தனைகளை சுமந்தே வந்து உள்ளன. அத்தகைய கதைகளை நாம் படிக்கும் முன்னர் அத்தகைய தெனாலி ராமனை பற்றி இப்பகுதியில் சிறிது தெரிந்து கொள்வோமா!
 
## தெனாலி ராமன் பிறப்பு
 
தெனாலி ராமன் ஆந்திர மாநிலம் கிருஷ்ணா மாவட்டத்தில் கார்லபதி என்கிற கிராமத்தில் ராமையா - லட்சுமி அம்மாள் தம்பதியரின் மகனாக ஏழை பிராமண குடும்பத்தில் பிறந்தவர். தெனாலியில் உள்ள இராமலிங்க சுவாமியின் நினைவாக இராமலிங்கன் என்றே பெயரிடப்பட்டார். இவர் பிறந்து மூன்றாம் நாள் இவருடைய தந்தையார் மரணமடைய குடும்பம் வறுமையில் வாடியது. இவருடைய தாயார் இவரை எடுத்து கொண்டு தெனாலியில் இருந்து அவருடைய சகோதரனுடைய வீட்டுக்கு வந்து சேர்ந்தார். தாய் மாமன் ஆதரவில் தான் இராமலிங்கம் வளர்ந்தார்.
 
## தெனாலியின் கோமாளித் தனங்கள்
 
உரிய பருவத்தில் பள்ளியில் சேர்ந்தாலும் படிப்பில் கவனம் செல்லவில்லை. மற்றவர்களை சிரிக்க வைக்கும் ஆற்றல் அவரிடம் இயற்கையாகவே இருந்தது. அதனால் அகடவிகட கோமாளித் தனங்களில் தான் அவருடைய அறிவும் ஆற்றலும் ஜொலித்தன. கமலா என்கிற பெண்ணை மணந்தார் தெனாலி ராமன்.

## தெனாலி விகடகவி ஆக உயர்ந்தது 
இவர் அரசவை விகடகவியாக உயர்ந்தது குறித்து பல கதைகள் நிலவுகின்றன. 
இவருடைய ஊருக்கு வந்த துறவி ஒருவர் இவருடைய தைரியம் மற்றும் நகைச்சுவை உணர்வில் கவரப்பட்டு காளி தேவியிடம் வரம் பெறத்தக்க மந்திரம் சொல்லி தந்ததாகவும் அதன் அருளால் காளி  தேவியின் தரிசனம் பெற்ற தெனாலி ராமன் அவளையும் தன் நகைச்சுவை அறிவால் சிரிக்க வைத்து அவளாலேயே விகடகவி என்று வாழ்த்தப்பட்டதாகவும் சொல்கிறார்கள்.
அதில் நம்பிக்கை வரப் பெற்ற தெனாலி ராமன் விஜயநகரம் சென்று தன்னுடைய சாமர்த்தியத்தால் அரச தரிசனம் பெற்று தன்  அறிவுக் கூர்மை மற்றும் நகைச்சுவையால் அரசரின் அன்புக்கும் நம்பிக்கைக்கும் பாத்திரமாகி அரசவையில் முக்கிய இடம் பெற்று நல்வாழ்வு வாழ்ந்தார்.
 
## தெனாலி சைவரா? வைணவரா?
இவர் சைவரா வைணவரா என்பதில் சரித்திர ஆசியர்களிடையே குழப்பம் நிலவுகிறது. ஏனென்றால் இவருடைய பெயர் சைவப் பெயர். இவர்  எழுதிய நூலாகிய "பாண்டுரங்க மகாத்மியம்" விஷ்ணுவைப் பற்றியதாக இருப்பதோடு அவருக்கு சமர்ப்பிக்கப்பட்டு உள்ளதாகவும் உள்ளது.
அது தவிர "உத்பாதராதைய சரித்திரம்", "கடிகாச்சல மகாத்மியம்" ஆகிய நூல்களை இயற்றி உள்ளார். முன்னது உத்பாதர் என்கிற துறவியை பற்றியது. பிந்தையது தமிழகம் வேலூருக்கு அருகில் இருக்கும் நரசிம்மர் கோவிலை பற்றியது.
 
## தெனாலியின் பட்டங்கள்
தெனாலி "விகடகவி" என்றும் "குமார பாரதி" என்றும் அனைவராலும் பாராட்டப்பட்டு அன்போடு அழைக்கப்பட்டார்.

## தெனாலியின் சிறப்புகள் 
இந்திய மொழிகளில் இவரைப் பற்றிய பாடக் குறிப்புகள் இல்லாத மொழியே கிடையாது என்னும் அளவுக்குப் பிரபலமானவர்.இவருடைய கதையை "தி அட்வென்சர்ஸ் ஆஃப் தென்னாலி ராமன்" என்கிற பெயரில் கார்ட்டூன் நெட் ஒர்க் தொலைக்காட்சி நிறுவனம் கி.பி.2001இல் படமாக்கப்பது.
 
## தெனாலியின் காலம்
தெனாலி ராமன் வாழ்த்த காலம் கி.பி.1509 முதல் கி.பி.1575 வரை ஆகும். தமிழ் நகைச்சுவை உலகில் மிகவும் புகழ் பெற்ற தெனாலி ராமகிருஷ்ணா என்பவர் விஜயநகரத்தை ஆண்ட கிருஷ்ணா தேவராயரின் அவையை அலங்கரித்த எட்டு அரசவைப்  புலவர்களுள் (அஷ்டதிக்கஜங்கள்) ஒருவர் ஆவார்.
 
இப்படிப்பட்ட புகழ் பெற்ற புலவரான தெனாலி ராமனின் கதைகளில் "தெனாலி ராமனின் பூனை" என்னும் கதையை இப் பகுதியில் நாம் பார்க்கலாம்.


## தெனாலி ராமனின் பூனை

தெனாலி ராமன் அழகான கறுப்புப் பூனை ஒன்றை செல்லமாக வளர்த்து வந்தான். அவன் பூனைக்குத் தன் கையாலேயே சோறு ஊட்டுவான். பால் வார்த்து குடிக்கச் செய்வான். தான் குளிக்கும் போது பூனையையும் குளிப்பாட்டுவான். ஒரு குழந்தையிடம் அன்பு செலுத்துவது போல் தெனாலி ராமன் பூனையிடம் அன்பு செலுத்தி வந்தான்.

ஆனால் தெனாலி ராமன் குடியிருந்த தெருவில் வாழும் மக்கள் அனைவரும் அவனுடைய பூனையை வெறுத்தனர். பூனை தங்கு தடை இல்லாமல் தெரு முழுவதும் சுற்றி வந்தது. யார் வீட்டில் வேண்டுமானாலும் புகுந்து விடும்.

பூனையின் முகத்தில் விழித்தால் கேட்ட சகுனம் என்ற ஒரு நம்பிக்கை நம் மக்களுக்கு உண்டு அல்லவா? தெனாலி ராமனின் பூனை ஏதாவது ஒரு வழியில் யார் கண்ணிலாவது அன்றாடம் தென்பட்டு விடும்.

அந்தச் சந்தர்ப்பத்தில் அவர்கள் செல்லும் காரியம் வெற்றி அடைந்தால் பூனையை பற்றி யாரும் ஏதும் சொல்ல மாட்டார்கள். சென்ற காரியம் தோல்வியடைந்து விட்டாலோ இன்று காலையில் தெனாலி ராமன் வீட்டுப் பூனையின் முகத்தில் விழித்தேன். அதனால் தான் காரியம் கேட்டு விட்டது என்று கூறுவார்கள். இந்தக் காரணத்தால் தெனாலி ராமன் வீட்டுப் பூனை ஒரு கேட்ட சகுனம் என்று எங்கு பார்த்தாலும் பேச்சு அடிபடத் தொடங்கியது.

தெனாலி ராமன் பூனை பற்றி மன்னரிடமும் பலர் புகார் சொல்லத் தலைப்பட்டனர். தெனாலி ராமன் வீட்டிற்கு எதிர் வீட்டில் இருக்கும் கருணானந்தர் என்ற ஒரு பெரிய வியாபாரி ஒரு நாள் அதிகாலையில் உறங்கியெழுந்து தெருவிற்கு வந்த போது தெனாலி ராமனின் வீட்டுப் பூனை அவர் கண்ணில் பட்டு விட்டது.

பூனை குறுக்கிட்டு விட்டதே இன்று என்ன விபரீதம் நடக்கப் போகிறதோ என்று கவலைப் பட்டர் கருணானந்தர். அவர் எதிர்பார்த்தது போலவே குளிக்கச் செல்லும் போது வழுக்கி விழுந்து விட்டார். அதன் காரணமாக ஒரு கால் சுளுக்கிக் கொண்டு விட்டது.

சுளுக்கு மிகவும் சாமானியமாகத் தான் இருந்தது. என்றாலும் சந்தர்ப்பதை பயன்படுத்திக் கொண்டு பூனை வளர்ப்பதற்காக தெனாலி ராமனுக்கு எதாவது தண்டனை வாங்கி கொடுக்க வேண்டும் என்று கருணாநந்தன் தீர்மானித்தார்.

உடனே அவர் மன்னரிடம் சென்று இன்று காலையில் நான் தெனாலி ராமனின் பூனை முகத்தில் விழித்தாள் கீழே விழுந்து அடிபட்டு கால் சுளுக்கிக் கொண்டு விட்டது. இவ்வாறு நான் சங்கடப் படுவதற்கு தெனாலி ராமனின் பூனை காரணமாக இருந்ததால் அதைக் கொன்று விட வேண்டும். தெனாலி ராமனுக்கும் சிறை தண்டனை விதிக்க வேண்டும் என்று வழக்குத் தொடுத்தார்.

அன்று மாலை அந்த வழக்குத் தொடர்பாக விசாரணை நடக்கும் என்று  கூறி மன்னர் கருணானந்தரையும் தெனாலி ராமனையும் மாலையில் அரசவைக்கு வரச் சொல்லி இருந்தார்.

கருணானந்தர் வழக்கில் தனது தரப்பு விஷயங்களை மன்னரிடம் எடுத்துச் சொல்லிக் கொண்டு இருந்தார். அப்போது காவலர்கள் ஒரு முரட்டு மனிதனைக் கட்டிப்பிடித்து இழுத்துக் கொண்டு வந்தனர். என்ன சமாச்சாரம் என்று கேட்டார் மன்னர்.

இரண்டு நாட்களுக்கு முன்னர் இந்தக் கருணானந்தர் வீட்டில் விலை மதிப்பு மிக்க பண்டங்களையும் பணத்தையும் கொள்ளையடித்த கொள்ளைக்காரன் இவன். இரண்டு நாட்களாக இவனைத் தேடி கொண்டு இருந்தோம். சற்று முன் தான் பிடிபட்டான். கொள்ளை அடிக்கப்பட்ட பொருட்கள் அனைத்தும் மீட்கப்பட்டு விட்டது என்று கூறி காவலர்கள் கொள்ளையனிடம் கைப்பற்றிய தங்க வைர நகைகளையும் பெருந்தொகையான பணத்தையும் அரசர் முன் வைத்தனர்.

மன்னர் கொள்ளைக்காரனிடம் இருந்து மீட்கப்பட்ட பொருள்களைக் கருணாநந்தரிடம் ஒப்படைக்கச் சொல்லி விட்டு கொள்ளைக்காரனுக்குச் சிறை தண்டனை விதித்தார். கொள்ளைக்காரனைக் காவலர்கள் சிறைக் கூடத்திற்கு இழுத்துச் சென்ற பிறகு மன்னர் கருணானந்தர் வழக்கைத் தொடர்ந்து விசாரித்தார். கருணானந்தர் தமது தரப்பு வாக்கு மூலத்தைச் சொல்லி முடித்தார்.

மன்னர் தெனாலி ராமனை அழைத்து அவன் தரப்பு விஷயங்களை சொல்லுமாறு உத்தரவு இட்டார். தெனாலி ராமன் மன்னருக்கு வணக்கம் செலுத்தி விட்டு கருணானந்தரை நோக்கி, கருணானந்தர் அவர்களே, என்னுடைய பூனையின் முகத்தில் விழித்ததனால் தங்களுக்கு கால் சுளுக்கிக் கொண்டது என்பது தங்களுடைய வாதம் அல்லவா? ஆமாம் என்று பதில் அளித்தார் கருணானந்தர். 

அதாவது இன்று காலையில் என் பூனையின் முகத்தில் விழித்து விட்டதால் இன்று நடைபெறும் நிகழ்ச்சிகளுக்கு என் பூனை தான் காரணம் அல்லவா? ஆமாம் என்று கூறினார் கருணானந்தர்.
அதனால் இன்று நிகழும் நிகழ்ச்சிகளுக்கு என் பூனையோ அல்லது நானோ பொறுப்பு ஏற்க வேண்டும் என்று கூறுகிறீர்கள். அப்படித்தானே? என்றான் தெனாலி ராமன். ஆம் அப்படிதான் என்று கூறினார் கருணானந்தர். அதாவது இன்று உங்களால் சுளுக்கியதற்கு என் பூபனையும் நானும் பொறுப்பு ஏற்க வேண்டும். ஆமாம் என்று கூறினார் கருணானந்தர். 

அப்படியானால் இன்று என் பூனை முகத்தில் விழித்த உங்களுக்கு களவு போய் கை நழுவி விட்ட பெருஞ்செல்வம் திரும்பக் கிடைத்து இருக்கிறது. நியாயமாக அந்தக் கொள்ளை போய் மீண்ட செல்வம் முழுவதும் என் பூனைக்குத் தான் சொந்தம். நியாயப்படி இப்படித்தானே ஆக வேண்டும்? என்று அழுத்தம் திருத்தமான குரலில் கேட்டான் தெனாலி ராமன். கருணானந்தர் திகைத்தார் திணறினார்.

தெனாலி ராமன் மன்னரை நோக்கி, மன்னர் பெருமானே! காலையில் கருணானந்தர் என் பூனையின் முகத்தில் விழித்த காரத்ணத்தால் ஏற்பட்ட அவருடைய கால் சுளுக்கிற்கு நான் பொறுப்பு ஏற்கிறேன். அது தொடர்பாக எனக்கு என்ன தண்டனை கொடுத்தாலும் ஏற்றுக் கொள்கிறேன். அதே போல் என் பூனையின் முகத்தில் விழித்த காரணத்தால் திரும்ப அவருக்கு கிடைத்த கொள்ளை போய் மீண்ட செல்வத்தை எனக்கே அளித்திட வேண்டுகிறேன். கருணானந்தர் வாதப்படி நியாயப்படி அது எனக்கே சொந்தம் என்றான். 

என்ன கருணானந்தரே! தெனாலி ராமன் கூறுவதற்கு என்ன பதில் சொல்கிறீர்கள் என்று கேட்டார் மன்னர். தெனாலி ராமன் விஷயத்தில் சமரசமாகப் போய் விடுவது என்ற தீர்மானத்திற்கு வந்து இருக்கிறேன். மன்னர் அவர்களே! எனக்கு ஏற்பட்டிருக்கும் சுளுக்கு அற்பமான விசயமாதலால் நானே அதைக் குணப்படுத்திக் கொள்கிறேன் என்று கூறினார் கருணானந்தர்.

கருணானந்தர் சமரசத்துக்கு வந்தாலும் நான் வர வேண்டும் அல்லவா? வழக்கு என்று வந்து விட்ட பிறகு தங்கள் தீர்ப்பு தான் முக்கியமே தவிர சமரசமல்ல என்று அழுத்தம் திருத்தமான குரலில் கூறினான் தெனாலி ராமன்.

மன்னர் கூறினார் கருணானந்தர் அவர்களே! பூனையின் சகுனம் என்று பார்க்க ஆரம்பித்து விட்டால் கெட்டதை மட்டுமே ஏன் கவனிக்க வேண்டும் என்று விளங்கவில்லை. பூனையைப் பார்த்த அன்று நிகழும் நன்மைகளை ஏன் கணக்கில் எடுத்துக் கொள்வதில்லை. அது முறையல்ல. அதனால் இந்த மாதிரியான வீண் விவகாரங்களை பெரிது படுத்தாமல் தெனாலி ராமன் போன்ற கவுரவமான மனிதர்களுடன் ஒத்து உறவாட முயலுவது தான் நல்லது என்று யோசனை கூறி இருவரையும் சமரசப் படுத்தி அனுப்பி வைத்தார் மன்னர்.

## கதையின் நீதி:

நமக்கு தொல்லை தரும் நோக்கத்தோடு எவர் சுற்றி கொண்டு இருக்கிறாரோ அவருக்கு தெனாலி ராமன் நல்ல பதில் கொடுத்தார். இதே போல் நம்மை வேண்டும் என்றே சீண்டுபவர்களை நாம் இப்படி மாட்டி விடுவதில் எந்த ஒரு தவறும் இல்லை.


