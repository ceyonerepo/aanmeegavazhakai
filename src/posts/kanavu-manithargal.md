---
title : 'மனிதர்களை தொடர்பு படுத்தும் கனவுகள்'
publishedDate: "Thu Mar 03 2022 15:40:54 GMT+0530"
category: 'kanavu-palangal'
slug: 'kanavu-manithargal-random'
thumbnail: '../thumbnails/kanavu-manithargal.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - kanavu palangal
keywords:
    - kanavu palangal
    - kanavu palangal in tamil
---

# கனவு
 
கனவு என்பதற்கு தூக்கத்தில் மனத் திரையில் எழும் காட்சி அல்லது சொப்பனம் என்று பொருள் படும். கனவு என்பது ஒருவர் தூங்கும் பொது அவரது மனதில் எழும் மனப் படிமங்கள், காட்சிகள், ஓசைகள், உணர்வுகள், நிகழ்வுகளைக் குறிக்கிறது. ஒருவர் கனவு காணும் பொழுது அவரது கண்களில் அசைவுகள் காணப்படுவது அவதானிக்கப்பட்டு உள்ளது. கனவு என்றால் என்ன என்பது தொடர்பாக ஒரு முழுமையான அறிவியல் புரிதல் இன்னும் இல்லை. மூலையில் உள்ள நினைவு குறிப்புகளை ஒன்றோடு ஒன்று தொடர்பு படுத்தும் செல்பாட்டின் விளைவாக இருக்கலாம் என்றும் கருதப்படுகிறது. இது அணைத்து பாலூட்டிகளிலும் ஏற்படக்கூடிய விளைவு ஆகும். ஆர்மமடில்லோக்களில் இது மிகவும் மிகுதியாக இருப்பதாக அறியப்பட்டு உள்ளது.
 
## கனவுகளின் தன்மை
 
நாம் காணும் கனவுகள்  நல்ல கனவுகளாக இருந்தால் மகிழ்ச்சி. ஆனால் சில நேரங்களில் கேட்ட கனவுகளும் நமக்கு தோன்றலாம். அவ்வாறு நாம் காணும் கனவு கேட்ட கனவாக இருந்தால் அதன் கடுமையினை குறைக்க பரிகாரம் செய்யலாம். கேட்ட கனவு கண்டவர்கள் அதை பற்றி யாரிடமும் சொல்ல கூடாது. அன்று பசுவுக்கு புல், பழம், கீரை கொடுக்க வேண்டும். அதன் முன் நின்று தான் கண்ட கனவினை மனசுக்குள் சொல்ல வேண்டும்.
 
## கனவுகளும் அது நமக்கு தோன்றும் நேரமும்
 
நாம் காணும் ஒவ்வொரு கனவுகளுக்கும் ஒவ்வொரு பலன்கள்  உண்டு. அதிலும், நாம் கண்டா கனவுகளின் பொறுத்து அதன் பலன்கள்  அமையும்.நாம் காணும் கனவின் நேரத்தின் அடிப்படையில் அந்த கனவு எவ்வளவு காலத்தில் பலிக்கும் என்று பஞ்சாங்க சாஸ்திரங்கள் கூறுகின்றன. அதை இப்போது பாப்போம்.
 
- இரவில் மாலை 6 மணி முதல் 8.24 மணிக்குள் கண்ட கனவு ஒரு வருடத்தில் பலிக்கும்.
- இரவு 8.24 மணி முதல் 10.48 மணிக்குள் கண்ட கனவு 3 மாதத்தில் பலிக்கும்.
- இரவு 10.48 மணி முதல் 1.12 மணிக்குள் கண்ட கனவு 1 மாதத்தில் பலிக்கும்.
- இரவு 1.12 மணி முதல் 3.36 மணிக்குள் கண்ட கனவு 10 தினங்களில் பலிக்கும்.
- இரவு 3.36 மணி முதல் 6 மணிக்குள் கண்ட கனவு உடனடியாக பலிக்கும்.
- பகல் கனவு காண்பவர்களும் கூட உண்டு. ஆனால் பகலில் காணும் கனவுகள் பலிப்பதில்லை.
 
## கனவுகளின் பலன்
 
பொதுவாக எல்லாக் கனவுகளுக்கும் பலன் உண்டு என்று சொல்லி விட முடியாது. ஆனால் குறிப்பிட்ட சில நேரங்களில், நாம் ஆழ்ந்த உரக்க நிலையில் இருக்கும் பொது வரும் கனவுகளுக்கு நிச்சயம் பலன் உண்டு. அந்த வகையில் நமது கனவில் வரும் விஷயங்களுக்கு பலன்கள் என்ன என்பதை இங்கு தெரிந்து கொள்ளலாம். பின்வரும் அனைத்தும் பல்வேறுபட்ட அதாவது இரும்பு, கடல், கண்டங்கள் போன்றவை தோன்றும் கனவுகளுக்கான பலன்களை பார்க்கலாம்.
 
 
## இறந்தவர்கள் கனவில் வந்தால்:

நீங்கள் இறந்து போன்றவர்களுடன் பேசுவது போல கனவு வந்தது என்றால் உங்களுக்கு, பெயரும் மிக சிறப்பான புகழும் உண்டாகும் என்பது பொருள். ஒருவருக்கு அவர்களின் இறந்து போன தாய் தந்தையர் கனவில் தோன்றினார்கள் என்றால் கனவு கொண்டவருக்கு வர இருக்கும் ஆபத்து அல்லது இடையூறைச் சுட்டிக்காட்டி எச்சரிக்கை செய்ய வந்து இருக்கிறார்கள் என்று அர்த்தம். இது பலரின் வாழ்வில் ஏற்படும் அனுபவ உண்மை ஆகும்.

தான் இறந்து விட்டது போல் ஒருவருக்கு கனவு வருமானால் அது நன்மைகளையே குறிப்பிடும் என எடுத்துக் கொள்ளலாம். சுக வாழ்க்கை உண்டாகும். உங்களின் உறவினர் ஒருவர் இறந்து விட்டது போல் கனவு வந்தால் அவருடைய துன்பங்கள் துயரங்கள்  வெகு சீக்கிரம் நீங்கும் என்பதை குறிக்கிறது. உங்கள் நண்பன் ஒருவன் இறந்து போனது போல் கனவு வந்தால் வெகு விரைவில் ஏதேனும் நற்செய்தி ஒன்று வரும் என்பது பொருள்.

ஒரு நபரின் குழந்தைகள் இறப்பது போல் கனவு வந்தால் அவருக்கு வர இருக்கின்ற பேராபத்து ஒன்றைக் குறிப்பிடும். தன்னுடைய மனைவி இறந்து விட்டார் போல கனவு வருமானால் மனைவிக்கு இரட்டைக் குழந்தை பிறக்க இருப்பதைக் குறிக்கும். ஒருவரின் இறந்து போன மனைவி, விண்ணுலகில் மகிழ்ச்சியாக இருப்பது போல் கனவு வந்தால் அவரின் வாழ்க்கை நிம்மதியாக அமையும். அதே நேரம், மறைந்த மனைவியின் முகம் துயரம் தோய்ந்ததாக இருப்பின் வாழ்க்கை நிலை அற்றதாக மாறும்.

## பிறரை அடிப்பது போல் கனவு வந்தால்:

நாம் பிறரை அடிப்பது போல் கனவு வந்தால் நண்பர்களால் புகழப்படும் நிலை ஏற்படும். மேலும் புதிய நண்பர்கள் கிடைப்பார்கள். புகழ் பன்மடங்கு பெருகும். தான் அடிபட்டு காயம் அடைந்து இருப்பது போல் கனவு வந்தால் தன அபிவிருத்தி உண்டாகும். எனினும் கத்தி, துப்பாக்கியால் சுடப்பட்டு காயமடைந்ததாக கனவு காண்பது நன்மையான பலன் தராது. உங்களுக்கு பழி ஏதேனும் வந்து சேரும்.

## பிரபலமானோர் கனவில் வந்தால்:

பிரதமர், ஜனாதிபதி போன்றோர்களுடன் அறிமுகம் ஆவது போல் கனவு கண்டால் சமூகத்தில் உங்களுக்கு அந்தஸ்தும் மதிப்பும் உண்டாகும். மணமாகாத இளம் பெண்கள் மேற்சொன்ன படி கனவு கண்டால், அவளை மணம் முடிக்க போகும் வருங்கால கணவன் அப்பெண்ணின் குடும்பத்தை விட பன்மடங்கு வசதி மிக்கவனாக இருப்பான் என கொள்ளலாம். அரச குடும்பத்தாருடன் பழகுவது போன்ற கனவு வந்தால், உங்களின் நண்பர்கள் மூலமாக பண உதவி கிடைக்கும்.

## அப்சரஸ் பெண்கள் கனவில் வந்தால்:

அப்சரஸ் எனப்படும் தேவலோகப் பெண்களை ஆண்கள் தங்களின் கனவில் கண்டால் எதிர்பாராத நன்மைகள் அவர்களுக்கு உண்டாகும். திருமணமாகாத பெண்களின் கனவில் வந்தால் விரைவில் அப்பெண்களுக்கு திருமணம் நிகழும். திருமணமான பெண்கள் கனவில் வந்தால் மிகுந்த பொருள் வரவு உண்டு.

## அழகற்ற பெண் கனவில் வந்தால்:

அழகு இல்லாத பெண் ஒருத்தியை, திருமணமாகாத ஒரு ஆண் மகன் கனவில் காணும் பட்சத்தில், அதற்கு நேர்மாறான பலனாக மிகவும் அழகான பெண் அந்த ஆணுக்கு மனைவியாக அமைவாள்.

## அதிசயமானவர் கனவில் வந்தால்:

பார்ப்பதற்கு விந்தையான மனிதன் அல்லது நூதனப் பொருள்கள் உங்கள் கனவில் வந்தால் எதிர் வரும் தீமையைச் சுட்டிக் காட்டும் அறிகுறி ஆகும். நம்பிக்கை மோசடி, ஏமாற்றம் போன்றவை ஏற்பட வாய்ப்பு அதிகம் ஆகும்.

## ஆசிரியர் கனவில் வந்தால்:

உங்களுக்கு கல்வி போதிக்கும் ஆசிரியர்களில் எவரேனும் ஒருவரை கனவில் காணும் பட்சத்தில் உங்கள் வாழ்வில் வளங்கள் அனைத்தும் அமோகமாகப் பெருகும். பண வரவும் அதிகரிக்கும்.

## எதிரிகள் கனவில் வந்தால்:

உங்கள் கனவில் உங்கள் எதிரிகளைக் கண்டால் மிகவும் எச்சரிக்கையுடன் நடந்து கொள்ள வேண்டும். கனவு காண்பவருக்கு எதிராக சாதி திட்டங்கள் தயாரிக்கப்பட்டு விட்டன என்பதை இந்த கனவு குறிப்பிடும்.

## முடிவுரை:

மனிதர்களாகிய நாம் அனைவரும் ஏதோ ஒரு சந்தர்ப்பத்தில் ஏதேனும் நிகழ்வுகளை பற்றி கனவுகளை நாம் அடிக்கடி சந்திக்க வேண்டிய நிலை ஏற்படும். எனவே நாம் காணும் கனவு நல்லதோ அல்லது கெட்டதோ அதை பற்றி நாம் அதிகம் ஆராயாமல் அதற்குரிய பலன்களையோ அல்லது அதனால் நாம் எவ்வாறு எச்சரிக்கையோடு இருக்க வேண்டும் என்பதை கற்றுக் கொள்ள வேண்டும். கனவு என்பது அனைவரின் வாழ்விலும் பின்னி பிணைந்த ஒரு விஷயம் ஆகும். எனவே நாம் அனைவரும் இனிமையான அல்லது நல்ல பலன்களை தரும் கனவுகளோடு நம் வாழ்வை பயணிப்போம்.
