---
title : 'உள்ளங்கைகளில் ஏன் ரோமம் முளைப்பதில்லை?'
publishedDate: 'Wed Apr 06 2022 13:32:56 GMT+0530'
category: 'akbar-beerbaal-stories'
slug: 'ullankaigalil-aen-romam-mulaippathillai'
thumbnail: '../thumbnails/akbar-beerbaal.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - akbar beerbal stores
    - siru kadhaigal
    - moral stories
    - naneri kathaigal
    - tamil short stores
    - stories in tamil
keywords: 
    - akbar beerbal stories
    - akbar beerbal
    - beerbal
    - tamil tories
    - moral stories in tamil
    - tamil kathaigal
---
 
# அக்பர் பீர்பால் கதைகள் - தமிழ் நன்னெறிக் கதைகள்

# உள்ளங்கைகளில் ஏன் ரோமம் முளைப்பதில்லை?

## ஜலாலுத்தீன்  முகம்மது அக்பர்:

மொகலாயப் பேரரசர்களில் பெருஞ் சிறப்பு வாய்ந்தவர் ஜலாலுத்தீன் முகம்மது அக்பர் ஆவார். அவருடைய பாட்டனார் பாபர் என்பவர் இப்ராஹிம் லோடியை வென்று மொகலாயப் பேரரசை 1526 - ல் தோற்றுவித்தார். அவர் நான்கு ஆண்டு காலம் தான் அரசாட்சி செய்தார். அதற்குப் பிறகு அக்பருக்குத் தந்தையான உமாயூன் என்பவர் 1530 ல் அரியணை ஏறினார். பத்தாண்டு காலம் தொடர்ந்தார் போல் அரசாட்சி நடத்துவதற்கு அவர் பட்டபாடு கொஞ்ச நஞ்சமல்ல. அந்தச் சமயத்தில் சூர் வம்சத்தைச் சேர்ந்த ஷெர்கான் என்பவன் உமாயூனுக்கு விரோதமாகக் கிளம்பி அவரை முறியடித்து விட்டு அரசைக் கைப்பற்றிக் கொண்டான்.

ஷெர்கானால் தோற்கடிக்கப்பட்ட உமாயூன் நாடு நகரம், சொத்து சுகம் அனைத்தையும் இழந்து, பகைவர்க்கு அஞ்சி அமர்கோட் ராணாவிடம் தஞ்சம் புகுந்தார். அங்கு தான் ஜலாலுத்தீன் முகம்மது அக்பர் 1542 ல் பிறந்தார்.

உமாயூன் பதினைந்து ஆண்டுகளுக்குப் பிறகு பெரும் படையுடன் 1555 ல் ஷெர்ஷாவின் மேல் படை எடுத்தார். அப்பொழுது அக்பருக்கு வயது பதின்மூன்று. உமாயூன் தன்னுடைய படையை இரு பிரிவுகளாகப் பிரித்து ஒரு பிரிவுக்கு சிறுவன் அக்பரையும் அவனுக்குத் துணையாக தளபதி பைராம்கானையும் அனுப்பினார். இம்முறை உமாயூன் வெற்றிவாகை சூடினார். இழந்த நாட்டை திரும்பப் பெற்று மீண்டும் 1555 ல் பட்டம் சூட்டிக் கொண்டார். அதற்கு பிறகு ஓராண்டு காலம் தான் உமாயூன் உயிரோடு வாழ்ந்தார். அவர் இறந்ததும்  பதினான்கு வயதுச் சிறுவனான அக்பர் 1556 ல் அரியணை ஏறினார். அவருக்குத் துணையாக தளபதி பைராம்கான்  ஆரம்பத்தில் துணை புரிந்தார்.

அக்பர் தன்னுடைய ஆட்சிக் காலத்தின் போது இந்து முஸ்லீம் ஒற்றுமைக்காக மிகவும் பாடுபட்டார். இந்துக்கள் பால் சக்கரவர்த்தி அன்பு காட்டியதற்கு காரணம் பீர்பால் தான் என்று நினைக்கத் தோன்றுகிறது. 

பீர்பாலுடன் சேர்த்து மொத்தம் ஒன்பது அறிஞர்கள் அக்பரின் அரசவையை அலங்கரித்தார்கள். இவர்களை "நவரத்தினங்கள்" என்று குறிப்பிடுவார்கள். ராஜா பீர்பால், ராஜா மான்சிங், ராஜா தேடர்மால், ஹகீம் ஹிமாம், முல்லா தோபியாஸா, பைஜி, அபுல்பசல், ரஹீம், மியான் தான்சேன் முதலியவர்கள் தான் இந்த நவரத்தினங்கள்.

மதங்களினால் தான் மக்கள் வேற்றுமை அடைந்து ஒருவருக்கு ஒருவர் வீணே சச்சரவு இட்டு கொள்கிறார்கள் என்று கருதிய அக்பர் அனைவரையும் ஒன்று சேர்க்கக் கருதி ஒரு புதிய மதத்தை தோற்றுவித்தார்.  இப்புதிய மதத்திற்கு "தீன்இலாஹி" என்பதாகும். 

அக்பர் தம்முடைய 63 வது வயதில் 1605 ல் "தான் பெற்ற மகனே தனக்கு விரோதியாக மாறி விட்டானே" என்ற மன வருத்தத்துடன் உயிர் துறந்தார். அவருடைய விருப்பத்திற்கிணங்க அவர் இறந்த போது எந்த மதத்தினருக்கு உரிய துதிப் பாடல்களும் அவருக்கு ஓதப்படவில்லை. அவருடைய சடலம் ஆடம்பரம் ஏதுமின்றி கல்லறையில் புதைக்கப் பெற்றது.

## ராஜா பீர்பால் 

ராஜா பீர்பாலுடைய இயற்பெயர் மகேஷ்தாஸ் என்பது ஆகும். சிலர் பிரம்மதாஸ் என்றும் குறிப்பிடுகிறார்கள். பீர்பால் என்றால் வீரர்களில் பலம் பொருந்தியவன் என்பதாகும். இந்தப் பெயரை இவருக்குச் சூட்டியவரும் அக்பர் சக்கரவர்த்தி தான்.

பீர்பால் கோதாவரி நதிக் கரையிலுள்ள மார்ஜால் என்ற ஊரில் பிறந்ததாகச் சிலர் கூறுகிறார்கள். அப்துல் காதர் பதௌனி என்ற சரித்திராசிரியர் இவர் கால்பி என்ற ஊரில், ஓர் ஏழைப் பிராமணக் குடும்பத்தில் 1541 ல் பிறந்ததாகக் கூறுகிறார்.

பீர்பால் இளமைப் பருவத்திலேயே கவிதைகள் புனைந்து பாடுவதில் வல்லவராக விளங்கினார். காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதரின் திருமகளை மணம் செய்து கொண்ட பிறகு தான் ஓரளவு சுகமாக வாழ்ந்து வந்தார். மாமனார் இறந்ததும் அவரே சமஸ்தானத்து தலைமைப் பண்டிதரானார். அவர் அறிவுத்திறனை அறிந்த மக்கள் அவரைப் புகழ்ந்தனர். புதிய பண்டிதரின் புகழ் சமஸ்தானமெங்கும் பரவியது.

காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதராக இருந்த பீர்பால் என்ன காரணத்தினாலோ அந்தப் பதவியைத் துறந்து விட்டு ஆக்ராவுக்கு வந்து சேர்ந்தார். அங்கு ராம்சந்த் என்ற ஒரு செல்வரின் உதவியால் புரோகிதத் தொழிலைச் செய்து வந்தார்.

பீர்பால் ஆக்ராவில் இருக்கும் போது அவரது நகைச்சுவைப் பேச்சையும், அறிவுத் திறனையும் கண்ட ஆக்ரா மக்கள் அவரைப் புகழ்ந்தனர். இது அக்பரது செவிகளிலும் விழுந்தது. அக்பரது இந்துக்களிடம் அளவு கடந்த அன்பு. அக்பருடைய அரசவையில் கல்வியில் சிறந்த அறிவாளிகள் பலர் இருந்தனர். ஆனால் நகைச்சுவையாகப் பேசிச் சிரிக்க வைக்க ஒரு விகடகவி இல்லை.

ஒரு நாள் ஆக்ரா வீதியில் பாட்டுப் பாடி பிச்சை எடுத்துக்  கொண்டு இருந்த ஏழை பிராமணனான பீர்பாலை அக்பர் பார்க்க நேர்ந்தது. பீர்பாலின் பாடலும், வேடிக்கைப் பேச்சும் அக்பரின் மனதைக் கவர்ந்து விட்டன. அப்போதே பீர்பாலுடன் அவர் நட்புக் கொண்டு விட்டார். அதன் பின்னர் அவர் அக்பரிடம் வேலைக்கு அமர்ந்ததே ஒரு வேடிக்கையான கதை.

அரசவையில் பீர்பாலுடைய விகடப் பேச்சுகளும், நகைச்சுவையான உரையாடல்களும் அக்பரின் உள்ளத்தைப் பெரிதும் கவர்ந்தன. இதன் காரணமாக அக்பரின் அரசவையில் பீர்பால் சிறப்பு மிக்க ஓர் இடத்தைப் பெற்று விட்டார்.

# உள்ளங்கைகளில் ஏன் ரோமம் முளைப்பதில்லை?

பீர்பால், ஏன் உள்ளங்கைகளில் மட்டும் ரோமமே இல்லையே! இதன் காரணம் என்னவோ? என்று ஒரு முறை அக்பர் பீர்பாலைப் பார்த்துக் கேட்டார். அரசே, என்னைப் போன்ற எளியவர்களுக்கும், பண்டிதர்களுக்கும், அறிஞர்களுக்கும் தாங்கள் ஓய்வு ஒழிச்சல் இல்லாமல் இரண்டு கைகளாலும் அள்ளி அள்ளிக் கொடுப்பதால் தங்கள் உள்ளங்கைகளில் ரோமமே முளைப்பதில்லை என்றார் பீர்பால்.

பீர்பால் சொன்ன காரணம் பொருத்தமாக இல்லாது இருந்த போதிலும் சக்கவர்த்தி கேட்ட குயுக்தியான கேள்விக்கு அது ஒரு சூடு போல இருந்தது. இருந்தாலும் அரசர் தம் தோல்வியை ஒப்புக் கொள்ளாதவர் போல் சற்று நேரம் யோசித்து விட்டு மீண்டும் கேட்டார்.

அது சரி பீர்பால், எல்லோருக்கும் அள்ளி அள்ளிக் கொடுப்பதால் என் உள்ளங்கையில் ரோமம் முளைக்கவில்லை; ஆனால் உன் உன்னங்கைகளிலும் ரோமம் முளைக்கவில்லையே, இது ஏன்? என்றார் அக்பர்.

அரசே, தாங்கள் தரும் பரிசுகளைப் பெற்றுக் பெற்று இவ்வடியேனுடைய உள்ளங்கைகள் தேய்ந்து போய் ரோமமே முளைப்பதில்லை என்று சற்றும் தயங்காமல் பதில் கூறினார் பீர்பால்.

என்ன இது, இதற்கும் ஏதோ ஒரு காரணம் கூறி பீர்பால் தப்பித்துக் கொண்டாரே! இவரை எப்படி மடக்குவது? என்று சிந்தனை செய்த வண்ணம் அரசவையில் பார்வையை ஓட்டினார் அக்பர்.

அரசவையில் இருந்தவர்கள் அடுத்த படியாகச் சக்கரவர்த்தி என்ன சொல்லப் போகிறாரோ என்று அவர் முகத்தையே பார்த்துக் கொண்டு இருந்தனர். அவர்கள் எல்லோரும் பீர்பாலிடம் பொறாமை கொண்டவர்கள். எப்படியாவது பீர்பால் மாமன்னரின் கேள்விக்கு விடை தெரியாமல் விழித்து அவமானப் பட வேண்டும் என்று நினைப்பவர்கள்.

அவர்களை பார்த்த உடனே அக்பர், "அது சரி பீர்பால், வரையாது வழங்குவதால் என் உள்ளங்கைகளில் ரோமம் முளைப்பது இல்லை. ஓயாது வாங்குவதால் உன் உள்ளங்கைகளில் ரோமம் முளைப்பது இல்லை. அப்படியானால், இதோ அரசவையில் அமர்ந்து இருக்கிறார்களே, இவர்களுடைய உள்ளங்கைகளிலும் ரோமம் முளைக்க  வில்லையே, இது ஏன்?" என்றார்.

இந்தக் கேள்விக்கு பீர்பால் கண்டிப்பாக பதில் சொல்ல முடியாது என்று பொறாமைக்காரர்கள் எண்ணி மகிழ்ந்து கொண்டு இருக்கும்  போதே,

"அரசே, ஒவ்வொரு முறையும் தாங்கள் வரையாது வழங்குவதையும், நான் தவறாது தங்களிடம் பரிசுகள் பெறுவதையும் கண்டு இவர்கள் வயிறெரிந்து தங்கள் உள்ளங்கைகளை அழுத்தி தேய்த்துக் கொள்வதால் இவர்களுடைய உள்ளங்கைகளிலும் ரோமம் முளைப்பது இல்லை" என்றார் பீர்பால். இதைக் கேட்டு பொறாமைக்காரர்கள் அனைவரும் தலை கவிழ்ந்தனர்.
