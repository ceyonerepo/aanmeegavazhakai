---
title : 'அதிசய குதிரை'
publishedDate: 'Thu Jan 27 2022 00:03:25 GMT+0530'
category: 'tenali-raman-stories'
slug: 'athisaya-kuthirai'
thumbnailCredit: 'Photo by Tatiana from pexels.com'
thumbnail: '../thumbnails/athisaya-kuthirai.jpg'
tags: 
    - thenali raman
    - siru kadhaigal
keywords: 
    - tenali raman
    - tenali raman stories
    - tenali rama krishna
    - tenaliraman
    - tenali ramakrishna stories
    - tenali raman stories in tamil
    - tenali raman short stories
---
 
# தெனாலி ராமன்

தெனாலி ராமன் என்றாலே அனைவருக்கும் நினைவிற்கு வருவது அவருடைய நகைச்சுவை பேச்சு தான். பேச்சு நகைச்சுவையாக இருக்குமே தவிர அவரது சிந்தனையும் திறன்களும் அளவிட முடியாத அளவுக்கு உயர்ந்தவையாகவே இருந்து இருக்கிறது. அவரது எந்த ஒரு நகைச்சுவை  பேச்சும் யாரையும் காயப்பட வைத்தது  இல்லை. மேலும் அவரது ஒவ்வொரு நகைச்சுவை கதைகளிலும் ஏதேனும் ஒரு உள் அர்த்தம் மற்றும் நமது அன்றாட வாழ்க்கையோடு தொடர்புடைய பல சிந்தனைகளை சுமந்தே வந்து உள்ளன. அத்தகைய கதைகளை நாம் படிக்கும் முன்னர் அத்தகைய தெனாலி ராமனை பற்றி இப்பகுதியில் சிறிது தெரிந்து கொள்வோமா!
 
## தெனாலி ராமன் பிறப்பு
 
தெனாலி ராமன் ஆந்திர மாநிலம் கிருஷ்ணா மாவட்டத்தில் கார்லபதி என்கிற கிராமத்தில் ராமையா - லட்சுமி அம்மாள் தம்பதியரின் மகனாக ஏழை பிராமண குடும்பத்தில் பிறந்தவர். தெனாலியில் உள்ள இராமலிங்க சுவாமியின் நினைவாக இராமலிங்கன் என்றே பெயரிடப்பட்டார். இவர் பிறந்து மூன்றாம் நாள் இவருடைய தந்தையார் மரணமடைய குடும்பம் வறுமையில் வாடியது. இவருடைய தாயார் இவரை எடுத்து கொண்டு தெனாலியில் இருந்து அவருடைய சகோதரனுடைய வீட்டுக்கு வந்து சேர்ந்தார். தாய் மாமன் ஆதரவில் தான் இராமலிங்கம் வளர்ந்தார்.
 
## தெனாலியின் கோமாளித் தனங்கள்
 
உரிய பருவத்தில் பள்ளியில் சேர்ந்தாலும் படிப்பில் கவனம் செல்லவில்லை. மற்றவர்களை சிரிக்க வைக்கும் ஆற்றல் அவரிடம் இயற்கையாகவே இருந்தது. அதனால் அகடவிகட கோமாளித் தனங்களில் தான் அவருடைய அறிவும் ஆற்றலும் ஜொலித்தன. கமலா என்கிற பெண்ணை மணந்தார் தெனாலி ராமன்.

## தெனாலி விகடகவி ஆக உயர்ந்தது 
இவர் அரசவை விகடகவியாக உயர்ந்தது குறித்து பல கதைகள் நிலவுகின்றன. 
இவருடைய ஊருக்கு வந்த துறவி ஒருவர் இவருடைய தைரியம் மற்றும் நகைச்சுவை உணர்வில் கவரப்பட்டு காளி தேவியிடம் வரம் பெறத்தக்க மந்திரம் சொல்லி தந்ததாகவும் அதன் அருளால் காளி  தேவியின் தரிசனம் பெற்ற தெனாலி ராமன் அவளையும் தன் நகைச்சுவை அறிவால் சிரிக்க வைத்து அவளாலேயே விகடகவி என்று வாழ்த்தப்பட்டதாகவும் சொல்கிறார்கள்.
அதில் நம்பிக்கை வரப் பெற்ற தெனாலி ராமன் விஜயநகரம் சென்று தன்னுடைய சாமர்த்தியத்தால் அரச தரிசனம் பெற்று தன்  அறிவுக் கூர்மை மற்றும் நகைச்சுவையால் அரசரின் அன்புக்கும் நம்பிக்கைக்கும் பாத்திரமாகி அரசவையில் முக்கிய இடம் பெற்று நல்வாழ்வு வாழ்ந்தார்.
 
## தெனாலி சைவரா? வைணவரா?
இவர் சைவரா வைணவரா என்பதில் சரித்திர ஆசியர்களிடையே குழப்பம் நிலவுகிறது. ஏனென்றால் இவருடைய பெயர் சைவப் பெயர். இவர்  எழுதிய நூலாகிய "பாண்டுரங்க மகாத்மியம்" விஷ்ணுவைப் பற்றியதாக இருப்பதோடு அவருக்கு சமர்ப்பிக்கப்பட்டு உள்ளதாகவும் உள்ளது.
அது தவிர "உத்பாதராதைய சரித்திரம்", "கடிகாச்சல மகாத்மியம்" ஆகிய நூல்களை இயற்றி உள்ளார். முன்னது உத்பாதர் என்கிற துறவியை பற்றியது. பிந்தையது தமிழகம் வேலூருக்கு அருகில் இருக்கும் நரசிம்மர் கோவிலை பற்றியது.
 
## தெனாலியின் பட்டங்கள்
தெனாலி "விகடகவி" என்றும் "குமார பாரதி" என்றும் அனைவராலும் பாராட்டப்பட்டு அன்போடு அழைக்கப்பட்டார்.

## தெனாலியின் சிறப்புகள்
இந்திய மொழிகளில் இவரைப் பற்றிய பாடக் குறிப்புகள் இல்லாத மொழியே கிடையாது என்னும் அளவுக்குப் பிரபலமானவர்.இவருடைய கதையை "தி அட்வென்சர்ஸ் ஆஃப் தென்னாலி ராமன்" என்கிற பெயரில் கார்ட்டூன் நெட் ஒர்க் தொலைக்காட்சி நிறுவனம் கி.பி.2001இல் படமாக்கப்பது.
 
## தெனாலியின் காலம்
தெனாலி ராமன் வாழ்த்த காலம் கி.பி.1509 முதல் கி.பி.1575 வரை ஆகும். தமிழ் நகைச்சுவை உலகில் மிகவும் புகழ் பெற்ற தெனாலி ராமகிருஷ்ணா என்பவர் விஜயநகரத்தை ஆண்ட கிருஷ்ணா தேவராயரின் அவையை அலங்கரித்த எட்டு அரசவைப்  புலவர்களுள் (அஷ்டதிக்கஜங்கள்) ஒருவர் ஆவார்.
 
இப்படிப்பட்ட புகழ் பெற்ற புலவரான தெனாலி ராமனின் கதைகளில் "அதிசய குதிரை" என்னும் கதையை இப் பகுதியில் நாம் பார்க்கலாம்.
 
## அதிசய குதிரை 

கிருஷ்ண தேவாராயரின் படைகளுள் குதிரைப் படை மிகவும் முக்கியமான ஒன்று. குதிரைப் படையும் மிகவும் வலிமை உள்ளதாக இருந்தது. போர் இல்லாத காலங்களில் குதிரைகளை பராமரிக்க அரசவை மந்திரிகளில் ஒருவர் ஒரு நல்ல யோசனையை அரசருக்கு சொன்னார். அந்த யோசனை அரசர் கிருஷ்ண தேவராயருக்கு மிகவும் பிடித்து விட்டது. அந்த யோசனை சரி வரும் என்று  நினைத்தார். எனவே அதற்கு ஒப்புதல் அளித்தார்.
 
அந்த யோசனை  நாட்டில் உள்ள ஒவ்வொரு மக்களின் வீட்டிற்கும் ஒரு குதிரையையும் அதற்கு தீனி போடுவதற்கு தேவையான தொகையையும் அந்தந்த வீட்டை சேர்ந்தவர்களுக்கு கொடுத்து விட வேண்டும். அந்த தொகையை வைத்து அவர்கள் அந்த குதிரையை பராமரித்து வந்தால் அது ஆரோக்கியமுடனும் நல்ல வீரத்துடனும் இருக்கும் என்பது மந்திரியாரின் யோசனை.
 
இந்த யோசனை அரசருக்கு மிகவும் பிடித்து விட்டது. ஏனென்றால் இதற்கு என்று தனியாக காவலர்கள் பாதுகாப்பவர்கள் என நிறைய பேரை தேட தேவை இல்லை. எனவே அரசர் இதற்கு ஒப்புக் கொண்டார். 
 
உடனே அந்த மந்திரியரின் யோசனை படி ஒரு வீட்டிற்கு ஒரு குதிரையும் அதற்கு தீர் போடுவதற்கு ஒரு குறிப்பிட்ட தொகையும் கொடுக்கப்பட்டு வந்தது. மக்கள் அனைவரும் அந்த தொகையை பெற்று கொண்டு குதிரையை நன்கு ஓட்டம் அளித்து வளர்த்து வந்தனர்.
 
எல்லாருக்கும் ஒரு குதிரை கொடுக்கப்பட்டது போல் தெனாலி ராமனுக்கும் ஒரு குதிரை கொடுத்தனர்.ஆனால் தெனாலி ராமனோ ஒரு சிறிய கொட்டகையில் குதிரையை அடைத்து வைத்து புல் போடுவதற்கு மட்டும் ஒரு சிறிய துவாரம் வைத்து அந்த குதிரையை அங்கு அடைத்து வைத்து விட்டான். அந்த துவாரத்தின் வழியாக புல்லை நீட்டியவுடம் அந்த குதிரை வெடுக்கென அதை வாயால் கவ்விக் கொள்ளும். மிகவும் சிறிதளவு புள் மட்டுமே தினமும் தெனாலி ராமன் தனக்கு கொடுக்கப்பட்ட குதிரைக்கு போட்டு வந்தான். அவன் கொடுக்கும் தீனி அந்த குதிரைக்கு போதவில்லை. எனவே அந்த குதிரை எலும்பும் தோலுமாக நோஞ்சானாக இருந்தது.
 
குதிரைக்கு தீனி வாங்குவதற்கு என்று அரசர் கொடுத்த பணத்தை அந்த குதிரைக்கு அவன் செலவு செய்யவே இல்லை. மாறாக அந்த  பணம் முழுவதையும் தன்னுடைய தேவைகளுக்கு எடுத்து கொண்டான் தெனாலி ராமன். குதிரைக்கு தீனி வாங்கி போடும் பணத்தில் தெனாலி ராமன் நன்கு உண்டு கொழுந்தான்.
 
ஒரு நாள் குதிரைகள் எப்படி இருக்கின்றன என்பதை அரசன் காண நினைத்தார். எனவே நாட்டில் உள்ள அனைவருக்கும் செய்தி அனுப்பி வைத்தார். தான் கொடுத்த குதிரைகளை அனைவரும் அரண்மனைக்கு அழைத்து வரச் சொல்லி ஆணை இட்டார். எனவே அதன் படி குதிரைகள் அனைத்தும் அரண்மனைக்கு கொண்டு வரப்பட்டன. மன்னர் அணைத்து குதிரைகளையும் பார்வை இட்டார். குதிரைகள் அனைத்தும் மிக திருப்தியாக இருந்ததால் மன்னர் மகிழ்ச்சி அடைந்தார்.
 
ஆனால் தெனாலி ராமனோ தனது குதிரையை அழைத்து வராமல் தான் மட்டும் வந்து அங்கு நின்று கொண்டு இருந்தான். அதை அரசர் கவனித்து விட்டார். எனவே அங்கு நின்று கொண்டு இருந்த தெனாலி ராமனை அழைத்து "உன் குதிரை எங்கே? ஏன் குதிரையை கொண்டு வரவில்லை" என மன்னர் கேட்டார். 
 
அதற்கு தெனாலி ராமனோ "என் குதிரை மிகவும் முரட்டு தனமாக  இருக்கிறது. அதை என்னால் அடக்க முடியவில்லை. அதனால் தன இங்கே கொண்டு வர இயலவில்லை" என்று கூறினான். "குதிரைப் படைத் தலைவரை என்னுடன் அனுப்புங்கள். அவரிடம் எனது குதிரையை கொடுத்து அன்புகிறேன்" என்று கூறினான். இதை உண்மை என்று நம்பி விட்டார் அரசர். எனவே அரசர் குதிரைப் படைத் தலைவனை தெனாலி ராமனுடன் அனுப்பி வைத்தார்.
 
குதிரைப் படைத் தலைவருக்கு நீண்ட தாடி உண்டு. தெனாலி உடன் சென்ற தலைவர் அவனது வீட்டை அடைந்தார். குதிரைப் படைத் தலைவர் அந்த துவாரத்தின் வழியாக குதிரையை எட்டிப் பார்த்தார். உடனே குதிரை அது புல் தான் என்று நினைத்து அவரது தாடியை கவ்விக் கொண்டது. குதிரை தாடியை நன்கு பிடித்து கொண்டது. வலி பொறுக்க மாட்டாத குதிரை படைத் தலைவர் எவ்வளவோ முயன்றும் தாடியை குதிரையிடம் இருந்து விடுவிக்க முடியவில்லை. இச்செய்தி மன்னருக்கு எட்டியது. மன்னரும் உண்மையிலேயே அது முரட்டுக் குதிரையாகத்தான் இருக்கும் என்று எண்ணி தெனாலி ராமன் வீட்டுக்கு விரைந்தார். 
 
தெனாலி ராமன் வீட்டுக்கு வந்த மன்னர் அங்கு குதிரையின் வாயில் குதிரைப் படை தலைவரின் தாடி சிக்கி இருப்பதாய் அறிந்து அந்தக் கேட்டகையைப் பறிக்க செய்தார். பின் குதிரையைப் பார்த்தல் குதிரை எலும்பும், தோலுமாக நிற்பதற்கு கூட சக்தி இன்றி இருந்ததை கண்டு மன்னர் கோபம் கொண்டு அதன் காரணத்தை தெனாலி ராமனிடம் கேட்டார்.
 
அதற்கு தெனாலி ராமன் மன்னரை பார்த்து, "இவ்வாறு சக்தி இன்றி  இருக்கும் போதே குதிரைப் படைத் தலைவரின் தடியை கவ்விக் கொண்டு விட மாட்டேன் என்கிறது. நன்கு உணவு ஊட்டி வளர்த்து இருந்தால் குதிரைப் படைத் தலைவரின் கதி அதோகதி தான் ஆகி இருக்கும்" என்றான்.
 
இதை கேட்ட மன்னர் கோபத்திலும் சிறிது விட்டார். பின்னர் தெனாலி ராமனை மன்னித்து விட்டார்.
 
## கதையின் நீதி
 
இந்த கதையின் மூலம் நாம் தெரிந்து கொள்வது என்ன என்றால் எந்த ஒரு சூழ்நிலையாக இருந்தாலும் சமயோஜிதமாக யோசனை செய்து நாம் பேசினால் அந்த சூழலே அழகாக மற்றும் புன்னகை உள்ளதாக மாறி விடும். அதை தான் தெனாலி ராமன் இக்கதையில் செய்து இருக்கிறார்.
