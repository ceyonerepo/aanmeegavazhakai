---
title : 'ஜோஸ்யம் பலித்தது'
publishedDate: 'Fri Mar 04 2022 19:39:40 GMT+0530'
category: 'tenali-raman-stories'
slug: 'josyam-palithathu'
thumbnail: '../thumbnails/fighter.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - thenali ramang
    - siru kadhaigal
keywords: 
    - tenali raman
    - tenali raman stories
    - tenali rama krishna
    - tenaliraman
    - tenali ramakrishna stories
    - tenali raman stories in tamil
    - tenali raman short stories
---
 
# தெனாலி ராமன்
தெனாலி ராமன் என்றாலே அனைவருக்கும் நினைவிற்கு வருவது அவருடைய நகைச்சுவை பேச்சு தான். பேச்சு நகைச்சுவையாக இருக்குமே தவிர அவரது சிந்தனையும் திறன்களும் அளவிட முடியாத அளவுக்கு உயர்ந்தவையாகவே இருந்து இருக்கிறது. அவரது எந்த ஒரு நகைச்சுவை  பேச்சும் யாரையும் காயப்பட வைத்தது  இல்லை. மேலும் அவரது ஒவ்வொரு நகைச்சுவை கதைகளிலும் ஏதேனும் ஒரு உள் அர்த்தம் மற்றும் நமது அன்றாட வாழ்க்கையோடு தொடர்புடைய பல சிந்தனைகளை சுமந்தே வந்து உள்ளன. அத்தகைய கதைகளை நாம் படிக்கும் முன்னர் அத்தகைய தெனாலி ராமனை பற்றி இப்பகுதியில் சிறிது தெரிந்து கொள்வோமா!
 
## தெனாலி ராமன் பிறப்பு
 
தெனாலி ராமன் ஆந்திர மாநிலம் கிருஷ்ணா மாவட்டத்தில் கார்லபதி என்கிற கிராமத்தில் ராமையா - லட்சுமி அம்மாள் தம்பதியரின் மகனாக ஏழை பிராமண குடும்பத்தில் பிறந்தவர். தெனாலியில் உள்ள இராமலிங்க சுவாமியின் நினைவாக இராமலிங்கன் என்றே பெயரிடப்பட்டார். இவர் பிறந்து மூன்றாம் நாள் இவருடைய தந்தையார் மரணமடைய குடும்பம் வறுமையில் வாடியது. இவருடைய தாயார் இவரை எடுத்து கொண்டு தெனாலியில் இருந்து அவருடைய சகோதரனுடைய வீட்டுக்கு வந்து சேர்ந்தார். தாய் மாமன் ஆதரவில் தான் இராமலிங்கம் வளர்ந்தார்.
 
## தெனாலியின் கோமாளித் தனங்கள்
 
உரிய பருவத்தில் பள்ளியில் சேர்ந்தாலும் படிப்பில் கவனம் செல்லவில்லை. மற்றவர்களை சிரிக்க வைக்கும் ஆற்றல் அவரிடம் இயற்கையாகவே இருந்தது. அதனால் அகடவிகட கோமாளித் தனங்களில் தான் அவருடைய அறிவும் ஆற்றலும் ஜொலித்தன. கமலா என்கிற பெண்ணை மணந்தார் தெனாலி ராமன்.

## தெனாலி விகடகவி ஆக உயர்ந்தது 
இவர் அரசவை விகடகவியாக உயர்ந்தது குறித்து பல கதைகள் நிலவுகின்றன. 
இவருடைய ஊருக்கு வந்த துறவி ஒருவர் இவருடைய தைரியம் மற்றும் நகைச்சுவை உணர்வில் கவரப்பட்டு காளி தேவியிடம் வரம் பெறத்தக்க மந்திரம் சொல்லி தந்ததாகவும் அதன் அருளால் காளி  தேவியின் தரிசனம் பெற்ற தெனாலி ராமன் அவளையும் தன் நகைச்சுவை அறிவால் சிரிக்க வைத்து அவளாலேயே விகடகவி என்று வாழ்த்தப்பட்டதாகவும் சொல்கிறார்கள்.
அதில் நம்பிக்கை வரப் பெற்ற தெனாலி ராமன் விஜயநகரம் சென்று தன்னுடைய சாமர்த்தியத்தால் அரச தரிசனம் பெற்று தன்  அறிவுக் கூர்மை மற்றும் நகைச்சுவையால் அரசரின் அன்புக்கும் நம்பிக்கைக்கும் பாத்திரமாகி அரசவையில் முக்கிய இடம் பெற்று நல்வாழ்வு வாழ்ந்தார்.
 
## தெனாலி சைவரா? வைணவரா?
இவர் சைவரா வைணவரா என்பதில் சரித்திர ஆசியர்களிடையே குழப்பம் நிலவுகிறது. ஏனென்றால் இவருடைய பெயர் சைவப் பெயர். இவர்  எழுதிய நூலாகிய "பாண்டுரங்க மகாத்மியம்" விஷ்ணுவைப் பற்றியதாக இருப்பதோடு அவருக்கு சமர்ப்பிக்கப்பட்டு உள்ளதாகவும் உள்ளது.
அது தவிர "உத்பாதராதைய சரித்திரம்", "கடிகாச்சல மகாத்மியம்" ஆகிய நூல்களை இயற்றி உள்ளார். முன்னது உத்பாதர் என்கிற துறவியை பற்றியது. பிந்தையது தமிழகம் வேலூருக்கு அருகில் இருக்கும் நரசிம்மர் கோவிலை பற்றியது.
 
## தெனாலியின் பட்டங்கள்
தெனாலி "விகடகவி" என்றும் "குமார பாரதி" என்றும் அனைவராலும் பாராட்டப்பட்டு அன்போடு அழைக்கப்பட்டார்.

## தெனாலியின் சிறப்புகள் 
இந்திய மொழிகளில் இவரைப் பற்றிய பாடக் குறிப்புகள் இல்லாத மொழியே கிடையாது என்னும் அளவுக்குப் பிரபலமானவர்.இவருடைய கதையை "தி அட்வென்சர்ஸ் ஆஃப் தென்னாலி ராமன்" என்கிற பெயரில் கார்ட்டூன் நெட் ஒர்க் தொலைக்காட்சி நிறுவனம் கி.பி.2001இல் படமாக்கப்பது.
 
## தெனாலியின் காலம்
தெனாலி ராமன் வாழ்த்த காலம் கி.பி.1509 முதல் கி.பி.1575 வரை ஆகும். தமிழ் நகைச்சுவை உலகில் மிகவும் புகழ் பெற்ற தெனாலி ராமகிருஷ்ணா என்பவர் விஜயநகரத்தை ஆண்ட கிருஷ்ணா தேவராயரின் அவையை அலங்கரித்த எட்டு அரசவைப்  புலவர்களுள் (அஷ்டதிக்கஜங்கள்) ஒருவர் ஆவார்.
 
இப்படிப்பட்ட புகழ் பெற்ற புலவரான தெனாலி ராமனின் கதைகளில் "ஜோஸ்யம் பலித்தது " என்னும் கதையை இப் பகுதியில் நாம் பார்க்கலாம்.

## ஜோஸ்யம் பலித்தது
 
 பீஜப்பூர் சுல்தான் அரசர் கிருஷ்ண தேவராயரை கொள்வதர்காக ஒற்றன் ஒருவரை அனுப்பி வைத்தார். ஆனால் அந்த ஒற்றனை அடையாளம் கண்டு பிடித்து தெனாலி ராமன் அரசருக்கு தாக்க சமயத்தில் காட்டினான். எனவே அரசர் கிருஷ்ண தேவராயர் அந்த ஒற்றனை வாளால் குத்து கொன்று விட்டார். பிறகு மிகுந்த கோபம் கொண்ட அரசர் கிருஷ்ண தேவராயர் பீஜப்பூர் சுல்தான் மீது படை எடுக்க அனைத்து ஏற்பாடுகளையும் செய்ய ஆரம்பித்தார்.

தன ஒற்றனின் கதியையும் தீவிரமான யுத்த ஏற்பாட்டையும் கேள்விப் பட்ட பீஜப்பூர் சுல்தான் மிகவும் மனம் உடைந்து போனார். எப்படியும் இதைத் தடுக்க வேண்டுமே என்று எண்ணி இரகசியமாக விஜய நகரத்து அரண்மனை ஜோசியன் ஒருவனை  வருவித்தான். 

அரசர் கிருஷ்ண தேவராயருக்கு தகுந்த ஜோஸ்யம் சொல்லி, வர இருக்கும் யுத்தத்தைத் தடுக்கும் படி நிறைய பொன்  கொடுத்து அனுப்பி வைத்தான் பீஜப்பூர் சுல்தான். அரண்மனை ஜோதிடனுக்கு அளவு இல்ல சந்தோசம். அவனுடைய பாத்து வருஷ வருமானம் ஏக சமயத்தில் கிடைத்தால் எப்படி இருக்கும். அப்படி ஒரு சந்தோசம் அவனுக்கு.

மறுநாளே அரசனிடம் சென்றான் ஜோதிடன். "யுத்தத்திற்கான ஏற்பாடுகள் அனைத்தையும் செய்து வைத்துக் கொண்டு இப்படி தாங்கள் தயாராக உள்ளீர்கள் அரசரே. ஆனால் உங்கள் கால பலன் அதற்கு ஏற்றார் போல் தற்போது இல்லையே" என்று கூறினான்.

"பின் எப்படி இருக்கிறது" என்று அரண்மனை ஜோசியனிடம் அரசர் கிருஷ்ண தேவராயர் கேட்டார். "அரசே! நன்றாய் ஆராய்ந்து பார்த்தேன். இன்னும் ஊர் ஆண்டு வரை இந்த துங்கபத்திரா நதியை நீங்கள் தாண்டவே கூடாது என்று அல்லவே இருக்கிறது?" என்று ஜோதிடன் அரசர் கிருஷ்ணா தேவராயரிடம் கூறினான். 

உடனே அரசர் கிருஷ்ணா தேவராயர் "ஏன் தாண்டினால்" என்று கேட்டார். அவரது குரலில் மிகுந்த ஆர்வம் தொனித்தது. அது ஏன் என்று அறிந்து கொள்ளும் ஆர்வம். அதற்கு அரண்மனை ஜோதிடன் "எனக்கு சொல்லவே நா கூசுகிறது. அரசே! அவ்வாறு நடந்தால் தங்கள் உயிருக்கு ஆபத்து" என்று அரசர் கிருஷ்ணா தேவராயரிடம் கூறினான்.

அதை கேட்டு, "அரசே! அப்படி என்றால் படை எடுப்பைப் பற்றி யோசிக்க வேண்டியது தான். பீஜப்பூர் ராஜ்யமா பெரிது? தங்கள் மேலான உயிர் அல்லவே எங்கள் அனைவருக்கும் மிகப் பெரியது" என்று கூறலாயினர் அரண்மனைப் பிரதானிகள்.

செய்தி அறிந்து அந்தப்புர ராணிகளும் "இப்போது யுத்தம் வேண்டாம்" என்று மன்றாடத் தொடங்கி விட்டார்கள். அரசர் ஆழ்ந்து யோசிக்க ஆரம்பித்தார்.

உடனே தெனாலி ராமனை  அழைத்தார். "என்ன தெனாலி ராமா எனக்கோ யுத்தம் செய்ய வேண்டும் என்று இருக்கிறது. அதே சமயத்தில்  வார்த்தையை தட்ட முடியவில்லை. இதற்கு என்ன செய்யலாம்" என்று தெனாலி ராமனை பார்த்து அரசர் கிருஷ்ணா தேவராயர் யோசனை கேட்டார்.

உடனே தெனாலி ராமன் அரசர் கிருஷ்ண தேவராயரிடம் "அரசே, ஜோதிடன் சொல்லும் பலன்கள் அனைத்தும் அப்படியே நடந்து விடுகிறதா? என்ன" என்று கேட்டான். அதற்கு அரசர் கிருஷ்ண தேவராயர் "ஆமாம், நீ சொல்வது சரி தான். ஆனால் அதை நிரூபித்துக் காட்ட வேண்டாமா? அப்படி நிரூபித்து காட்டுபவனுக்கு பத்தாயிரம் பொன் வரை பரிசு கொடுக்க தயாராய் இருக்கிறேன்" என்று கூறினார் அரசர் கிருஷ்ண தேவராயர்.

"அப்படி என்றால் சரி அரசே! அதை நான் ஏற்றுக் கொள்கிறேன். ஆனால் அதை நிரூபித்துக் காட்டி அந்த ஜோதிடனுக்கு நானே தண்டனை கொடுக்கவும் தாங்கள் அனுமதி அளிக்க வேண்டும்", என்று கேட்டான் தெனாலி ராமன்.

"ஓ அப்படியே ஆகட்டும்" என்றார் அரசர் கிருஷ்ண தேவராயர். அதற்கு மறுநாளே தர்பார் கூடிற்று. அந்த ஜோதிடனும் வந்து இருந்தான். "என்ன ஜோதிடரே, உம்முடைய பலன் எல்லாம் தவறாமல் சரியாய் நடந்து ஏறுமா? என்று கேட்டான் தெனாலி ராமன். 

அதற்கு அந்த ஜோதிடன், "ஆகா! அதற்கு என்ன சந்தேகம்! அப்படியே நூற்றுக்கு நூறு நடந்து ஏறும்" என்று கூறினான். அப்படி என்றால் நீங்கள் நீடுழி வாழ வேண்டும்" என்று ஒரு பெரிய கும்பிடு போடு கொண்டான் தெனாலி ராமன். உடனே அவன் "அது சார், உங்கள் ஆயுள் பாதம் எப்படி இருக்கிறது. இன்னும் எதனை ஆண்டுகள் வாழ்வீர்கள்?" என்று ஜோதிடனை பார்த்து கேட்டான். "எனக்கு பூரண ஆயுசு உண்டு என்று ஏதோ கனக்குப் போட்டுப் பார்த்து விட்டு "இன்னும் நாற்பத்து நாலு ஆண்டுகள் இருப்பேன்" என்று பெருமிதமாய்ச் சொல்லிக் கொண்டான் ஜோதிடன். 

உடனே தெனாலி ராமன், "உண்மையாகவே! இதோ உம்முடைய வாகு பொய்த்து விட்டதே" என்று வெடுக்கென்று வாளை உருவி நறுக்கென்று அந்த ஜோதிடன் உடைய தலையாகி சிதைத்தான் தெனாலி ராமன். "தன் ஆயுளை சரியாக தெரிந்து கொள்ளாத இவன் அரசனுடைய ஆயுளுக்கு கணக்கு இடுகிறானாம்," என்று சொல்லிக் கொண்டே அவனுடைய புத்தக கட்டை பரிசோதனை செய்தான் தெனாலி ராமன்.

அந்த ஜோதிடன் அதிபத்திரமாய் வைத்து இருந்த புத்தக கட்டில், பீஜப்பூர் சுல்தானுக்கும் அவனுக்கும் நடந்து கொண்டு இருந்த கடிதப் போக்குவரத்து அனைத்தும் அதில் இருந்தது. அனைவருக்கும் ஒரே ஆச்சரியம். ஆகா!  இவனுக்கு சரியான தண்டனை என்று  ஆமோதித்தார்கள்.

உடனே அரசர் தான் சொன்ன படி பத்தாயிரம் பொன்னையும் தெனாலி ராமனுக்கு .பரிசாகக் கொடுத்தார். 

மறுநாளே அரசர் தன்  ஏற்பாட்டின் படி பீஜப்பூர் சுல்தானின் மீது படை எடுத்தார். வெகு விரைவில் பீஜாப்பூரோடு குல்பர்காவையும் ஜெயித்து கொண்டு வெற்றிக்கு கொடியுடன் ஊர் திரும்பினார். 

## கதையின் நீதி:

யாரேனும் இருக்கும் இடத்தில் இருப்பவர்களுக்கு எதிராக ஏதும் சாதி செய்தால் அது ரொம்ப காலம் நீடிக்காது. அவர்களுக்கு இந்த கதையில் வந்த ஜோதிடரின் நிலைமை தான் ஏற்படும். எனவே நாம் எங்கு இருக்கிறோமோ அவர்களுக்கு தான் விசுவாசமாக இருக்க வேண்டும். இதை தான் இந்த கதை நமக்கு சொல்கிறது.
