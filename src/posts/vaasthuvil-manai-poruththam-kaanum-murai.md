---
title : 'வாஸ்துவில் மனைப் பொருத்தம் காணும் முறை'
publishedDate: 'Thu Mar 17 2022 12:39:28 GMT+0530'
category: 'life'
slug: 'vaasthuvil-manai-poruththam-kaanum-murai'
thumbnail: '../thumbnails/vaasthu-house.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - vasthu
    - aanmeegam
    - nambikai
    - vaasthu saasthiram
keywords:
    - vasthu tamil
    - vasthu in tamil
    - manaiyadi shastra vastu in tamil
---

# வாஸ்து சாஸ்திரம் 
# வாஸ்துவில் மனைப் பொருத்தம் காணும் முறை

## வாஸ்து சாஸ்திரம் என்றால் என்ன?

1. நமது வீடு எப்படி இருக்க வேண்டும்?
2. வீடு காட்டும் முறைகள் 
3. எப்படி வீடு இருந்தால் என்ன பலன் கிடைக்கும்
என்பதை விளக்கி சொல்வது வாஸ்து சாஸ்திரம் ஆகும்.

தனக்கு என்று ஒரு வீடு வேண்டும் என்ற ஆசை எல்லோருக்கும் இருக்கிறது. கூழோ கஞ்சியோ நம் வீட்டில் குடித்தால் அதன் திருப்தியே தனி தான். ஆனால் வீடு கட்டும் போது பலர் முக்கியமான வாஸ்து கூட பார்க்காமல் கட்டி விட்டு பின் அவதிப் படுகிறார்கள். ஆபத்து வந்த பின் காப்பதை விட வரும் முன்னே காப்பது நல்லது அல்லவா!

## வாஸ்து:

வாஸ்து என்பது மனையடி சாஸ்திரத்தின் இதயம் போன்றது ஆகும். வாஸ்து எனப்படும் சமஸ்கிருத சொல்லுக்கு வீடு கட்டக் கூடிய மனை என்று பொருள் படும். இது தனி சாஸ்திரமாக சிலரால் கருதப்படுகிறது. இந்த சாஸ்திரத்தின் நாயகனாக தெய்வாம்சம் பொருந்திய ஒரு பெண் கருதப்படுகிறது.

உயிரற்ற ஜடப் பொருட்களான செங்கல், சிமெண்ட், மணல், கம்பிகள், வெட்டப்பட்ட மரம் போன்ற வஸ்துகளை சரியான விகிதத்தில் கலந்து நிலத்தின் மேல் கட்டிடமாக எழுப்பி உயிரோட்டம் கொடுப்பதே வாஸ்து.

வாஸ்து உண்மையில்லை, வாஸ்து ஏமாற்று வித்தை, வாஸ்து பொய் என்று சமூக அக்கறையோடு பதிவு செய்யப்படும் குற்றச்சாட்டுகளுக்கு என் பதில், 

வாஸ்து என்பது உண்மை, வாஸ்து என்பது அறிவியல், வாஸ்து என்பது மத சம்மந்தப்பட்ட விஷயம் அல்ல. வாஸ்துவில் யந்திரம், மந்திரம், பூஜை, தாயத்து மற்றும் கண் கட்டு வித்தைகளுக்கு வேலை இல்லை.

பாம்பு கடிக்கு விஷமுறிவு மருந்து மற்றொரு பாம்பின் விஷம் தான் என்பது உண்மையாக இருப்பின் வீட்டில் உள்ள பிரச்சனைகளுக்கு தீர்வு அந்த வீட்டிலேயே தான் உள்ளது என்பது தான் நான் கண்டா உண்மை.

## வாஸ்து விதிகள்:

வாஸ்து சாஸ்திரம் என்பது ஒவ்வொரு மனித வாழ்வின் வெற்றிக்கும் அடித்தளமாக விளங்கக் கூடிய ஒன்று ஆகும். வாஸ்து விதிகளின் படி ஒரு வீடோ, தொழிற்சாலையோ அமைக்கப்படும் போது அங்கு இருக்கக் கூடிய அனைவருக்கும் எப்பொழுதும் நல்ல ஆற்றலே இருக்கும். என்றும் மன அமைதி, உடல் நலம், செல்வ செழிப்போடு காணப்படுவர். வாஸ்து என்றால் நான்கு திசை மற்றும்  நான்கு மூலைகள் கொண்டே கருதப்படுகின்றன. அவை வடக்கு, கிழக்கு, தெற்கு, மேற்கு. மேலும் நான்கு மூலைகள் வடகிழக்கு, தென்கிழக்கு, வடமேற்கு, தென்மேற்கு ஆகும். இவற்றை கொண்டு வாஸ்துவில் அடிப்படையாக கருதப்படும் ஆறு மிக முக்கியமான பொதுவான விதிகள்.

## வாஸ்துவின் பொதுவான விதிகள்:

1. மனை மற்றும் அதனுள் கட்டப்படும் கட்டிடம் இரண்டும் சதுரம் அல்லது செவ்வகமாக இருக்க வேண்டும்.

2. வடக்கு  மற்றும் கிழக்கில் அதிக காலி இடம் இருத்தல் அவசியம்.

3. தலை வாசல் என்றுமே உச்சத்தில் தான் அமைக்கப்பட வேண்டும்.

4. தெருத் தாக்கம் (தெருக்  குத்து) இருந்தால் கண்டிப்பாக உச்சமாக தான் இருக்க வேண்டும்.

5. உட்புறம் மற்றும் வெளிப்புறம் போடப்படும் படிக்கட்டுகள் அமைக்கப்படும் முறையை அறிவது அவசியம்.

6. வடகிழக்கு பள்ளமாகவும், கனமில்லாமலும், தென்மேற்கு உயரமாகவும், கனமாகவும் இருத்தல் அவசியம்.

வாஸ்து சாஸ்திரத்தைப் பற்றி இன்னும் பல அறிய நூல்கள் உள்ளன. சிற்ப நகரம், சிற்ப ரத்தினம், மயமதம், காஸ்யபம், ஸர்வார்த்த மனுசாரம், வாஸ்து வித்யா போன்ற நூல்கள் சிலை வடிப்பது, ஆலயம் அமைப்பது, வீடு கட்டுவது போன்ற அறிய கலைகளை பற்றி விவரிக்கின்றன.


# வாஸ்துவில் மனைப் பொருத்தம் காணும் முறை

மனைப் பொருத்தம் காண்பது என்பது மிகச் சிறந்த சற்று கடினமான ஆனால் அவசியமான கணித முறை ஆகும். இதற்கென பதினோரு விதமான பொருத்தத்தை நாம் பார்க்க வேண்டும்.

## 1. கெற்பப் பொருத்தம்:

மனையின் அளவை மனைக்கு உரியவரின் கையால் நான்கு சாண் அளவு கொண்ட கோலினால் (சுமார் 36 அங்குலம்) அளந்து அகல நீளத்தைப் பெருக்கிய தொகை துருவபதம் எனப்படும். அதை குழி என்றும் கூறுவார். துருவபதம் எனும் குழியை எட்டால் வகுக்க மிஞ்சி நிற்கும் மீதியை கெற்பப் பொறுத்த எண் ஆகும். வகுத்துப் பார்க்க மீதி இல்லாமல் 0 வந்தால் எட்டாம் என்னையே மீதமாகக் கொண்டு பலன் காண வேண்டும்.

மீதி 1 என்றால் கருட கெற்பம் - உத்தமம்.
மீதி 2 என்றால் புறா கெற்பம் - மத்திமம்.
மீதி 3 என்றால் சிம்ம கெற்பம் - உத்திமம் 
மீதி 4 என்றால் நாய் கெற்பம் - அதமம் 
மீதி 5 என்றால் சர்ப்ப கெற்பம் - அதமம் 
மீதி 6 என்றால் காக்கை கெற்பம் - அதமம் 
மீதி 7 என்றால் யானை கெற்பம் - உத்தமம் 
மீதி 8 என்றால் கழுதை கெற்பம் - அதமம்

இந்த அட்டவணையைப் பயன்படுத்தி கெற்பப் பொருத்தம் காணலாம்.

## 2. ஆதாயப் பொருத்தம்:

முன்பு கண்ட துருவத்தை எட்டால் பெருக்கி அதை 12 ஆல் வகுக்க மீதம் இருந்தால் ஆதாயம் ஆகும். மீதம் இல்லை எனில் எட்டாம் எண்ணையே மீதமாக கொள்ளலாம். மீதத்திற்கு கீழ்கண்ட படி பலன்களை அறிந்து கொள்ளலாம்.

1 - 2 = போகம்
3 - 4 = தீயால் பயம் 
5 - 6 = தன இலாபம் 
7 - 8 = மங்களம் 
9 - 10 = தெய்வ அருள் 
11 - 12 = சுக மேன்மை 

## 3. விரயப் பொருத்தம்:

முன்பு கண்ட துருவத்தை ஒன்பதால் பெருக்கி 12 ஆல் வகுக்க மீதம் விரயம் ஆகும். இல்லாவிட்டால் பத்தாம் எண்ணை மீதம் எனக் கொள்ள வேண்டும்.

1 - நாசம் 
2 - தீயால் பயம் 
3 - நன்மை 
4 - புத்திர விருத்தி 
5 - விரயம் 
6 - ஆரோக்கியம் 
7 - வறுமை 
8 - திருமகள் அருள் 
9 - புத்திர தோஷம் 
10 – உயர்நிலை

## 4. யோனிப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 3 ஆல் பெருக்கி 8 ஆல் வகுக்க மீதம் வருவது யோனி ஆகும். மீதம் இல்லாவிடில் வகுத்த 8யே மீதமாகக் கொள்ள வேண்டும். கீழ்கண்ட அட்டவணையைப் பயன்படுத்தி யோனிப் பொருத்தத்தை அறியலாம்.

1 - கருடன் = செல்வம் 
2 - பூனை = கழகம் 
3 - சிங்கம் = வெற்றி 
4 - நாய் = நோய் 
5 - பாம்பு = செல்வம் 
6 - எலி = தனம் 
7 - யானை = இலாபம் 
8 - முயல் = வறுமை 

## 5. நட்சத்திர பொருத்தம்:

முன்பு கண்ட துருவத்தை 8 ஆல் பெருக்கி 27 ஆல் வகுக்க மீதம் நட்சத்திர பொருத்தத்தை கீழ்கண்ட அட்டவணையால் அறிந்து கொள்ளலாம்.

1 - அசுவினி = உத்தமம் 
2 - பரணி = அதமம் 
3 - கிருத்திகை = அதமம் 
4 - ரோகிணி = உத்தமம் 
5 - மிருகசீரிஷம் = அதமம் 
6 - திருவாதிரை = உத்தமம் 
7 - புனர்பூசம் = உத்தமம் 
8 - பூரம் = அதமம் 
9 - ஆயில்யம் = மத்திமம் 
10 - மகம் = மத்திமம் 
11 - பூசம் = மத்திமம் 
12 - உத்திரம் = உத்தமம் 
13 - அஸ்தம் = அதமம் 
14 - சித்திரை = மத்திமம் 
15 - சுவாதி = உத்தமம் 
16 - விசாகம் = அதமம் 
17 - அனுஷம் = உத்தமம் 
18 - கேட்டை = மத்தமம் 
19 - மூலம் = மத்திமம் 
20 - பூராடம் = அதமம் 
21 - உத்திராடம் = மத்திமம் 
22 - திருவோணம் = மத்திமம் 
23 - அவிட்டம் = அதமம் 
24 - சதயம் = உத்தமம் 
25 - பூரட்டாதி = அதமம் 
26 - உத்திரட்டாதி = உத்தமம் 
27 - ரேவதி = மத்திமம்

## 6. வாரப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 9 ஆல் பெருக்கி 7 ஆல்  வகுக்க மீதி கிடைப்பதை கீழ்கண்ட அட்டவணை மூலம் வாரப் பொருத்தம் பற்றி அறியலாம்.

1 - ஞாயிறு - சுகக்கேடு 
2 - திங்கள் - தீங்கு 
3 - செவ்வாய் - தீமை 
4 - புதன் - லட்சுமி கடாட்சம் 
5 - வியாழன் - புத்திர விருத்தி 
6 - வெள்ளி - சுகம் மேன்மை 
7 - சனி - கழகம்

## 7. அம்சப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 4 ஆல் பெருக்கி 9 ஆல் வகுக்க மீதம் வருவதை கீழ்கண்ட அட்டவணை மூலம் அம்சப் பொருத்தம் அறியலாம். மீதம் இல்லாவிடில் 9ம் எண்ணையே மீதமாக கொள்ள வேண்டும்.

1 - தடகர அம்சம் = துன்பம் 
2 - சோமகரான அம்சம் = பாக்கியம் 
3 - சக்தி அம்சம் = சுகம் 
4 - புஷ்கர அம்சம் = நவதானிய செழிப்பு 
5 - குரு அம்சம் = இலாபம் 
6 - சுக்கிர அம்சம் = சுகபோகம் 
7 - சனி அம்சம் = பகை 
8 - இராகு அம்சம் = துயரம் 
9 - கேது அம்சம் = சகல சௌபாக்கியம்

## 8. வம்சப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 4 ஆல் வகுக்க மீதத்தை கீழ் கண்ட அட்டவணை மூலம் வம்சப் பொருத்தம் அறியலாம். மீதம் இல்லாவிடில் 4யை மீதமாகக் கொள்ளலாம்.

1 பிரம்மா வருணம் = சுபம் 
2 ஷத்திரிய வருணம் = சுபம் 
3 வைசிய வருணம் = சுபம் 
4 மற்றைய வருணம்  = சுபம் 

## 9. திதிப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 9 ஆல் பெருக்கி 15 ஆல் வகுக்க மீதி வருவதை கீழ்கண்ட அட்டவணையின் படி அறியலாம். மீதி இல்லையேல் 15 ஆம் எண்ணையே மீதமாக கொள்ளவும்.

1 - பிரதமை = நன்மை 
2 - துதியை = நன்மை 
3 - திரிதியை = வெற்றி 
4 - சதுர்த்தி = தோல்வி 
5 - பஞ்சமி = புத்திர பாக்கியம் 
6 - சஷ்டி = சுக மேன்மை 
7 - சப்தமி = நோய் 
8 - அஷ்டமி = இழப்பு 
9 - நவமி = பீடை 
10 -  தசமி - மங்களம் 
11 - ஏகாதசி = தீமை
12 - துவாதசி = சுகம் 
13 - திரயோதசி = செல்வம் 
14 - சதுர்த்தசி = நாசம் 
15 - பௌர்ணமி (அ) அமாவாசை = சுகம்

## 10. இராசிப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 4 ஆல்  பெருக்கி 12 ஆல்  வகுக்க மீதியை கீழுள்ள அட்டவணையின் படி பொருத்தம் காணலாம். மீதி இல்லை எனில் மீதம் 12 என்றே கொள்ள வேண்டும்.

1 - மேஷம் = சுகம் 
2 - ரிஷபம் = க்ஷேமம் 
3 - மிதுனம் = மேன்மை 
4 - கடகம் = புகழ் 
5 - சிம்மம் = வரம் 
6 - கன்னி = அன்பு 
7 - துலாம் = இன்பம் 
8 - விருச்சகம் = வெற்றி 
9 - தனுசு = லாபம் 
10 - மகரம் = சுகம் 
11 - கும்பம் = தீமை 
12 - மீனம் = உயர்வு 

## 11. வயதுப் பொருத்தம்:

முன்பு கண்ட துருவத்தை 27 ஆல் பெருக்கி 100 ஆல் வகுத்து மீதியை வயதுப் பொருத்தம் காணலாம். மீதம் இல்லையேல் 100 ஆல் எண்ணை மீதமாக கொள்ளலாம்.

45 க்கு மேல் உத்தமம் 
45 க்கு கீழ் அதமம் 

இது வரை கண்ட பதினோரு பொருத்தங்களில் குறைந்த பட்சம் ஆறு பொருத்தங்களாவது பொருந்தி வர வேண்டும்.
