---
title : 'மாமரம் யாருடையது?'
publishedDate: 'Wed Mar 30 2022 13:03:23 GMT+0530'
category: 'akbar-beerbaal-stories'
slug: 'maamaram-yaarudaiyathu'
thumbnail: '../thumbnails/mango-tree.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - akbar beerbal stores
    - siru kadhaigal
    - moral stories
    - naneri kathaigal
    - tamil short stores
    - stories in tamil
keywords: 
    - akbar beerbal stories
    - akbar beerbal
    - beerbal
    - tamil tories
    - moral stories in tamil
    - tamil kathaigal
---
 
# அக்பர் பீர்பால் கதைகள் - தமிழ் நன்னெறிக் கதைகள்

# மாமரம் யாருடையது?

## ஜலாலுத்தீன்  முகம்மது அக்பர்:

மொகலாயப் பேரரசர்களில் பெருஞ் சிறப்பு வாய்ந்தவர் ஜலாலுத்தீன் முகம்மது அக்பர் ஆவார். அவருடைய பாட்டனார் பாபர் என்பவர் இப்ராஹிம் லோடியை வென்று மொகலாயப் பேரரசை 1526 - ல் தோற்றுவித்தார். அவர் நான்கு ஆண்டு காலம் தான் அரசாட்சி செய்தார். அதற்குப் பிறகு அக்பருக்குத் தந்தையான உமாயூன் என்பவர் 1530 ல் அரியணை ஏறினார். பத்தாண்டு காலம் தொடர்ந்தார் போல் அரசாட்சி நடத்துவதற்கு அவர் பட்டபாடு கொஞ்ச நஞ்சமல்ல. அந்தச் சமயத்தில் சூர் வம்சத்தைச் சேர்ந்த ஷெர்கான் என்பவன் உமாயூனுக்கு விரோதமாகக் கிளம்பி அவரை முறியடித்து விட்டு அரசைக் கைப்பற்றிக் கொண்டான்.

ஷெர்கானால் தோற்கடிக்கப்பட்ட உமாயூன் நாடு நகரம், சொத்து சுகம் அனைத்தையும் இழந்து, பகைவர்க்கு அஞ்சி அமர்கோட் ராணாவிடம் தஞ்சம் புகுந்தார். அங்கு தான் ஜலாலுத்தீன் முகம்மது அக்பர் 1542 ல் பிறந்தார்.

உமாயூன் பதினைந்து ஆண்டுகளுக்குப் பிறகு பெரும் படையுடன் 1555 ல் ஷெர்ஷாவின் மேல் படை எடுத்தார். அப்பொழுது அக்பருக்கு வயது பதின்மூன்று. உமாயூன் தன்னுடைய படையை இரு பிரிவுகளாகப் பிரித்து ஒரு பிரிவுக்கு சிறுவன் அக்பரையும் அவனுக்குத் துணையாக தளபதி பைராம்கானையும் அனுப்பினார். இம்முறை உமாயூன் வெற்றிவாகை சூடினார். இழந்த நாட்டை திரும்பப் பெற்று மீண்டும் 1555 ல் பட்டம் சூட்டிக் கொண்டார். அதற்கு பிறகு ஓராண்டு காலம் தான் உமாயூன் உயிரோடு வாழ்ந்தார். அவர் இறந்ததும்  பதினான்கு வயதுச் சிறுவனான அக்பர் 1556 ல் அரியணை ஏறினார். அவருக்குத் துணையாக தளபதி பைராம்கான்  ஆரம்பத்தில் துணை புரிந்தார்.

அக்பர் தன்னுடைய ஆட்சிக் காலத்தின் போது இந்து முஸ்லீம் ஒற்றுமைக்காக மிகவும் பாடுபட்டார். இந்துக்கள் பால் சக்கரவர்த்தி அன்பு காட்டியதற்கு காரணம் பீர்பால் தான் என்று நினைக்கத் தோன்றுகிறது. 

பீர்பாலுடன் சேர்த்து மொத்தம் ஒன்பது அறிஞர்கள் அக்பரின் அரசவையை அலங்கரித்தார்கள். இவர்களை "நவரத்தினங்கள்" என்று குறிப்பிடுவார்கள். ராஜா பீர்பால், ராஜா மான்சிங், ராஜா தேடர்மால், ஹகீம் ஹிமாம், முல்லா தோபியாஸா, பைஜி, அபுல்பசல், ரஹீம், மியான் தான்சேன் முதலியவர்கள் தான் இந்த நவரத்தினங்கள்.

மதங்களினால் தான் மக்கள் வேற்றுமை அடைந்து ஒருவருக்கு ஒருவர் வீணே சச்சரவு இட்டு கொள்கிறார்கள் என்று கருதிய அக்பர் அனைவரையும் ஒன்று சேர்க்கக் கருதி ஒரு புதிய மதத்தை தோற்றுவித்தார்.  இப்புதிய மதத்திற்கு "தீன்இலாஹி" என்பதாகும். 

அக்பர் தம்முடைய 63 வது வயதில் 1605 ல் "தான் பெற்ற மகனே தனக்கு விரோதியாக மாறி விட்டானே" என்ற மன வருத்தத்துடன் உயிர் துறந்தார். அவருடைய விருப்பத்திற்கிணங்க அவர் இறந்த போது எந்த மதத்தினருக்கு உரிய துதிப் பாடல்களும் அவருக்கு ஓதப்படவில்லை. அவருடைய சடலம் ஆடம்பரம் ஏதுமின்றி கல்லறையில் புதைக்கப் பெற்றது.

## ராஜா பீர்பால் 

ராஜா பீர்பாலுடைய இயற்பெயர் மகேஷ்தாஸ் என்பது ஆகும். சிலர் பிரம்மதாஸ் என்றும் குறிப்பிடுகிறார்கள். பீர்பால் என்றால் வீரர்களில் பலம் பொருந்தியவன் என்பதாகும். இந்தப் பெயரை இவருக்குச் சூட்டியவரும் அக்பர் சக்கரவர்த்தி தான்.

பீர்பால் கோதாவரி நதிக் கரையிலுள்ள மார்ஜால் என்ற ஊரில் பிறந்ததாகச் சிலர் கூறுகிறார்கள். அப்துல் காதர் பதௌனி என்ற சரித்திராசிரியர் இவர் கால்பி என்ற ஊரில், ஓர் ஏழைப் பிராமணக் குடும்பத்தில் 1541 ல் பிறந்ததாகக் கூறுகிறார்.

பீர்பால் இளமைப் பருவத்திலேயே கவிதைகள் புனைந்து பாடுவதில் வல்லவராக விளங்கினார். காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதரின் திருமகளை மணம் செய்து கொண்ட பிறகு தான் ஓரளவு சுகமாக வாழ்ந்து வந்தார். மாமனார் இறந்ததும் அவரே சமஸ்தானத்து தலைமைப் பண்டிதரானார். அவர் அறிவுத்திறனை அறிந்த மக்கள் அவரைப் புகழ்ந்தனர். புதிய பண்டிதரின் புகழ் சமஸ்தானமெங்கும் பரவியது.

காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதராக இருந்த பீர்பால் என்ன காரணத்தினாலோ அந்தப் பதவியைத் துறந்து விட்டு ஆக்ராவுக்கு வந்து சேர்ந்தார். அங்கு ராம்சந்த் என்ற ஒரு செல்வரின் உதவியால் புரோகிதத் தொழிலைச் செய்து வந்தார்.

பீர்பால் ஆக்ராவில் இருக்கும் போது அவரது நகைச்சுவைப் பேச்சையும், அறிவுத் திறனையும் கண்ட ஆக்ரா மக்கள் அவரைப் புகழ்ந்தனர். இது அக்பரது செவிகளிலும் விழுந்தது. அக்பரது இந்துக்களிடம் அளவு கடந்த அன்பு. அக்பருடைய அரசவையில் கல்வியில் சிறந்த அறிவாளிகள் பலர் இருந்தனர். ஆனால் நகைச்சுவையாகப் பேசிச் சிரிக்க வைக்க ஒரு விகடகவி இல்லை.

ஒரு நாள் ஆக்ரா வீதியில் பாட்டுப் பாடி பிச்சை எடுத்துக்  கொண்டு இருந்த ஏழை பிராமணனான பீர்பாலை அக்பர் பார்க்க நேர்ந்தது. பீர்பாலின் பாடலும், வேடிக்கைப் பேச்சும் அக்பரின் மனதைக் கவர்ந்து விட்டன. அப்போதே பீர்பாலுடன் அவர் நட்புக் கொண்டு விட்டார். அதன் பின்னர் அவர் அக்பரிடம் வேலைக்கு அமர்ந்ததே ஒரு வேடிக்கையான கதை.

அரசவையில் பீர்பாலுடைய விகடப் பேச்சுகளும், நகைச்சுவையான உரையாடல்களும் அக்பரின் உள்ளத்தைப் பெரிதும் கவர்ந்தன. இதன் காரணமாக அக்பரின் அரசவையில் பீர்பால் சிறப்பு மிக்க ஓர் இடத்தைப் பெற்று விட்டார்.

# மாமரம் யாருடையது?

ஆக்ரா நகருக்கு அருகிலுள்ள ஓர் ஊரில் கேசவன் என்றொருவன் இருந்தான். அவன் பரம ஏழை. கூலி வேலை செய்து பிழைப்பவன். அவனுக்கு மாம்பழங்கள் சாப்பிட வேண்டும் என்ற ஆசை உதித்தது. பழம் வாங்குவதற்கு அவனிடம் காசு கிடையாது.

"நாமே ஒரு மாமரம் வைத்து பயிராக்கினால் சில வருடங்கள் கழித்து நிறைய பழங்கள் பழுக்கும். அதில் இருந்து கிடைக்கும் பழத்தில் நாமும் நம் குடும்பத்தினருடன் ஆசை தீரத் சாப்பிட்டு விட்டு மீதியை விற்பனையும் செய்யலாம்" என்று நினைத்தான். ஆனால் மாமரம் பயிரிடுவதற்கு அவனுக்கு சொந்த நிலம் எதுவும் கிடையாது. எனவே அவன் ஊருக்கு வெளியே உள்ள புறம்போக்கு நிலத்தில் எங்கிருந்தோ கஷ்டப்பட்டுக் கொண்டு வந்த மாஞ்செடியை நட்டான். ஆடு மாடுகள் மேய்ந்து விடாத  படி அதைச் சுற்றிலும் வேலி அமைத்தான். நாள்தோறும் தண்ணீர் ஊற்றி அதைக் காத்து வந்தான். நாளைடைவில் அந்த  மாஞ்செடி பெரியதாகி பூத்துக் காய்த்தது.

கேசவன் மாஞ்செடிக்கு தண்ணீர் ஊற்றி விட்டுச் செல்வதையும், அதைக் கவனமாகப் பேணி வளர்த்து வருவதையும் சோமன் என்பவன் பார்த்துக் கொண்டிருந்தான். கேசவன் சென்ற பிறகு அவன் மாஞ்செடியை கவனிப்பது போல் நாள்தோறும் பாசாங்கு செய்து விட்டுப் போனான்.

முடிவில் மரத்தில் பழுத்துள்ள பழங்களை பறிப்பதற்காக ஆவலுடன் கேசவன் வந்தபோது சோமன் ஓடி வந்து அவனைத் தடுத்தான். "திருட்டுப்  பயலே, மாம்பழங்களைத் திருடவா வந்தாய்? மரியாதையாக ஓடிப்போ" என்று கத்தினான். இதைக் கேட்டதும் கேசவன் வெலவெலத்து நின்று விட்டான். இதற்குள் அவர்களைச் சுற்றி கூட்டமும் கூடி விட்டது.

கேசவன் மாமரம் தன்னுடையது தான் என்று கூறினான். ஒருவரும் அவன் பேச்சை நம்பவில்லை. "கூலி வேலை செய்து பிழைப்பவனுக்கு மாமரம் எப்படிச் சொந்தமாயிருக்கும்?" என்று அவர்கள் நினைத்தார்கள். "நீ தினந்தோறும் வந்து இந்த மரத்தை பார்த்து விட்டுச் செல்வதில் இருந்தே சந்தேகப்பட்டேன். என்னுடைய சந்தேகம் சரியாகப் போய் விட்டது. மரியாதையாக ஓடிப் போய்விடு" என்று கத்தினான் சோமன்.

"நான் நாள்தோறும் துன்பப்பட்டு தண்ணீர் ஊற்றி வளர்த்து வந்தேன். இப்பொழுது மரம் உன்னுடையது என்று சொல்லுகிறாயா? என்றான் கேசவன். "தண்ணீர் ஊற்றியதால் மரம் உனக்குச் சொந்தமாகி விடுமா? யாரைக் கேட்டு தண்ணீர் ஊற்றினாய்?" என்று கோபத்துடன் கூவினான் சோமன்.

கூட்டத்தில் இருந்தவர்களுக்கு சோமன் சொல்லுவதே உண்மையாக இருக்கும் என்று தோன்றியது. ஆயினும் அந்தக் கூட்டத்தில் இருந்த ஒரு முதியவர், "இவ்வளவு வம்பு ஏன்? நமது பீர்பாலிடம் வழக்கைச் சொன்னால் அவர் யார் மாமரத்திற்கு உண்மையான சொந்தக்காரன் என்று கண்டுபிடித்து விடுவார்" என்றார்.

"ஓ, அப்படியே செய்யலாம்" என்று கூறியவாறு முன்னால் நடந்தான் சோமன். கேசவனும் அவனுடன் சென்றான். இருவரும் பீர்பாலிடம் சென்றனர். "ஐயா, நான் கஷ்டப்பட்டு பயிராக்கி வளர்த்த மாமரத்தில் இருந்து இவன் பழங்களை பறிக்க வந்தான். கேட்டதற்கு மாமரமே தன்னுடையது என்கிறான்" என்று சோமன் பீர்பாலிடம் கூறினான்.

"இல்லை. அந்த மாமரம் என்னுடையது தான். நான் தான் அதை நட்டுப் பயிராக்கி நாள்தோறும் தண்ணீர் ஊற்றி வளர்த்து வந்தேன்" என்றான் கேசவன். இருவர் சொல்வதையும் கேட்ட பீர்பாலுக்கு யார் கூறுவது உண்மை என்று தெரியவில்லை. அந்த மாமரம் உள்ள இடத்துக்கு அருகே குடியிருப்பவர்களைக் கூப்பிட்டு விசாரித்தார் பீர்பால்.

அவர்கள், "இருவரையும் தான் நாள்தோறும் மரத்தடியில் பார்த்தோம். யார் மரத்திற்கு உண்மையான சொந்தக்காரர் என்று தெரியாது" என்று கூறினார்கள். அவர்கள் கூறியதைக் கேட்ட பீர்பால், கேசவனையும், சோமனையும் பார்த்து, "நாளைய தினம் நீங்கள் வாருங்கள். அப்பொழுது மரத்திற்கு யார் உண்மையான சொந்தக்காரன் என்று கூறுகிறேன்" என்றார். இருவரும் தங்கள் வீடுகளுக்குச் சென்று விட்டார்கள்.

அன்று மாலை பீர்பால் ஒரு சேவகனை சோமன் வீட்டுக்கு அனுப்பி, அவனுடைய மாமரத்தில் திருடர்கள் காய்களையும், பழங்களையும் பறித்துச் செல்வதாகக் கூறி விட்டு என்ன நடக்கிறதென்று பார்த்து வந்து கூறு" என்றார்.

பீர்பால் அனுப்பிய சேவகன் சோமன் வீட்டுக்குச் சென்றான். அப்பொழுது சோமன் வீட்டில் இல்லை. மாமரத்தில் காய்களையும், பழங்களையும் திருடர்கள் பறித்துச் செல்கிறார்கள் என்பதைக் கேட்டும் கூட சோமனுடைய மனைவி அலட்சியமாக இருந்து விட்டாள். இதை பீர்பாலிடம் வந்து கூறினான் அந்த சேவகன்.

இதே மாதிரி கேசவன் வீட்டிலும் போய்ச் சொல்லுமாறு கூறினார் பீர்பால். சேவகன் கேசவன் வீட்டுக்குச் சென்ற போது அவனும் வீட்டில் இல்லை. செய்தியைக் கேட்ட அவனுடைய மனைவி, "ஐயோ கடவுளே! இவ்வளவு நாளாக அரும்பாடு பட்டு அவர் மரத்தை வளர்த்து வந்தார். பலன் கிடைக்கும் போது திருடர்கள் பழங்களையும், காய்களையும் திருட வந்து விட்டார்களா?" என்று ஆத்திரத்துடன் கூறியவாறே மாமரம் இருக்கும் இடத்தை நோக்கிச் சென்றாள் கேசவனின் மனைவி.

இதையும் சேவகன் வந்து பீர்பாலிடம் கூறினான். இந்த நிகழ்ச்சிகளை வைத்துக் கொண்டு மட்டும் மரத்திற்கு யார் உண்மையான சொந்தக்காரன் என்பதை தீர்மானித்து விடக் கூடாது என்று கருதிக் கொண்டார் பீர்பால்.

மறுநாள் கேசவனும், சோமனும் பீர்பால் வீட்டுக்கு வந்தனர். "நானும் வெகு நேரம் யோசனை செய்து பார்த்தேன். நீங்கள் இருவரும் கூறுவது எனக்கு உண்மையாகவே படுகிறது. எனவே நீங்கள் இருவருமே அந்த மாமரத்திற்கு சொந்தக்காரர்கள் என்று தீர்ப்பளிக்கிறேன். அதில் காய்த்துள்ள காய்களையும், பழங்களையும் நீங்கள் இருவருமே சரிபாதியாகப் பங்கிட்டு எடுத்துக் கொள்ள வேண்டும்" என்று பீர்பால் கூறியதும் சோமன் மகிழ்ச்சி அடைந்தான்.

எவ்வித உழைப்பும் இல்லாமல் மாமரத்தில் உள்ள காய்களிலும், பழங்களிலும் சரிபாதி அவனுக்கு கிடைக்கப் போவது அவனுக்கு மகிழ்ச்சியை அளித்தது. "அதுவும் தவிர, தகராறுக்குரிய அந்த மாமரத்தை வெட்டி இருவரும் சரி சமமாகப் பங்கிட்டுக் கொள்ள வேண்டும்" என்று பீர்பால் தீர்ப்பளித்தார்.

இதைக் கேட்டதும் சோமன், "நல்ல தீர்ப்பு அளித்தீர்கள்" என்று மகிழ்ச்சியுடன் கூறினான். ஆனால் கேசவனோ, "ஐயா, தயவு செய்து மரத்தை மட்டும் வெட்ட வேண்டாம். குழந்தையைப் போல காப்பாற்றி வளர்த்தேன். எனக்கு அதில் இருந்து எவ்விதப் பலனும் கிடைக்காவிட்டாலும் கூடப் பாதகமில்லை. மரத்தை மட்டும் வெட்ட வேண்டாம். சோமன் எல்லாப் பலனையும் அனுபவிக்கட்டும்" என்று துக்கம் தொண்டையை அடைக்கச் சொன்னான்.

இதில் இருந்து மரத்தின் உண்மையான சொந்தக்காரனை கண்டு பிடித்து விட்ட பீர்பால் சோமனுக்குப் பன்னிரண்டு கசையடிகள் கொடுக்குமாறு சேவகர்களுக்கு ஆணையிட்டார்.
