---
title : 'உடையாத கவசம்'
publishedDate: 'Wed Mar 23 2022 14:55:11 GMT+0530'
category: 'akbar-beerbaal-stories'
slug: 'udaiyaatha-kavasam'
thumbnail: '../thumbnails/kavasam.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - akbar beerbal stores
    - siru kadhaigal
    - moral stories
    - naneri kathaigal
    - tamil short stores
    - stories in tamil
keywords: 
    - akbar beerbal stories
    - akbar beerbal
    - beerbal
    - tamil tories
    - moral stories in tamil
    - tamil kathaigal
---
 
# அக்பர் பீர்பால் கதைகள் - தமிழ் நன்னெறிக் கதைகள்

# உடையாத கவசம்

## ஜலாலுத்தீன்  முகம்மது அக்பர்:

மொகலாயப் பேரரசர்களில் பெருஞ் சிறப்பு வாய்ந்தவர் ஜலாலுத்தீன் முகம்மது அக்பர் ஆவார். அவருடைய பாட்டனார் பாபர் என்பவர் இப்ராஹிம் லோடியை வென்று மொகலாயப் பேரரசை 1526 - ல் தோற்றுவித்தார். அவர் நான்கு ஆண்டு காலம் தான் அரசாட்சி செய்தார். அதற்குப் பிறகு அக்பருக்குத் தந்தையான உமாயூன் என்பவர் 1530 ல் அரியணை ஏறினார். பத்தாண்டு காலம் தொடர்ந்தார் போல் அரசாட்சி நடத்துவதற்கு அவர் பட்டபாடு கொஞ்ச நஞ்சமல்ல. அந்தச் சமயத்தில் சூர் வம்சத்தைச் சேர்ந்த ஷெர்கான் என்பவன் உமாயூனுக்கு விரோதமாகக் கிளம்பி அவரை முறியடித்து விட்டு அரசைக் கைப்பற்றிக் கொண்டான்.

ஷெர்கானால் தோற்கடிக்கப்பட்ட உமாயூன் நாடு நகரம், சொத்து சுகம் அனைத்தையும் இழந்து, பகைவர்க்கு அஞ்சி அமர்கோட் ராணாவிடம் தஞ்சம் புகுந்தார். அங்கு தான் ஜலாலுத்தீன் முகம்மது அக்பர் 1542 ல் பிறந்தார்.

உமாயூன் பதினைந்து ஆண்டுகளுக்குப் பிறகு பெரும் படையுடன் 1555 ல் ஷெர்ஷாவின் மேல் படை எடுத்தார். அப்பொழுது அக்பருக்கு வயது பதின்மூன்று. உமாயூன் தன்னுடைய படையை இரு பிரிவுகளாகப் பிரித்து ஒரு பிரிவுக்கு சிறுவன் அக்பரையும் அவனுக்குத் துணையாக தளபதி பைராம்கானையும் அனுப்பினார். இம்முறை உமாயூன் வெற்றிவாகை சூடினார். இழந்த நாட்டை திரும்பப் பெற்று மீண்டும் 1555 ல் பட்டம் சூட்டிக் கொண்டார். அதற்கு பிறகு ஓராண்டு காலம் தான் உமாயூன் உயிரோடு வாழ்ந்தார். அவர் இறந்ததும்  பதினான்கு வயதுச் சிறுவனான அக்பர் 1556 ல் அரியணை ஏறினார். அவருக்குத் துணையாக தளபதி பைராம்கான்  ஆரம்பத்தில் துணை புரிந்தார்.

அக்பர் தன்னுடைய ஆட்சிக் காலத்தின் போது இந்து முஸ்லீம் ஒற்றுமைக்காக மிகவும் பாடுபட்டார். இந்துக்கள் பால் சக்கரவர்த்தி அன்பு காட்டியதற்கு காரணம் பீர்பால் தான் என்று நினைக்கத் தோன்றுகிறது. 

பீர்பாலுடன் சேர்த்து மொத்தம் ஒன்பது அறிஞர்கள் அக்பரின் அரசவையை அலங்கரித்தார்கள். இவர்களை "நவரத்தினங்கள்" என்று குறிப்பிடுவார்கள். ராஜா பீர்பால், ராஜா மான்சிங், ராஜா தேடர்மால், ஹகீம் ஹிமாம், முல்லா தோபியாஸா, பைஜி, அபுல்பசல், ரஹீம், மியான் தான்சேன் முதலியவர்கள் தான் இந்த நவரத்தினங்கள்.

மதங்களினால் தான் மக்கள் வேற்றுமை அடைந்து ஒருவருக்கு ஒருவர் வீணே சச்சரவு இட்டு கொள்கிறார்கள் என்று கருதிய அக்பர் அனைவரையும் ஒன்று சேர்க்கக் கருதி ஒரு புதிய மதத்தை தோற்றுவித்தார்.  இப்புதிய மதத்திற்கு "தீன்இலாஹி" என்பதாகும். 

அக்பர் தம்முடைய 63 வது வயதில் 1605 ல் "தான் பெற்ற மகனே தனக்கு விரோதியாக மாறி விட்டானே" என்ற மன வருத்தத்துடன் உயிர் துறந்தார். அவருடைய விருப்பத்திற்கிணங்க அவர் இறந்த போது எந்த மதத்தினருக்கு உரிய துதிப் பாடல்களும் அவருக்கு ஓதப்படவில்லை. அவருடைய சடலம் ஆடம்பரம் ஏதுமின்றி கல்லறையில் புதைக்கப் பெற்றது.

## ராஜா பீர்பால் 

ராஜா பீர்பாலுடைய இயற்பெயர் மகேஷ்தாஸ் என்பது ஆகும். சிலர் பிரம்மதாஸ் என்றும் குறிப்பிடுகிறார்கள். பீர்பால் என்றால் வீரர்களில் பலம் பொருந்தியவன் என்பதாகும். இந்தப் பெயரை இவருக்குச் சூட்டியவரும் அக்பர் சக்கரவர்த்தி தான்.

பீர்பால் கோதாவரி நதிக் கரையிலுள்ள மார்ஜால் என்ற ஊரில் பிறந்ததாகச் சிலர் கூறுகிறார்கள். அப்துல் காதர் பதௌனி என்ற சரித்திராசிரியர் இவர் கால்பி என்ற ஊரில், ஓர் ஏழைப் பிராமணக் குடும்பத்தில் 1541 ல் பிறந்ததாகக் கூறுகிறார்.

பீர்பால் இளமைப் பருவத்திலேயே கவிதைகள் புனைந்து பாடுவதில் வல்லவராக விளங்கினார். காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதரின் திருமகளை மணம் செய்து கொண்ட பிறகு தான் ஓரளவு சுகமாக வாழ்ந்து வந்தார். மாமனார் இறந்ததும் அவரே சமஸ்தானத்து தலைமைப் பண்டிதரானார். அவர் அறிவுத்திறனை அறிந்த மக்கள் அவரைப் புகழ்ந்தனர். புதிய பண்டிதரின் புகழ் சமஸ்தானமெங்கும் பரவியது.

காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதராக இருந்த பீர்பால் என்ன காரணத்தினாலோ அந்தப் பதவியைத் துறந்து விட்டு ஆக்ராவுக்கு வந்து சேர்ந்தார். அங்கு ராம்சந்த் என்ற ஒரு செல்வரின் உதவியால் புரோகிதத் தொழிலைச் செய்து வந்தார்.

பீர்பால் ஆக்ராவில் இருக்கும் போது அவரது நகைச்சுவைப் பேச்சையும், அறிவுத் திறனையும் கண்ட ஆக்ரா மக்கள் அவரைப் புகழ்ந்தனர். இது அக்பரது செவிகளிலும் விழுந்தது. அக்பரது இந்துக்களிடம் அளவு கடந்த அன்பு. அக்பருடைய அரசவையில் கல்வியில் சிறந்த அறிவாளிகள் பலர் இருந்தனர். ஆனால் நகைச்சுவையாகப் பேசிச் சிரிக்க வைக்க ஒரு விகடகவி இல்லை.

ஒரு நாள் ஆக்ரா வீதியில் பாட்டுப் பாடி பிச்சை எடுத்துக்  கொண்டு இருந்த ஏழை பிராமணனான பீர்பாலை அக்பர் பார்க்க நேர்ந்தது. பீர்பாலின் பாடலும், வேடிக்கைப் பேச்சும் அக்பரின் மனதைக் கவர்ந்து விட்டன. அப்போதே பீர்பாலுடன் அவர் நட்புக் கொண்டு விட்டார். அதன் பின்னர் அவர் அக்பரிடம் வேலைக்கு அமர்ந்ததே ஒரு வேடிக்கையான கதை.

அரசவையில் பீர்பாலுடைய விகடப் பேச்சுகளும், நகைச்சுவையான உரையாடல்களும் அக்பரின் உள்ளத்தைப் பெரிதும் கவர்ந்தன. இதன் காரணமாக அக்பரின் அரசவையில் பீர்பால் சிறப்பு மிக்க ஓர் இடத்தைப் பெற்று விட்டார்.

# உடையாத கவசம்

ஒரு நாள் அக்பரின் அரசவைக்கு புகழ் பெற்ற கருமான் ஒருவன் வந்தான். அவன் அரசர்களுக்கு இரும்புக் கவசங்கள் செய்து தருவதில் வல்லவன். அவனிடம் பல மன்னர்கள் போர்க்களம் செல்லும் போது தாங்கள் அணிந்து செல்லக் கூடிய இரும்புக் கவசங்களை விலைக்கு வாங்கிக் கொள்வார்கள்.

அந்தக் கருமான் அக்பரிடம் விற்பதற்காக எஃகினால் செய்யப்பட்ட அழகிய கவசம் ஒன்றைக் கொண்டு வந்து இருந்தான். அந்தக் கவசம் அழகிய முறையில் மிகச்  சிறப்பாகச் செய்யப்பட்டு இருந்தது. அதன் அழகைக் கண்டு மயங்கிய அக்பர் பாதுஷா, "இந்தக் கவசம் எவ்வளவு அழகாகச் செய்யப்பட்டு  இருக்கிறது, பார்த்தீர்களா?" என்று அரசவையில் இருந்தவர்களிடம் கூறினார்.

அக்பர் கூறியதைக் கேட்டதும் மிகவும் கர்வம் அடைந்து இருந்த அந்தக் கருமான், "அரசே, இந்தக் கவசம் அழகாக மட்டும் செய்யப்பட்டது அல்ல. மிகச் சிறப்பான முறையில் உறுதியாகவும் செய்யப்பட்டது. இதை எவ்விதம் வேண்டுமானாலும் சோதனை செய்து பார்க்கலாம்" என்று கூறினான்.

அப்படியா! என்று வியந்த அக்பர் சக்கரவர்த்தி பக்கத்தில் இருந்த வீரன் ஒருவனை அழைத்து அந்தக் கவசத்தை மீது அவனுடைய உடைவாளை வீசச் சொன்னார்.

வீரன் அக்பர் கூறியபடியே தன் உடைவாளை அந்தக் கவசத்தின் மீது ஓங்கி வீசினான். உடனே அந்த இரும்புக் கவசம் துண்டு துண்டாக உடைந்து விட்டது. அதனைக் கண்ட அக்பர் சக்கரவர்த்திக்கு மிகவும் கோபம் வந்து விட்டது.

நீ செய்து கொண்டு வந்திருக்கும் கவசத்தின் மீது உடைவாளை வீசியதும் துண்டு துண்டாக உடைந்து போய் விட்டதே! இதை அணிந்த கொண்டா போர்க்களத்திற்குச் செல்லுவார்கள்! நீ என்ன கருமான்? சிறப்பாகவும், உறுதியாகவும் செய்யப்பட்டதாகக் கூறிய இந்தக் கவசத்தின் கதியே இப்படி ஆகி விட்டது என்றால் சாதாரணமாகச் செய்யப்படும் கவசம் எந்த மாதிரி இருக்கும் என்று சொல்லத் தேவை இல்லை. வீண் பேச்சு எதற்கு? இன்னும் உனக்கு முப்பது நாட்கள் தவணை தருகிறேன். அதற்குள் மிகச் சிறந்த உடையாத உறுதியுடைய கவசம் ஒன்று செய்து கொண்டு வர வேண்டும். அந்தக் கவசத்தின் மீது உடைவாளை எவ்வளவு வீசினாலும் உடையக் கூடாது. சோதனை செய்து பார்ப்பேன். கவசம் உடைந்ததோ உன் தலை உருண்டு விடும். எச்சரிக்கை ஆக இரு! என்றார் அக்பர்.

அந்தக் கருமானும் மிகவும் சிரத்தையுடன் முன் செய்ததை விட சிறந்ததாக புதிய கவசம் ஒன்றை இரவு பகலாக உழைத்து செய்து முடித்தான். அதை முடித்த பிறகு அவனுக்கு ஓர் ஐயம் தோன்றியது.

அரசர் இதனை சோதனை செய்து பார்க்கும் போது இதுவும் உடைந்து விட்டால் என் தலை உருண்டு விடுமே! சோதனையின் போது கவசம் உடையாமல் இருக்குமா? ஐயோ உடைந்து போய் விட்டால்? நினைக்கவே பயமாக இருக்கிறதே! என்று நடுங்கினான்.

கடைசியில் அவனுக்கு பீர்பாலின் நினைவு வந்தது. அவர் அக்பரிடம் அடைந்து உள்ள செல்வாக்கும், அவருடைய அறிவுக் கூர்மையும் அவன் நினைவுக்கு வந்தன.  அவரிடம் சென்று யோசனை கேட்டால் அவர் எதாவது சொல்ல மாட்டாரா? என்ற எண்ணத்துடன் பீர்பாலிடம் சென்றான். தன்னுடைய குறையை அவரிடம் முறையிட்டான்.

அந்தக் கருமான் கூறியதை மிகவும் அனுதாபத்துடன் கேட்டார் பீர்பால்.  முடிவில், நாளைக்கு நீ செய்து இருக்கும் புதிய கவசத்துடன் அரசவைக்கு வா. மேற்கொண்டு ஏதாவது நடந்தால் நான் பார்த்துக் கொள்கிறேன். பயப்படாமல் வீடு போய் சேர் என்று அவனுக்கு ஆறுதல் கூறி அவனை அனுப்பி வைத்தார்.

அடுத்த நாள், அரசவையில் கருமான் தான் செய்து கொண்டு வந்து இருந்த புதிய  கவசத்தை மாமன்னரிடம் காட்டினான்.

அதைக் கண்ட அக்பர் சக்கரவர்த்தி, சரி இந்தக் கவசத்தை நீயே அணிந்து கொள். நீ அணிந்து கொண்டு இருக்கும் கவசத்தின் மீது என் வீரனை உடைவாளால் வெட்டச் சொல்லுகிறேன். அப்போது இந்தக் கவசம் உடையாமல் இருந்தால் நீ பிழைத்தாய். இல்லை எனில் நான் தனியாக உனக்கு மரண தண்டனை தர வேண்டிய அவசியமும் இருக்காது. கவசம் உடைந்ததும் நீயே இறந்து விடுவாய், என்றார்.

கருமான் பயத்துடனும், நடுக்கத்துடனும் தான் செய்து கொண்டு வந்து இருந்த புதிய கவசத்தை அணிந்து கொண்டான். அக்பரின் ஆணைப்படி வீரன் உடைவாளால் அந்தக் கருமான் அணிந்து கொண்டு இருந்த கவசத்தின் மீது  ஒரே வெட்டாக வெட்டப்  போனான். அப்போது அந்தக் கருமான் சட்டென்று அந்தச் சேவைகனின் கையை கெட்டியாகப் பிடித்துக் கொண்டான்.

இதைக் கண்ட அக்பருக்கு சொல்ல முடியாத கோபம் வந்தது. என்ன காரியம் செய்தாய்? நான் என்ன சொன்னேன் என்பது உனக்கு தெரியவில்லையா? நீ அணிந்து கொண்டு இருக்கும் கவசத்தின் மீது உடைவாளை வீசி சோதனை செய்து பார்க்கச் சொன்னால், என் ஆணையை நிறைவேற்ற முடியாத படி ஏன் அவன் கையை கெட்டியாகப் பிடித்துக் கொண்டாய்? என்று மிகக் கோபமாக கருமானைப் பார்த்து கேட்டார்.

அப்போது அரசவையில் இருந்த பீர்பால் எழுந்து, அரசே! கவசத்தின் மூலம் மட்டும் வாள் வீச்சில் இருந்து உயிர் தப்ப முடியுமானால் கைகளை கட்டிக் கொண்டே வெறுங்கவசத்துடன் சண்டை போடலாம்.  ஆனால், அப்படியன்று, உயிருக்கு ஆபத்து வரும் போது கை தானாகவே உதவிக்கு வரும். இது இயற்கை. போர்க்களத்திலும் இதே கதை தான். எனவே, கருமான் செய்ததில் தவறொன்றும் இல்லை என்றார், அமைதியான குரலில்.

பீர்பால் சொன்ன விளக்கத்தைக் கேட்டதும் அக்பர் மகிழ்ந்து தம் கருத்தை மாற்றிக் கொண்டு அந்தக் கருமானுக்கு பல பரிசுகள் அளித்து கவசத்தைப் பெற்றுக் கொண்டார்.
