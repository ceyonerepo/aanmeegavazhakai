---
title : 'நீங்கள் வாழ்வில் சுபிக்சம் பெற உங்கள் வீட்டின் பூஜை அறை மற்றும் சமயலறையை இவ்வாறு அமைத்து பாருங்கள்!'
publishedDate: 'Sat Apr 16 2022 12:52:32 GMT+0530'
category: 'vasthu'
slug: 'poojai-arai-samayal-arai'
thumbnail: '../thumbnails/pooja-room.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - vasthu
    - aanmeegam
    - nambikai
    - vaasthu saasthiram
keywords:
    - vasthu tamil
    - vasthu in tamil
    - manaiyadi shastra vastu in tamil
---

# நீங்கள் வாழ்வில் சுபிக்சம் பெற உங்கள் வீட்டின் பூஜை அறை மற்றும் சமயலறையை இவ்வாறு அமைத்து பாருங்கள்!

## வாஸ்து சாஸ்திரம் என்றால் என்ன?

1. நமது வீடு எப்படி இருக்க வேண்டும்?
2. வீடு காட்டும் முறைகள் 
3. எப்படி வீடு இருந்தால் என்ன பலன் கிடைக்கும்
என்பதை விளக்கி சொல்வது வாஸ்து சாஸ்திரம் ஆகும்.

தனக்கு என்று ஒரு வீடு வேண்டும் என்ற ஆசை எல்லோருக்கும் இருக்கிறது. கூழோ கஞ்சியோ நம் வீட்டில் குடித்தால் அதன் திருப்தியே தனி தான். ஆனால் வீடு கட்டும் போது பலர் முக்கியமான வாஸ்து கூட பார்க்காமல் கட்டி விட்டு பின் அவதிப் படுகிறார்கள். ஆபத்து வந்த பின் காப்பதை விட வரும் முன்னே காப்பது நல்லது அல்லவா!

## வாஸ்து:

வாஸ்து என்பது மனையடி சாஸ்திரத்தின் இதயம் போன்றது ஆகும். வாஸ்து எனப்படும் சமஸ்கிருத சொல்லுக்கு வீடு கட்டக் கூடிய மனை என்று பொருள் படும். இது தனி சாஸ்திரமாக சிலரால் கருதப்படுகிறது. இந்த சாஸ்திரத்தின் நாயகனாக தெய்வாம்சம் பொருந்திய ஒரு பெண் கருதப்படுகிறது.

உயிரற்ற ஜடப் பொருட்களான செங்கல், சிமெண்ட், மணல், கம்பிகள், வெட்டப்பட்ட மரம் போன்ற வஸ்துகளை சரியான விகிதத்தில் கலந்து நிலத்தின் மேல் கட்டிடமாக எழுப்பி உயிரோட்டம் கொடுப்பதே வாஸ்து.

வாஸ்து உண்மையில்லை, வாஸ்து ஏமாற்று வித்தை, வாஸ்து பொய் என்று சமூக அக்கறையோடு பதிவு செய்யப்படும் குற்றச்சாட்டுகளுக்கு என் பதில், 

வாஸ்து என்பது உண்மை, வாஸ்து என்பது அறிவியல், வாஸ்து என்பது மத சம்மந்தப்பட்ட விஷயம் அல்ல. வாஸ்துவில் யந்திரம், மந்திரம், பூஜை, தாயத்து மற்றும் கண் கட்டு வித்தைகளுக்கு வேலை இல்லை.

பாம்பு கடிக்கு விஷமுறிவு மருந்து மற்றொரு பாம்பின் விஷம் தான் என்பது உண்மையாக இருப்பின் வீட்டில் உள்ள பிரச்சனைகளுக்கு தீர்வு அந்த வீட்டிலேயே தான் உள்ளது என்பது தான் நான் கண்டா உண்மை.

## வாஸ்து விதிகள்:

வாஸ்து சாஸ்திரம் என்பது ஒவ்வொரு மனித வாழ்வின் வெற்றிக்கும் அடித்தளமாக விளங்கக் கூடிய ஒன்று ஆகும். வாஸ்து விதிகளின் படி ஒரு வீடோ, தொழிற்சாலையோ அமைக்கப்படும் போது அங்கு இருக்கக் கூடிய அனைவருக்கும் எப்பொழுதும் நல்ல ஆற்றலே இருக்கும். என்றும் மன அமைதி, உடல் நலம், செல்வ செழிப்போடு காணப்படுவர். வாஸ்து என்றால் நான்கு திசை மற்றும்  நான்கு மூலைகள் கொண்டே கருதப்படுகின்றன. அவை வடக்கு, கிழக்கு, தெற்கு, மேற்கு. மேலும் நான்கு மூலைகள் வடகிழக்கு, தென்கிழக்கு, வடமேற்கு, தென்மேற்கு ஆகும். இவற்றை கொண்டு வாஸ்துவில் அடிப்படையாக கருதப்படும் ஆறு மிக முக்கியமான பொதுவான விதிகள்.

## வாஸ்துவின் பொதுவான விதிகள்:

1. மனை மற்றும் அதனுள் கட்டப்படும் கட்டிடம் இரண்டும் சதுரம் அல்லது செவ்வகமாக இருக்க வேண்டும்.

2. வடக்கு  மற்றும் கிழக்கில் அதிக காலி இடம் இருத்தல் அவசியம்.

3. தலை வாசல் என்றுமே உச்சத்தில் தான் அமைக்கப்பட வேண்டும்.

4. தெருத் தாக்கம் (தெருக்  குத்து) இருந்தால் கண்டிப்பாக உச்சமாக தான் இருக்க வேண்டும்.

5. உட்புறம் மற்றும் வெளிப்புறம் போடப்படும் படிக்கட்டுகள் அமைக்கப்படும் முறையை அறிவது அவசியம்.

6. வடகிழக்கு பள்ளமாகவும், கனமில்லாமலும், தென்மேற்கு உயரமாகவும், கனமாகவும் இருத்தல் அவசியம்.

வாஸ்து சாஸ்திரத்தைப் பற்றி இன்னும் பல அறிய நூல்கள் உள்ளன. சிற்ப நகரம், சிற்ப ரத்தினம், மயமதம், காஸ்யபம், ஸர்வார்த்த மனுசாரம், வாஸ்து வித்யா போன்ற நூல்கள் சிலை வடிப்பது, ஆலயம் அமைப்பது, வீடு கட்டுவது போன்ற அறிய கலைகளை பற்றி விவரிக்கின்றன.

## நீங்கள் வாழ்வில் சுபிக்சம் பெற உங்கள் வீட்டின் பூஜை அறை மற்றும் சமயலறையை இவ்வாறு அமைத்து பாருங்கள்!

## பூஜை அறை 

ஒரு வீட்டில் சுப நிகழ்ச்சிகள் நிகழ்ந்தால் முதலில் நன்றி சொல்வது இறைவனுக்கு தான். எங்கும் நிறைந்து இருக்கும் பரம் பொருளான இறைவனை வீட்டில் நாம் வைத்து வணங்குவது பூஜையறையில் தான். கஷ்டங்கள் வந்தால் இறைவா ஏனிந்த நிலை என புலம்புகிறோம். மன நிம்மதி வேண்டி நாம் சென்று மனதார கை கூப்பி வணங்கி ஐந்து நிமிடங்கள் இறைவனிடம் நம் குறைகளை இறக்கி வைக்கும் இடமும் பூஜை அறை தான். நம்மோடு வாழ்ந்து இறைவனடி சேர்ந்தவர்களையும் பூஜை அறையில் வைத்து தான் இறைவனாக இருந்து நல் வழி காட்டும் படி வேண்டிக் கொள்கிறோம். தினமும் பூப் போட்டு விளக்கேற்றி ஊதுபத்தி ஏற்றி, சாம்பிராணி புகை போட்டு நம்மால் முடிந்த அளவிற்கு ஏதாவது பொங்கலோ அல்லது ஏதாவது ஒரு பழ வகையோ வைத்து தினமும் வழிபடுகிறோம்.

## வீடு சுபிக்சம் அடைய 

நாம் வாழும் வீடானது சுபிட்சங்கள் நிறைந்ததாகவும், மகழ்ச்சி தரமானதாகவும் இருப்பதற்கு பூஜைகள் செய்வது, பிரார்த்தனை செய்வது, கடவுளை வணங்குவது போன்றவை ஓர் சிறந்த வழியாகும். அதிலும் வாஸ்து ரீதியாக பூஜை அறையை எங்கு அமைக்க வேண்டுமோ அந்த இடத்தில் அமைத்து வழிபட்டால் வீடு எப்பொழுதும் லட்சுமி கடாட்சம் நிறைந்ததாக இருக்கும்.

## வாஸ்து படி பூஜை அறை 

நம் வீட்டின் பூஜை அறையை வாஸ்து ரீதியாக எங்கு அமைக்க வேண்டும்  என பார்க்கின்ற போது ஈசானே குடியிருக்கக் கூடிய ஈசான்ய மூலையில் அதாவது வட கிழக்கு மூலையில் அமைப்பது மிகவும் சிறப்பு. கட்டிடம் அமைக்கும் இடம் பெரியதாக இருந்து தனியாக பூஜை அறை அமைக்கும் பட்சத்தில் வட கிழக்கு மூலையில் பூஜை அறையை வைத்து அந்த பூஜை அறையின் மேற்கு சுவற்றில் மேடை அமைத்து சுவாமி படமானது கிழக்கு நோக்கி இருக்கும் படி வைத்தால் நாம் சுவாமியை வணங்கும் போது மேற்கு நோக்கி நின்று வணங்குவோம். இப்படி வணங்குவது குடும்பத்திற்கு மிகவும் நல்லது.

## பூஜை அறையின் நீள அகல அளவுகள் 

வீட்டில் ஏற்படக் கூடிய மகா லஷ்மி விளக்கை கிழக்கு அல்லது வடக்கு பார்த்தது போல ஏற்றுவது நல்லது. குறிப்பாக வட கிழக்கு மூலையில் பூஜை அறை அமைக்கும் போது பூஜை அறையின் அளவானது அகலம் மற்றும் நீளமாக 6, 8, 10, 11 அடிகளாக இருப்பது மிகவும் நல்லது.

## எந்த இடத்தில் பூஜை அறை இருக்கக் கூடாது 

கை கால் நீட்டி படுப்பதற்கே இடமில்லை. இதில் பூஜை அறைக்கென்று தனி இடத்திற்கு எங்கு செல்வது என நினைப்பவர்கள் முடிந்த வரை கிழக்கு பார்த்தது போல வட கிழக்கில் சுவாமி மேடை அமைத்து அதில் படங்களை வைத்து வழிபடுவது நல்லது. முடிந்த வரை சுவாமி கும்பிடும் அறை அல்லது இடத்திற்கு அருகில் அல்லது பக்கத்தில் கழிவறையோ குளியலறையோ இல்லாது இருப்பது நல்லது. குறிப்பாக மாடி படிக்கட்டுகளுக்கு கீழே பூஜை அறை  இல்லாது இருப்பது மிக மிக உத்தமம்.

## சமையலறை எங்கு அமைக்க வேண்டும் 

ஒரு வீட்டில் சமையல் அறை  எங்கு அமைத்தல் சிறப்பான பலன்கள் ஏற்படும் என்பதினை வாஸ்து சாஸ்திர ரீதியாக பார்த்தால் அக்னி மூலை என வர்ணிக்கப்பட கூடிய தென் கிழக்கு மூலையில் அமைப்பது மிக சிறப்பு. தென் கிழக்கு மூலையில் அமைக்கும் அறையில் எப்படிப்பட்ட அமைப்பு இருந்தால் சிறப்பான பலன்கள் ஏற்படும் என பார்த்தால் தென் கிழக்கு அறையில் கிழக்கு சுவற்றில் அதுவும் தெற்கை ஒட்டிய கிழக்கு சுவற்றில் சமையல் மேடை அமைக்க வேண்டும். அந்த மேடையில் அடுப்பு வைத்து கிழக்கு பார்த்து சமைப்பது போல் சமையலறை இருக்க வேண்டும். சமையல் மேடைக்கு மேல் கிழக்கு சுவற்றில் சன்னல் அல்லது வெண்டிலேட்டர் அமைத்து சமையல் அறைக்குள் சூரிய ஒளி வருவது போல் அமைப்பது மிகவும் சிறப்பு.

சமையல் மேடைக்கு அருகில் கிழக்கு சுவற்றில் அதுவும் வடக்கை ஒட்டிய கிழக்கு சுவற்றில் பாத்திரம் கழுவுவதற்காக தொட்டி அமைப்பது. வட கிழக்கு பகுதியில் தண்ணீர் வருவது போல வைப்பது மிக சிறப்பு. குறிப்பாக சமையல் அறையில் வட கிழக்குப் பகுதியில் பலமான பொருட்கள் எதுவும் வைக்காமல் முடிந்த வரை காலியாக விட்டு விட்டு தண்ணீர் தேக்கி வைக்க கூடிய பாத்திரங்களை வைப்பது மிகவும் சிறப்பு.

## சமையல் அறையில் பொருட்களை ஒழுங்கு படுத்தல் 

சமையல் அறையில் முடிந்த வரை கனமான பாத்திரங்களை தென் மேற்குப் பகுதியில் வைப்பது மிக சிறப்பு. அதாவது கிரைண்டர். குளிர் சாதனப் பெட்டி ஆகியவற்றை தென் மேற்கு பகுதி அல்லது மேற்கு சுவரை ஒட்டிய பகுதி, வட மேற்கு பகுதியில் வைப்பது மிக சிறப்பு. சமையல் அறையில் செல்ப் ஆனது தெற்கு சுவர் மற்றும் மேற்கு சுவற்றில் அமைத்து அதில் சமையலுக்கு தேவையான பொருட்களை வைக்கலாம். செல்ப் ஆனது கிழக்கு மற்றும் வடக்கு சுவற்றில் அமைப்பது அவ்வளவு சிறப்பு அல்ல. 

ஒரு வீட்டில் சமையல் அறை ஆனது மேற்கூறியவாறு அமைப்பதன் மூலமாக அனுகூலமான பலன்கள் உண்டாகும். தென் கிழக்கு மூலையில் சமையல் அறை வைக்க சாத்தியமான சூழ்நிலை இல்லாத பட்சத்தில் அதாவது தெற்கு பார்த்த வீட்டிற்கு உச்ச ஸ்தானமான கிழக்கு ஒட்டிய தெற்கு பகுதியில் தலை வாசல் வைக்கும் பட்சத்தில் சமையலறை தென் கிழக்கில் வைக்க முடியாது. அப்போது வட மேற்கில் சமையல் அறை எப்படி அமைப்போமோ, தென் கிழக்கு அறையில் சமையல் அறை  வைத்தல் எப்படி அமைப்போமோ, அதே போல அமைப்பில் வட மேற்கில் அமைக்க வேண்டும்.

## இட நெருக்கடி இருக்கும் போது சமையல் அறை எப்படி அமைக்க வேண்டும் 

இட நெருக்கடிக் கொண்ட சென்னை போன்ற பெரு நகரங்களில் தென் கிழக்கில் அமைக்க முடியாத சூழ்நிலையில் மற்ற இடங்களில் சமயல் அறையை அமைத்தால் தெற்கை ஒட்டிய கிழக்கு பகுதியில் சமையல் அறை, கிழக்கு சுவற்றில் சமையல் மேடை அமைப்பது மிகவும் சிறப்பு. 

## எங்கு சமையல் அறை அமையக் கூடாது 

பொதுவாக தென் கிழக்கில் சமையல் அறையை அமைக்காமல் ஈசான்ய மூலை என வர்ணிக்கப்பட கூடிய வட கிழக்குப் பகுதியில் சமையல் அறை அமைத்தால் நல்லது அல்ல. நைரிதி என வர்ணிக்கப்பட கூடிய தென் மேற்கிலும் சமையல் அறை அமைக்கக் கூடாது. வட கிழக்கு பகுதி தெய்வீகமான ஸ்தானம் ஆகும். ஜல நடமாட்டம் கொண்ட ஸ்தானம் ஆகும். அங்கு அடுப்பு இருப்பதன் மூலமாக பல்வேறு கேடு பலன்கள் ஏற்படும். தென் மேற்கு பகுதியில் பணப்பெட்டி, முக்கியமான பொருட்கள், படுக்கையறை போன்றவை இருக்கும் ஸ்தானமாகும். அங்கு அடுப்பு வைத்தல் குடும்பத்தில் சுபிட்சங்கள் குறையும்.

## உணவின் முக்கியத்துவம் 

உண்ணும் உணவானது உடலுக்கு மிகுந்த அவசியம் ஆகும். உண்ணும் உணவை கூட வாஸ்து ரீதியாக எந்த திசையில் அமர்ந்து உணவு உண்பது என்ற விதியும் உள்ளது. உணவு உண்ணும் போது வடக்கு அல்லது கிழக்கு திசை நோக்கி உட்கார்ந்து உண்டால் நல்ல செரிமானம் ஆவதுடன் உடல் ஆரோக்கியமும் சிறப்பாக இருக்கும்.

## முடிவுரை:

வாஸ்து சாஸ்திரம் மற்றும் தர்ம சாஸ்திரம், ஜோதிட சாஸ்திரம் இணைந்து தான் ஒரு வீட்டைப் பற்றி முடிவு எடுக்க வேண்டும். வாஸ்து நூல்களில் இடையே சிறிய வித்தியாசங்கள் காணப்படுகின்றது. ஆலய வாஸ்து, கிரக வாஸ்துவை விட சிறிது மாறுபட்டது எந்த ஒரு வீடு சம்பந்தப்பட்ட முடிவு ஆயினும் அவசரம் இல்லாமலும் அடுத்தவர்களுக்கு எந்தப் பாதிப்பும் இல்லாமலும் அமைவது அவசியமாகும். பொதுவாக நல்ல வழிகாட்டி உதவியுடன் மாற்றங்களைச் செய்து கொள்வது நன்மை தரும். மேலும் அடுக்கு மாடி மற்றும் கூட்டுக் குடும்பங்களுக்கு மாற்றங்கள் செய்யும் போது சற்று கவனத்துடன் செய்ய வேண்டும். சில இடங்களில் மாடிப்படி, வாசல் படி எண்ணிக்கை கணக்குப் பார்க்கும் வழக்கம் உண்டு. இவ்வாறு பார்க்க வேண்டாம் என்று சொல்லும் வாஸ்து நிபுணர்களும் உண்டு. ஆக பலன்களை ஆராய்ந்து முடிவு எடுக்க இறைவனைப் பிரார்த்தனை செய்வோம்.
