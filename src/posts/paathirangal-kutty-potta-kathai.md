---
title : 'பாத்திரங்கள் குட்டி போட்ட கதை'
publishedDate: 'Fri Mar 04 2022 19:39:40 GMT+0530'
category: 'tenali-raman-stories'
slug: 'paathirangal-kutty-potta-kathai'
thumbnail: '../thumbnails/fighter.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - thenali ramang
    - siru kadhaigal
keywords: 
    - tenali raman
    - tenali raman stories
    - tenali rama krishna
    - tenaliraman
    - tenali ramakrishna stories
    - tenali raman stories in tamil
    - tenali raman short stories
---
 
# தெனாலி ராமன்
தெனாலி ராமன் என்றாலே அனைவருக்கும் நினைவிற்கு வருவது அவருடைய நகைச்சுவை பேச்சு தான். பேச்சு நகைச்சுவையாக இருக்குமே தவிர அவரது சிந்தனையும் திறன்களும் அளவிட முடியாத அளவுக்கு உயர்ந்தவையாகவே இருந்து இருக்கிறது. அவரது எந்த ஒரு நகைச்சுவை  பேச்சும் யாரையும் காயப்பட வைத்தது  இல்லை. மேலும் அவரது ஒவ்வொரு நகைச்சுவை கதைகளிலும் ஏதேனும் ஒரு உள் அர்த்தம் மற்றும் நமது அன்றாட வாழ்க்கையோடு தொடர்புடைய பல சிந்தனைகளை சுமந்தே வந்து உள்ளன. அத்தகைய கதைகளை நாம் படிக்கும் முன்னர் அத்தகைய தெனாலி ராமனை பற்றி இப்பகுதியில் சிறிது தெரிந்து கொள்வோமா!
 
## தெனாலி ராமன் பிறப்பு
 
தெனாலி ராமன் ஆந்திர மாநிலம் கிருஷ்ணா மாவட்டத்தில் கார்லபதி என்கிற கிராமத்தில் ராமையா - லட்சுமி அம்மாள் தம்பதியரின் மகனாக ஏழை பிராமண குடும்பத்தில் பிறந்தவர். தெனாலியில் உள்ள இராமலிங்க சுவாமியின் நினைவாக இராமலிங்கன் என்றே பெயரிடப்பட்டார். இவர் பிறந்து மூன்றாம் நாள் இவருடைய தந்தையார் மரணமடைய குடும்பம் வறுமையில் வாடியது. இவருடைய தாயார் இவரை எடுத்து கொண்டு தெனாலியில் இருந்து அவருடைய சகோதரனுடைய வீட்டுக்கு வந்து சேர்ந்தார். தாய் மாமன் ஆதரவில் தான் இராமலிங்கம் வளர்ந்தார்.
 
## தெனாலியின் கோமாளித் தனங்கள்
 
உரிய பருவத்தில் பள்ளியில் சேர்ந்தாலும் படிப்பில் கவனம் செல்லவில்லை. மற்றவர்களை சிரிக்க வைக்கும் ஆற்றல் அவரிடம் இயற்கையாகவே இருந்தது. அதனால் அகடவிகட கோமாளித் தனங்களில் தான் அவருடைய அறிவும் ஆற்றலும் ஜொலித்தன. கமலா என்கிற பெண்ணை மணந்தார் தெனாலி ராமன்.

## தெனாலி விகடகவி ஆக உயர்ந்தது 
இவர் அரசவை விகடகவியாக உயர்ந்தது குறித்து பல கதைகள் நிலவுகின்றன. 
இவருடைய ஊருக்கு வந்த துறவி ஒருவர் இவருடைய தைரியம் மற்றும் நகைச்சுவை உணர்வில் கவரப்பட்டு காளி தேவியிடம் வரம் பெறத்தக்க மந்திரம் சொல்லி தந்ததாகவும் அதன் அருளால் காளி  தேவியின் தரிசனம் பெற்ற தெனாலி ராமன் அவளையும் தன் நகைச்சுவை அறிவால் சிரிக்க வைத்து அவளாலேயே விகடகவி என்று வாழ்த்தப்பட்டதாகவும் சொல்கிறார்கள்.
அதில் நம்பிக்கை வரப் பெற்ற தெனாலி ராமன் விஜயநகரம் சென்று தன்னுடைய சாமர்த்தியத்தால் அரச தரிசனம் பெற்று தன்  அறிவுக் கூர்மை மற்றும் நகைச்சுவையால் அரசரின் அன்புக்கும் நம்பிக்கைக்கும் பாத்திரமாகி அரசவையில் முக்கிய இடம் பெற்று நல்வாழ்வு வாழ்ந்தார்.
 
## தெனாலி சைவரா? வைணவரா?
இவர் சைவரா வைணவரா என்பதில் சரித்திர ஆசியர்களிடையே குழப்பம் நிலவுகிறது. ஏனென்றால் இவருடைய பெயர் சைவப் பெயர். இவர்  எழுதிய நூலாகிய "பாண்டுரங்க மகாத்மியம்" விஷ்ணுவைப் பற்றியதாக இருப்பதோடு அவருக்கு சமர்ப்பிக்கப்பட்டு உள்ளதாகவும் உள்ளது.
அது தவிர "உத்பாதராதைய சரித்திரம்", "கடிகாச்சல மகாத்மியம்" ஆகிய நூல்களை இயற்றி உள்ளார். முன்னது உத்பாதர் என்கிற துறவியை பற்றியது. பிந்தையது தமிழகம் வேலூருக்கு அருகில் இருக்கும் நரசிம்மர் கோவிலை பற்றியது.
 
## தெனாலியின் பட்டங்கள்
தெனாலி "விகடகவி" என்றும் "குமார பாரதி" என்றும் அனைவராலும் பாராட்டப்பட்டு அன்போடு அழைக்கப்பட்டார்.

## தெனாலியின் சிறப்புகள் 
இந்திய மொழிகளில் இவரைப் பற்றிய பாடக் குறிப்புகள் இல்லாத மொழியே கிடையாது என்னும் அளவுக்குப் பிரபலமானவர்.இவருடைய கதையை "தி அட்வென்சர்ஸ் ஆஃப் தென்னாலி ராமன்" என்கிற பெயரில் கார்ட்டூன் நெட் ஒர்க் தொலைக்காட்சி நிறுவனம் கி.பி.2001இல் படமாக்கப்பது.
 
## தெனாலியின் காலம்
தெனாலி ராமன் வாழ்த்த காலம் கி.பி.1509 முதல் கி.பி.1575 வரை ஆகும். தமிழ் நகைச்சுவை உலகில் மிகவும் புகழ் பெற்ற தெனாலி ராமகிருஷ்ணா என்பவர் விஜயநகரத்தை ஆண்ட கிருஷ்ணா தேவராயரின் அவையை அலங்கரித்த எட்டு அரசவைப்  புலவர்களுள் (அஷ்டதிக்கஜங்கள்) ஒருவர் ஆவார்.
 
இப்படிப்பட்ட புகழ் பெற்ற புலவரான தெனாலி ராமனின் கதைகளில் "பாத்திரங்கள் குட்டி போட்ட கதை" என்னும் கதையை இப் பகுதியில் நாம் பார்க்கலாம்.

## பாத்திரங்கள் குட்டி போட்ட கதை

விஜய நகரக்  கடை வீதியில் வட்டி கடை நடத்தி வந்தான் வட நாட்டைச் சேர்ந்த கரன்சந்த் என்பவன். அவன் மிகப் பேராசைக் காரன்.  வட்டி அதிகமாக வாங்குபவன். வட்டிக்கு வட்டி போடு வசூலிப்பவன். வட்டிக்குப் பணம் கொடுப்பது தவிர பாத்திரங்களை வாடகைக்குத் தருவான். பாத்திரங்களுக்கு வாடகை தவிர பாத்திரங்கள் தேய்ந்து விட்டன, சொட்டையாகி விட்டன எனக் குற்றம் சஷ்டி அதற்கும் பணம் வசூலித்து விடுவான்.

அவனைப் பற்றி அரசருக்கு பல முறை புகார் வந்தும் அவன் மீது நடவடிக்கை எடுக்க சரியான ஆதாரம் கிடைக்கவே இல்லை. வேற்று நாட்டைச் சார்ந்தவனாக இருந்ததால் அவனை எச்சரிக்கை செய்ய முடிந்ததே தவிர தண்டனை தர முடிய வில்லை.

தெனாலி ராமனுக்கு அவனை எப்படியாவது மாட்டச் செய்து தண்டனை வாங்கி தர வேண்டும் என்று எண்ணி அதற்காக ஒரு திட்டம் தீட்டினான். பேராசைக் காரனைப் பேராசைப்  பட வைத்தே தண்டனை பெறச் செய்ய வேண்டும் எனத் திட்டம் தீட்டினான்.

தெனாலி ராமனின் நண்பன் பிச்சையா. அவரைப் பயன்படுத்தி கரன்சந்திடம் பெரிய பெரிய பாத்திரங்கள் சிலவற்றை வாடகைக்கு வாங்கி வர செய்தான். அந்தப் பெரிய பெரிய பாத்திரங்களைப்  போலவே சின்ன சின்னப் பாத்திரங்களை விலைக்கு வாங்கினான். சில தினங்கள் கழித்துப் பெரிய பாத்திரங்களையும் சின்னப் பாத்திரங்களையும் கொண்டு வந்து கரன்சந்திடம் கொடுத்தார் பிச்சையா.

"என்ன இது சின்ன சின்ன பாத்திரங்கள்" என்று கேட்டான் கரன்சந்த் பிச்சையாவிடம். அதற்கு பிச்சையா கரன்சந்திடம் ஐயா, "நீங்கள் கொடுத்த பாத்திரங்கள் கருவுற்று இருந்தது தெரியாமல் நீங்கள் என்னிடம் தந்து விடீர்கள். அவை நேற்று தான் இந்த சின்ன சின்ன பாத்திரங்களைப் பிரசவித்தன" என்றான் பிச்சையா.

கரன்சந்திற்கு ஏகப்பட்ட மகிழ்ச்சி. சில நாட்கள் சென்றன. தெனாலி ராமன் பிச்சையாவை அனுப்பி விலை உயர்ந்த வெள்ளிப் பாத்திரங்களை ஏராளமாக வாடகைக்கு வாங்கி வரச் சொன்னான். தெனாலி ராமனின் நண்பனும் அவ்வாறே செய்கிறேன் என்று கூறினான். 

பேராசைக்  கொண்ட கரன்சந்த் ஏகப்பட்ட வெள்ளிப் பாத்திரங்களையும் தந்து, "இவை கருவுற்ற பாத்திரங்கள். எனவே நீங்கள் இந்த பாத்திரங்கள் குட்டி பாத்ததும் முன்பு செய்தது போல் சின்னச் சின்ன பாத்திரங்களையும் சேர்த்துக் கொண்டு வந்து விடும்" எனச் சொல்லி அனுப்பினான் கரன்சந்த்.

அப்படிப் பிரசவம் நடந்தால் தாயையும் பிள்ளையையும் பிரிச்சு வைப்பேனா? நிச்சயமாக உடனே கொண்டு வந்து தந்து விடுகிறேன் என்று கூறி விட்டுச் சென்றான் பிச்சையா.

நாட்கள் வாரங்கள் ஆயின. வாரங்கள் மாதங்கள் ஆயின. பிச்சையா பத்திரங்களைக் கொண்டு வந்து தரவே இல்லை. பிச்சையா பாத்திரங்களை கொண்டு வராதது கண்டு கரன்சந்த் ஆள் மேல் ஆள் அனுப்பி கேட்டான். பிச்சையா வரவே இல்லை. எனவே முடிவாக தானே சென்று பார்ப்பது என்று அவன் முடிவு செய்தான். எனவே கரன்சந்த் அவரைத் தேடித் சென்றான்.

பிச்சையா கரன்சந்தைக் கண்டதும் ஒப்பாரி வைக்க ஆரம்பித்து விட்டான். கரன்சந்திற்கு  ஒன்றுமே புரிய வில்லை. இவன் ஏன் இவ்வாறு செய்கிறான் என்று யோசிக்க ஆரம்பித்தான். பிறகு பிச்சையா கூற ஆரம்பித்தான். என்ன என்றால், "நான் என்ன என்று சொல்வேன்? அதனை பாத்திரங்களும் பிரசவிக்கட்டும் என்று இந்த அறைக்குள் போட்டு வைத்தேன். காலையில் பார்த்தா என்ன நடந்ததோ ஏது நடந்ததோ பிரசவ வேதனையில் அதனை பாத்திரமும் செத்துக் கிடந்தன. பிணத்தையா உங்களிடம் தூக்கிக்  கொண்டு வருவது என எல்லவரையும் ஈமச் சடங்குகள் செய்து எரித்து விட்டேன். இந்தத் துக்கத்தை உங்களிடம் வந்து சொல்ல வெட்கப் பட்டுக் கொண்டு வரவில்லை என்றார் பிச்சையா. கரன்சந்திற்கு  ஏகப்பட்ட கோபம் வந்தது. பிச்சையாவை கன்னா பின்னா என்று திட்டினான். நேராக அரச சபைக்கு சென்று பிச்சையா பத்தாயிரம் பெறுமானம் உள்ள வெள்ளிப் பாத்திரங்களை வாடகைக்கு வாங்கிச் சென்று திருப்பித் தரவில்லை என வழக்கு தொடுத்தான்.

பிச்சையாவை அழைத்து விசாரித்தார் அரசர் கிருஷ்ண தேவராயர். "பித்தளை பாத்திரங்கள் பிரசவித்த பொது குட்டிகளோடு பாத்திரங்களைக் கொண்டு வந்து கொடுத்தேனே" என்று கூறினார் பிச்சையா அரசர் கிருஷ்ண தேவராயரிடம்.

கரன்சந்த் திணறியபடி ஆமாம் என ஒப்புக் கொண்டான். உடனே பிச்சையா நிறுத்த வில்லை. தொடர்ந்து பேசலானார். "இப்போது பிரசவ நேரத்தில் பாத்திரங்கள் செத்துப் போய் விட்டன. அதற்கு நான் என்ன செய்வேன்" என்று பிச்சையா தனது வாதத்தை அரசர் கிருஷ்ண தேவராயரிடம் முறையிட்டார்.

பிச்சையாவின் பேச்சை கேட்டு கரன்சந்த் பதிலுக்கு கேள்வி கேட்டான். பாத்திரங்கள் எப்படி செத்துப் போகும் என்று பிச்சையாவை பார்த்து கரன்சந்த் கேட்டான்.

உடனே தெனாலி ராமன் குறுக்கே வந்து பாத்திரங்கள் பெண்களை போல குழந்தை பெறுவதை இருந்தால் குழந்தைப் பேற்றின் போது சில பெண்கள் இறந்து விடுவது போல பாத்திரங்கள் ஏன் இறந்து போகாது எனக் கேட்டான் கரன்சந்தைப் பார்த்து.

தெனாலி ராமனின் கேள்வியை பார்த்து அரசர் கிருஷ்ண தேவராயர் புரிந்து கொண்டார். இது தெனாலி ராமனின் வேலை என்று. கரன்சந்தின் பேராசையைச் சுட்டிக் காட்டி உடனடியாக விஜய நகரத்தை விட்டு வெளியேற வேண்டும் என உத்தரவு இட்டார் அரசர் கிருஷ்ண தேவராயர்.

எல்லோரையும் அனுப்பு விட்டு தெனாலி ராமனைப் பாராட்டினார் அரசர் கிருஷ்ண தேவராயர். "பாத்திரங்களை என்ன செய்தாய்" என்று தெனாலி ராமனிடம் அரசர் கிருஷ்ண தேவராயர் கேட்டார். அதற்கு தேனாலி ராமன், "பாத்திரங்களை நீங்கள் நடத்தும் ஆதவற்றவர்கள் விடுதிக்கு அன்பளிப்பாகத் தந்து விட்டேன்" என்றான். அதர்காகவும் பாராட்டினார் அரசர் கிருஷ்ண தேவராயர்.

## கதையின் நீதி:

ஏமாற்று பேர்வழிகளை அவர்கள் போக்கிலேயே போய் தான் வெல்ல வேண்டும். இல்லை எனில் அவர்கள் திருந்துவர்க்கு வாய்ப்பே இல்லை. எனவே இவ்வாறு சந்தர்ப்பம் பார்த்து சமயோசிதமாக செயல்பட வேண்டும் என்று இக்கதை நமக்கு உணர்த்துகிறது.
