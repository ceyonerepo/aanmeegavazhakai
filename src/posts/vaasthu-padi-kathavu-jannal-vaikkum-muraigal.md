---
title : 'வாஸ்து படி கதவு - ஜன்னல் வைக்கும் முறைகள்'
publishedDate: 'Sat Apr 16 2022 12:52:32 GMT+0530'
category: 'vasthu'
slug: 'vaasthu-padi-kathavu-jannal-vaikkum-muraigal'
thumbnail: '../thumbnails/door.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - vasthu
    - aanmeegam
    - nambikai
    - vaasthu saasthiram
keywords:
    - vasthu tamil
    - vasthu in tamil
    - manaiyadi shastra vastu in tamil
---

# வாஸ்து படி கதவு - ஜன்னல் வைக்கும் முறைகள்
## வாஸ்து சாஸ்திரம் 


## வாஸ்து சாஸ்திரம் என்றால் என்ன?

1. நமது வீடு எப்படி இருக்க வேண்டும்?
2. வீடு காட்டும் முறைகள் 
3. எப்படி வீடு இருந்தால் என்ன பலன் கிடைக்கும்
என்பதை விளக்கி சொல்வது வாஸ்து சாஸ்திரம் ஆகும்.

தனக்கு என்று ஒரு வீடு வேண்டும் என்ற ஆசை எல்லோருக்கும் இருக்கிறது. கூழோ கஞ்சியோ நம் வீட்டில் குடித்தால் அதன் திருப்தியே தனி தான். ஆனால் வீடு கட்டும் போது பலர் முக்கியமான வாஸ்து கூட பார்க்காமல் கட்டி விட்டு பின் அவதிப் படுகிறார்கள். ஆபத்து வந்த பின் காப்பதை விட வரும் முன்னே காப்பது நல்லது அல்லவா!

## வாஸ்து:

வாஸ்து என்பது மனையடி சாஸ்திரத்தின் இதயம் போன்றது ஆகும். வாஸ்து எனப்படும் சமஸ்கிருத சொல்லுக்கு வீடு கட்டக் கூடிய மனை என்று பொருள் படும். இது தனி சாஸ்திரமாக சிலரால் கருதப்படுகிறது. இந்த சாஸ்திரத்தின் நாயகனாக தெய்வாம்சம் பொருந்திய ஒரு பெண் கருதப்படுகிறது.

உயிரற்ற ஜடப் பொருட்களான செங்கல், சிமெண்ட், மணல், கம்பிகள், வெட்டப்பட்ட மரம் போன்ற வஸ்துகளை சரியான விகிதத்தில் கலந்து நிலத்தின் மேல் கட்டிடமாக எழுப்பி உயிரோட்டம் கொடுப்பதே வாஸ்து.

வாஸ்து உண்மையில்லை, வாஸ்து ஏமாற்று வித்தை, வாஸ்து பொய் என்று சமூக அக்கறையோடு பதிவு செய்யப்படும் குற்றச்சாட்டுகளுக்கு என் பதில், 

வாஸ்து என்பது உண்மை, வாஸ்து என்பது அறிவியல், வாஸ்து என்பது மத சம்மந்தப்பட்ட விஷயம் அல்ல. வாஸ்துவில் யந்திரம், மந்திரம், பூஜை, தாயத்து மற்றும் கண் கட்டு வித்தைகளுக்கு வேலை இல்லை.

பாம்பு கடிக்கு விஷமுறிவு மருந்து மற்றொரு பாம்பின் விஷம் தான் என்பது உண்மையாக இருப்பின் வீட்டில் உள்ள பிரச்சனைகளுக்கு தீர்வு அந்த வீட்டிலேயே தான் உள்ளது என்பது தான் நான் கண்டா உண்மை.

## வாஸ்து விதிகள்:

வாஸ்து சாஸ்திரம் என்பது ஒவ்வொரு மனித வாழ்வின் வெற்றிக்கும் அடித்தளமாக விளங்கக் கூடிய ஒன்று ஆகும். வாஸ்து விதிகளின் படி ஒரு வீடோ, தொழிற்சாலையோ அமைக்கப்படும் போது அங்கு இருக்கக் கூடிய அனைவருக்கும் எப்பொழுதும் நல்ல ஆற்றலே இருக்கும். என்றும் மன அமைதி, உடல் நலம், செல்வ செழிப்போடு காணப்படுவர். வாஸ்து என்றால் நான்கு திசை மற்றும்  நான்கு மூலைகள் கொண்டே கருதப்படுகின்றன. அவை வடக்கு, கிழக்கு, தெற்கு, மேற்கு. மேலும் நான்கு மூலைகள் வடகிழக்கு, தென்கிழக்கு, வடமேற்கு, தென்மேற்கு ஆகும். இவற்றை கொண்டு வாஸ்துவில் அடிப்படையாக கருதப்படும் ஆறு மிக முக்கியமான பொதுவான விதிகள்.

## வாஸ்துவின் பொதுவான விதிகள்:

1. மனை மற்றும் அதனுள் கட்டப்படும் கட்டிடம் இரண்டும் சதுரம் அல்லது செவ்வகமாக இருக்க வேண்டும்.

2. வடக்கு  மற்றும் கிழக்கில் அதிக காலி இடம் இருத்தல் அவசியம்.

3. தலை வாசல் என்றுமே உச்சத்தில் தான் அமைக்கப்பட வேண்டும்.

4. தெருத் தாக்கம் (தெருக்  குத்து) இருந்தால் கண்டிப்பாக உச்சமாக தான் இருக்க வேண்டும்.

5. உட்புறம் மற்றும் வெளிப்புறம் போடப்படும் படிக்கட்டுகள் அமைக்கப்படும் முறையை அறிவது அவசியம்.

6. வடகிழக்கு பள்ளமாகவும், கனமில்லாமலும், தென்மேற்கு உயரமாகவும், கனமாகவும் இருத்தல் அவசியம்.

வாஸ்து சாஸ்திரத்தைப் பற்றி இன்னும் பல அறிய நூல்கள் உள்ளன. சிற்ப நகரம், சிற்ப ரத்தினம், மயமதம், காஸ்யபம், ஸர்வார்த்த மனுசாரம், வாஸ்து வித்யா போன்ற நூல்கள் சிலை வடிப்பது, ஆலயம் அமைப்பது, வீடு கட்டுவது போன்ற அறிய கலைகளை பற்றி விவரிக்கின்றன.

## வாஸ்து படி கதவு - ஜன்னல் வைக்கும் முறைகள்

## கதவின் தேவை 

காடுகளிலும் மலை குகைகளிலும் வாழ்ந்த மனிதன் காலப் போக்கில் வீடு என்ற ஒன்றை கட்டி வாழ கற்றும் கொண்டான். வீடு கட்டிய மனிதனுக்கு அதை பாதுகாக்க கதவு என்ற ஒன்றை அமைக்கத் தெரியாமல் அதற்கு தட்டி செய்து மறைப்பதும், துணியை திரையாக தொங்க விடுவதுமாக இருந்தான். காலங்கள் முன்னேற முன்னேற மரத்தை அறுத்து அதில் கதவு செய்வதையும் கற்றுக் கொண்டான்.

சிறிய வீடோ, பெரிய வீடோ வீட்டின் உள்ளே உள்ள பொருட்களை பாதுகாக்க அமைக்கப் படுவது தான் கதவு. தற்போது உள்ள சூழ்நிலையில் கால் சவரனுக்காக கூட கொலை கொள்ளை நடக்கிறது என்பதால் நல்ல திடமான கதவுகளை அமைப்பதுடன் அதற்கு பாதுகாப்பாக கிரில் கேட்டையும் அமைத்துக் கொள்கிறார்கள். சிலர் வீட்டின் வெளியே கேமராவை அமைத்து உள்ளிருந்த படியே வெளியே வரும் மனிதர்கள் யார் யார் என கண்காணிக்கிறார்கள்.

## வாஸ்து படி கதவு ஜன்னல் அமைத்தல் 

வாஸ்து ரீதியாக வீட்டிற்கு பாதுகாப்பாக விளங்கும் கதவுகளை எந்த திசையில் அமைத்தால் சிறப்பாக இருக்கும் என பார்க்கின்ற போது ஒவ்வொரு திசையின் உச்ச ஸ்தானங்களில் கதவு வைப்பது நல்லது. ஹால், பெட்ரூம், கிட்சன், பூஜை அறை, பாத்ரூம் போன்ற எல்லாவற்றிற்கும் கதவு வைக்கும் பழக்கம் பெருகி வரும் நிலையில் அந்தந்த அறைகளுக்கும், அந்தந்த உச்ச ஸ்தானங்களில் கதவு அமைப்பது நல்லது.

## வடக்கு திசை 

வடக்கு பார்த்து அமைந்து உள்ள அனைத்து அறைகளுக்கும் அதன் உச்ச ஸ்தானமான வட கிழக்கு பகுதியில் அதிலும் குறிப்பன வடக்கு மத்திய பகுதி முதல் வட கிழக்கு இறுதி வரை உள்ள பகுதியில் கதவை அமைப்பது மிகவும் சிறப்பு. மேற்கூறிய பகுதிகளில் கதவிற்கு அருகில் ஜன்னலையும் அமைப்பது நல்லது. இப்படி அமைக்கும் கதவுகளுக்கு கீலை மேற்கு புறத்தில் அமைத்து கிழக்கில் இருந்து மேற்காக திறக்கும் படி அமைக்க வேண்டும். கதவு ஜன்னல் போன்றவை வட மேற்கு பகுதியில் அமைக்கமால் இருப்பது மிகவும் நல்லது.

## கிழக்கு திசை 

கிழக்கு பார்த்து அமைந்து உள்ள அனைத்து அறைகளுக்கும் அதன் உச்ச ஸ்தானமான வட கிழக்கு பகுதியில் அதாவது கிழக்கு மத்திம பகுதி முதல் வட கிழக்கு மூலை வரை உள்ள பகுதியில் கதவு மற்றும் ஜன்னல்களை அமைப்பது சிறப்பு. இப்படி அமைக்கும் கதவுகளுக்கு கீலை தெற்கு புறத்தில் அமைத்து வடக்கில் இருந்து தெற்காக திறக்கும் படி அமைக்க வேண்டும். தென் கிழக்கு பகுதியில் கதவினை அமைக்காமல் இருப்பது நல்லது.

## தெற்கு திசை 

தெற்கு பார்த்து அமைந்து உள்ள அனைத்து அறைகளுக்கும் அதன் உச்ச ஸ்தானமான தென் கிழக்கு பகுதியில் அதாவது தெற்கு மத்திம பகுதி முதல் தென் கிழக்கு மூலை வரை உள்ள பகுதியில் கதவு மற்றும் ஜன்னல்களை அமைப்பது சிறப்பு. இப்படி அமைக்கும் கதவுகளுக்கு கீலை மேற்கு புறத்தில் அமைத்து கிழக்கில் இருந்து மேற்காக திறக்கும் படி அமைக்க வேண்டும். தென் மேற்கு பகுதியில் கதவினை அமைக்காமல் இருப்பது நல்லது.

## மேற்கு திசை 

மேற்கு பார்த்து அமைந்து உள்ள அனைத்து அறைகளுக்கும் அதன் உச்ச ஸ்தானமான வட மேற்கு பகுதியில் அதாவது மேற்கு மத்திம பகுதி முதல் வட மேற்கு மூலை  வரை உள்ள பகுதியில் கதவு மற்றும் ஜன்னல்களை அமைப்பது சிறப்பு. இப்படி அமைக்கும் கதவுகளுக்கு கீலை கதவிற்கு தெற்கு புறமாக அமைத்து, வடக்கில் இருந்து தெற்காக கதவை திறப்பது போல அமைப்பது நல்லது. தென் மேற்கு பகுதியில் கதவினை அமைக்காமல் இருப்பது நல்லது.

## முடிவுரை:

வாஸ்து சாஸ்திரம் மற்றும் தர்ம சாஸ்திரம், ஜோதிட சாஸ்திரம் இணைந்து தான் ஒரு வீட்டைப் பற்றி முடிவு எடுக்க வேண்டும். வாஸ்து நூல்களில் இடையே சிறிய வித்தியாசங்கள் காணப்படுகின்றது. ஆலய வாஸ்து, கிரக வாஸ்துவை விட சிறிது மாறுபட்டது எந்த ஒரு வீடு சம்பந்தப்பட்ட முடிவு ஆயினும் அவசரம் இல்லாமலும் அடுத்தவர்களுக்கு எந்தப் பாதிப்பும் இல்லாமலும் அமைவது அவசியமாகும். பொதுவாக நல்ல வழிகாட்டி உதவியுடன் மாற்றங்களைச் செய்து கொள்வது நன்மை தரும். மேலும் அடுக்கு மாடி மற்றும் கூட்டுக் குடும்பங்களுக்கு மாற்றங்கள் செய்யும் போது சற்று கவனத்துடன் செய்ய வேண்டும். சில இடங்களில் மாடிப்படி, வாசல் படி எண்ணிக்கை கணக்குப் பார்க்கும் வழக்கம் உண்டு. இவ்வாறு பார்க்க வேண்டாம் என்று சொல்லும் வாஸ்து நிபுணர்களும் உண்டு. ஆக பலன்களை ஆராய்ந்து முடிவு எடுக்க இறைவனைப் பிரார்த்தனை செய்வோம்.
