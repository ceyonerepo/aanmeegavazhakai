---
title : 'வாழ்வில் வளம் பெற சில வாஸ்து குறிப்புகள் - பகுதி 2'
publishedDate: 'Sat Apr 16 2022 12:52:32 GMT+0530'
category: 'vasthu'
slug: 'vaasthu-kuripugal-part-2'
thumbnail: '../thumbnails/hints.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - vasthu
    - aanmeegam
    - nambikai
    - vaasthu saasthiram
keywords:
    - vasthu tamil
    - vasthu in tamil
    - manaiyadi shastra vastu in tamil
---

# வாழ்வில் வளம் பெற சில வாஸ்து குறிப்புகள் - பகுதி 2

## வாஸ்து சாஸ்திரம் என்றால் என்ன?

1. நமது வீடு எப்படி இருக்க வேண்டும்?
2. வீடு காட்டும் முறைகள் 
3. எப்படி வீடு இருந்தால் என்ன பலன் கிடைக்கும்
என்பதை விளக்கி சொல்வது வாஸ்து சாஸ்திரம் ஆகும்.

தனக்கு என்று ஒரு வீடு வேண்டும் என்ற ஆசை எல்லோருக்கும் இருக்கிறது. கூழோ கஞ்சியோ நம் வீட்டில் குடித்தால் அதன் திருப்தியே தனி தான். ஆனால் வீடு கட்டும் போது பலர் முக்கியமான வாஸ்து கூட பார்க்காமல் கட்டி விட்டு பின் அவதிப் படுகிறார்கள். ஆபத்து வந்த பின் காப்பதை விட வரும் முன்னே காப்பது நல்லது அல்லவா!

## வாஸ்து:

வாஸ்து என்பது மனையடி சாஸ்திரத்தின் இதயம் போன்றது ஆகும். வாஸ்து எனப்படும் சமஸ்கிருத சொல்லுக்கு வீடு கட்டக் கூடிய மனை என்று பொருள் படும். இது தனி சாஸ்திரமாக சிலரால் கருதப்படுகிறது. இந்த சாஸ்திரத்தின் நாயகனாக தெய்வாம்சம் பொருந்திய ஒரு பெண் கருதப்படுகிறது.

உயிரற்ற ஜடப் பொருட்களான செங்கல், சிமெண்ட், மணல், கம்பிகள், வெட்டப்பட்ட மரம் போன்ற வஸ்துகளை சரியான விகிதத்தில் கலந்து நிலத்தின் மேல் கட்டிடமாக எழுப்பி உயிரோட்டம் கொடுப்பதே வாஸ்து.

வாஸ்து உண்மையில்லை, வாஸ்து ஏமாற்று வித்தை, வாஸ்து பொய் என்று சமூக அக்கறையோடு பதிவு செய்யப்படும் குற்றச்சாட்டுகளுக்கு என் பதில், 

வாஸ்து என்பது உண்மை, வாஸ்து என்பது அறிவியல், வாஸ்து என்பது மத சம்மந்தப்பட்ட விஷயம் அல்ல. வாஸ்துவில் யந்திரம், மந்திரம், பூஜை, தாயத்து மற்றும் கண் கட்டு வித்தைகளுக்கு வேலை இல்லை.

பாம்பு கடிக்கு விஷமுறிவு மருந்து மற்றொரு பாம்பின் விஷம் தான் என்பது உண்மையாக இருப்பின் வீட்டில் உள்ள பிரச்சனைகளுக்கு தீர்வு அந்த வீட்டிலேயே தான் உள்ளது என்பது தான் நான் கண்டா உண்மை.

## வாஸ்து விதிகள்:

வாஸ்து சாஸ்திரம் என்பது ஒவ்வொரு மனித வாழ்வின் வெற்றிக்கும் அடித்தளமாக விளங்கக் கூடிய ஒன்று ஆகும். வாஸ்து விதிகளின் படி ஒரு வீடோ, தொழிற்சாலையோ அமைக்கப்படும் போது அங்கு இருக்கக் கூடிய அனைவருக்கும் எப்பொழுதும் நல்ல ஆற்றலே இருக்கும். என்றும் மன அமைதி, உடல் நலம், செல்வ செழிப்போடு காணப்படுவர். வாஸ்து என்றால் நான்கு திசை மற்றும்  நான்கு மூலைகள் கொண்டே கருதப்படுகின்றன. அவை வடக்கு, கிழக்கு, தெற்கு, மேற்கு. மேலும் நான்கு மூலைகள் வடகிழக்கு, தென்கிழக்கு, வடமேற்கு, தென்மேற்கு ஆகும். இவற்றை கொண்டு வாஸ்துவில் அடிப்படையாக கருதப்படும் ஆறு மிக முக்கியமான பொதுவான விதிகள்.

## வாஸ்துவின் பொதுவான விதிகள்:

1. மனை மற்றும் அதனுள் கட்டப்படும் கட்டிடம் இரண்டும் சதுரம் அல்லது செவ்வகமாக இருக்க வேண்டும்.

2. வடக்கு  மற்றும் கிழக்கில் அதிக காலி இடம் இருத்தல் அவசியம்.

3. தலை வாசல் என்றுமே உச்சத்தில் தான் அமைக்கப்பட வேண்டும்.

4. தெருத் தாக்கம் (தெருக்  குத்து) இருந்தால் கண்டிப்பாக உச்சமாக தான் இருக்க வேண்டும்.

5. உட்புறம் மற்றும் வெளிப்புறம் போடப்படும் படிக்கட்டுகள் அமைக்கப்படும் முறையை அறிவது அவசியம்.

6. வடகிழக்கு பள்ளமாகவும், கனமில்லாமலும், தென்மேற்கு உயரமாகவும், கனமாகவும் இருத்தல் அவசியம்.

வாஸ்து சாஸ்திரத்தைப் பற்றி இன்னும் பல அறிய நூல்கள் உள்ளன. சிற்ப நகரம், சிற்ப ரத்தினம், மயமதம், காஸ்யபம், ஸர்வார்த்த மனுசாரம், வாஸ்து வித்யா போன்ற நூல்கள் சிலை வடிப்பது, ஆலயம் அமைப்பது, வீடு கட்டுவது போன்ற அறிய கலைகளை பற்றி விவரிக்கின்றன.

## வாழ்வில் வளம் பெற சில வாஸ்து குறிப்புகள்

## தூசியும் ஒட்டடையும் 

கட்டட கலை சாஸ்திரம் என்கிற வாஸ்து கலையில் எண்ணற்ற விஷயங்கள் அடங்கி இருக்கிறது. அதில் ஒவ்வொன்றும் பயன் தரக் கூடியது. அனுசரிப்பதற்கு எளிதானது.

முக்கியமாக, தடையற்ற பண வரவு பெற, கட்டட சாஸ்திரத்தில் நிறைய விதிமுறைகள் உள்ளன. அதில் முக்கியமானது, வீட்டில் தூசியும், ஒட்டடையும் அதிக அளவில் சேர விடக் கூடாது. இன்னும் சொல்லப் போனால், முற்றிலுமாக தூசியும், ஒட்டடையும் வீட்டில் இல்லாமல் இருந்தால் அந்த இல்லத்தில் திருமகள் வாசம் செய்கிறாள்.

தூசியும், ஒட்டடையும் இல்லாத வீடுகளில் வசிப்பவர்கள் பணத்திற்கு பஞ்சம் இல்லாதவர்களாக இருக்கிறார்கள். அதிக அளவில் தூசியும் ஒட்டடையும் சேர்ந்த வீடுகளில் வசிப்பவர்கள், திறமை இருந்தும் அதிர்ஷ்டம் இல்லாதவர்களாக இருக்கிறார்கள். முன்னேற்றம்  குறைந்து காணப்படுகிறது. சிலரின் வீடுகளில் ஒட்டடை தான் மேற்கூரையை தாங்கிப் பிடித்துக் கொண்டு இருக்கிறதோ என எண்ணும் படியாக ஒட்டடை அதிக அளவில் இருக்கிறது. அப்படி இருப்பது நல்லது அல்ல.

அதனால், நல்ல வாஸ்து தன்மை உள்ள வீட்டுக்கு முக்கியமானது, வீட்டை தூய்மையாக வைத்து இருப்பதும், தூசியும், ஒட்டடையும் சேர விடாமல் பார்த்துக் கொள்வதும் ஆகும்.

## துணிமணிகளும் தலைமுடியும் 

வீட்டில் துணிமணிகளை மூலை மூலைக்கு போடக் கூடாது. வீட்டில் கண்ட இடத்தில் துணிமணிகள் அலைந்தால், அந்த வீட்டில் இருப்பவர்களின் கெளரவம் குறையும்.

அது போலவே, தலை முடி அலைந்தால் தேவை இல்லாத பிரச்சனைகள் தேடி வரும். அதனால் தான் அந்த காலத்தில் தலை வாரும் போது வீட்டின் வெளிப் புறத்தில் தலை வாருவார்கள். உதிரும் முடியை உருண்டையாக சுற்றி வெளியே வீசுவார்கள்.

அத்துடன், வீட்டை பெருக்கும் போது வீட்டின் மூலை முடுக்குகளிலும் சுத்தமாக பெருக்க வேண்டும். வீட்டின் மூலையில் குப்பைகளை கொட்டி வைத்தாலோ, அசுத்தமாக இருந்தாலோ அந்த வீட்டில் நடக்கும் விசேஷங்களில் அந்த இல்லத்தில் வசிக்கும் பெண்களால் கலந்து கொள்ள முடியாத அளவிற்கு உடல் நிலையில் பிரச்சனை வரலாம்.

## பூஜை அறையில் தீபம்!

காலையிலும், மாலையிலும் கண்டிப்பாக பூஜை அறையில் தீபம் ஏற்ற வேண்டும். நெருப்பை கீழ் நோக்கி பிடித்தாலும் அது மேல் நோக்கி எறிவது போல, கிரக கோளாறு ஏற்படும் போது, அந்த வீட்டில் இருப்பவர்களின் நற்பெயருக்கு கலங்கம் ஏற்படாமலும், அவர்களின் வாழ்க்கைக்கு நல்ல எதிர்காலமும் அமைத்து தரும் ஆற்றல் பூஜை அறையில் அமைந்த தீபத்திற்கு உண்டு.

## துடைப்பம் வைக்கும் இடம் 

ஈசானிய மூலை  எனப்படும் வீட்டின் வட கிழக்கு மூலையில் துடைப்பம் வைக்கக் கூடாது. தரித்திரம் தரும். ஆண்களுக்கு நல்லது அல்ல.

## சாஸ்திரத்தின் வெற்றி 

ஒரு சாஸ்திரத்தின் வெற்றி என்பது அதை முறையாக பயன் படுத்துவதில் இருக்கிறது. இந்திய பாரம்பரிய சாஸ்திரங்கள் அனைத்துமே மறுக்க முடியாத அறிவியல் உண்மைகளை அடிப்படையாக கொண்டவை. ஆனால் துரதிஷ்ட வசமாக அவற்றின் அறிவியல் அடிப்படைகளை புரிந்துக் கொள்ளாதவர்களாலும், தங்கள் வசத்திக்காக வேண்டுமென்றே திரித்து கூறுபவர்களாலும் இத்தகைய சாஸ்திரங்களின் உண்மை பலமும் பலனும் நீர்த்து போய் விட்டது.

வாஸ்து சாஸ்திரத்தை பயன் படுத்துபவர்களின் எண்ணிக்கை நாளுக்கு நாள் அதிகமாகிக் கொண்டே போகின்றது. இந்து மதத்தை சேர்ந்தவர்கள் மட்டும் அல்லாது மற்ற மதத்தை சேர்ந்தவர்களும் இப்போது எல்லாம் வாஸ்து சாஸ்திரத்தின் அடிப்படையிலேயே கட்டிடங்களை காட்டுகிறார்கள். ஆனால் வாஸ்து என்ற பெயரில் வட மேற்கு, தென் மேற்கு, வட கிழக்கு, தென் மேற்கு மூலைகளில் அமைக்கப்படும் அறைகளின் அமைப்பில் மட்டுமே கவனம் செலுத்துகின்றனர். வாஸ்து சாஸ்திரத்தின் மிக முக்கிய அம்சங்களில் பெரும்பாலானோர் கவனம் செலுத்துவது இல்லை.

## வாஸ்துவும் ஜோதிடமும் 

முதலில் வாஸ்து சாஸ்திரம் என்பது ஜோதிட சாஸ்திரத்தோடு தொடர்பு உடையது. ஜோதிடம் தெரியாத யாராலும் வாஸ்து சாஸ்திரத்தை முழுமையாக சொல்ல முடியாது. வாஸ்து சாஸ்திரம் என்று சொல்வதை விட வாஸ்து அறிவியல் என்று சொல்வதே சரி. ஜியோ மான்சரிசம் என்பதே வாஸ்துவின் அறிவியல் பெயர்.

## வாஸ்துவின் அடிப்படை 

வாஸ்து சாஸ்திரத்தின் அடிப்படை என்பது நிலத்தில் இருந்து துவங்குகிறது. ஒவ்வொரு நிலத்திற்கும் ஒரு வேதியல் மற்றும் பௌதீக தன்மை உண்டு. மண்ணின் மணம், சுவை மற்றும் நிறத்திற்கு தக்கவாறு அதன் குணம் மாறுபடும்.

## மண்ணின் தன்மை 

மண்ணின் கந்தக தன்மை, சுண்ணாம்பு சத்து, அமிலத் தன்மை ஆகியவை மனித உடலை பாதிப்பது போலவே மண்ணில் உள்ள எதிர்மறை சக்திகளும் மனிதனை பாதிக்கும். பாம்பு புற்று, கரையான் புற்று போன்றவை இருந்த இடத்தில் மண்ணில் உயிர் துடைப்பு இல்லாமல் இருக்கும். அதாவது மண்ணின் அணுக்களில் காற்று வெப்பம் மற்றும் நீரின் சரிவிகிதம் முறையாக இல்லாமல் இருக்கும், அந்த உயிர்களின் ஜீவ காந்த அலைகளின் நச்சு தன்மை நிலத்தில் நிறைந்து இருக்கும்.

மனித உடல்கள் அல்லது விலங்குகளின் உடல்கள் புதைக்கப்பட்ட இடமாக இருந்தால் அந்த உடல்களில் இருந்து வெளி வரும் பிரேத க்ருஷ்டை எனப்படும் தீய அணுக்களும் ஜீவ பந்தன சக்தியும் அந்த நிலத்தின் தூய்மையை கெடுத்து விடும்.

## விதண்ட ஸ்தம்பனம் 

குறிப்பிட்ட நிலத்தின் மீது ஏதேனும் பிரச்சனைகள் இருந்து வழக்கு விவகாரங்களில் சிக்கி இருந்து நீண்ட நாட்களாக பயன் படுத்தாமல் இருந்தால் விதண்ட ஸ்தம்பனம் எனப்படும் பூமி தோஷம் ஏற்பட்டு இருக்கும்.

## சுருஸ்த தோஷம் 

நிலத்தில் ஏதேனும் துர் மரணம் ஏற்பட்டு இருந்தாலோ, துர் காரியங்களுக்கு பணம் தேவைப்படுவதால் நிலம் விற்கப்பட்டு இருந்தாலோ, குடி தண்ணீர் அல்லது கோவில் இருந்த இடத்தை அழித்து சமப்படுத்தப்பட்ட நிலமாக இருந்தாலோ சுருஸ்த தோஷம் ஏற்படா இடமாகும் கருதப்படும்.

இத்தகைய நிலங்களில் பூமி சல்லியன் எனப்படும் பூமி தோஷம் இருப்பதால் அதை நிவர்த்தி செய்யாமல் கட்டிட வாஸ்து பார்த்து கட்டிடம் கட்டுவதால் வாஸ்து சாஸ்திரத்தின் முழு பயனையும் பெற முடியாது.
