---
title : 'வாஸ்து புருஷன் புராணக் கதை'
publishedDate: 'Tue Mar 08 2022 19:56:03 GMT+0530'
category: 'life'
slug: 'vaasthu-purusan-puraanak-kathai'
thumbnail: '../thumbnails/vaasthu.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - vasthu
    - aanmeegam
    - nambikai
    - vaasthu saasthiram
keywords:
    - vasthu tamil
    - vasthu in tamil
    - manaiyadi shastra vastu in tamil
---

# வாஸ்து சாஸ்திரம் 
# வாஸ்து புருஷன் புராணக் கதை

## வாஸ்து சாஸ்திரம் என்றால் என்ன?

1. நமது வீடு எப்படி இருக்க வேண்டும்?
2. வீடு காட்டும் முறைகள் 
3. எப்படி வீடு இருந்தால் என்ன பலன் கிடைக்கும்
என்பதை விளக்கி சொல்வது வாஸ்து சாஸ்திரம் ஆகும்.

தனக்கு என்று ஒரு வீடு வேண்டும் என்ற ஆசை எல்லோருக்கும் இருக்கிறது. கூழோ கஞ்சியோ நம் வீட்டில் குடித்தால் அதன் திருப்தியே தனி தான். ஆனால் வீடு கட்டும் போது பலர் முக்கியமான வாஸ்து கூட பார்க்காமல் கட்டி விட்டு பின் அவதிப் படுகிறார்கள். ஆபத்து வந்த பின் காப்பதை விட வரும் முன்னே காப்பது நல்லது அல்லவா!

## வாஸ்து:

வாஸ்து என்பது மனையடி சாஸ்திரத்தின் இதயம் போன்றது ஆகும். வாஸ்து எனப்படும் சமஸ்கிருத சொல்லுக்கு வீடு கட்டக் கூடிய மனை என்று பொருள் படும். இது தனி சாஸ்திரமாக சிலரால் கருதப்படுகிறது. இந்த சாஸ்திரத்தின் நாயகனாக தெய்வாம்சம் பொருந்திய ஒரு பெண் கருதப்படுகிறது.

உயிரற்ற ஜடப் பொருட்களான செங்கல், சிமெண்ட், மணல், கம்பிகள், வெட்டப்பட்ட மரம் போன்ற வஸ்துகளை சரியான விகிதத்தில் கலந்து நிலத்தின் மேல் கட்டிடமாக எழுப்பி உயிரோட்டம் கொடுப்பதே வாஸ்து.

வாஸ்து உண்மையில்லை, வாஸ்து ஏமாற்று வித்தை, வாஸ்து பொய் என்று சமூக அக்கறையோடு பதிவு செய்யப்படும் குற்றச்சாட்டுகளுக்கு என் பதில், 

வாஸ்து என்பது உண்மை, வாஸ்து என்பது அறிவியல், வாஸ்து என்பது மத சம்மந்தப்பட்ட விஷயம் அல்ல. வாஸ்துவில் யந்திரம், மந்திரம், பூஜை, தாயத்து மற்றும் கண் கட்டு வித்தைகளுக்கு வேலை இல்லை.

பாம்பு கடிக்கு விஷமுறிவு மருந்து மற்றொரு பாம்பின் விஷம் தான் என்பது உண்மையாக இருப்பின் வீட்டில் உள்ள பிரச்சனைகளுக்கு தீர்வு அந்த வீட்டிலேயே தான் உள்ளது என்பது தான் நான் கண்டா உண்மை.

## வாஸ்து விதிகள்:

வாஸ்து சாஸ்திரம் என்பது ஒவ்வொரு மனித வாழ்வின் வெற்றிக்கும் அடித்தளமாக விளங்கக் கூடிய ஒன்று ஆகும். வாஸ்து விதிகளின் படி ஒரு வீடோ, தொழிற்சாலையோ அமைக்கப்படும் போது அங்கு இருக்கக் கூடிய அனைவருக்கும் எப்பொழுதும் நல்ல ஆற்றலே இருக்கும். என்றும் மன அமைதி, உடல் நலம், செல்வ செழிப்போடு காணப்படுவர். வாஸ்து என்றால் நான்கு திசை மற்றும்  நான்கு மூலைகள் கொண்டே கருதப்படுகின்றன. அவை வடக்கு, கிழக்கு, தெற்கு, மேற்கு. மேலும் நான்கு மூலைகள் வடகிழக்கு, தென்கிழக்கு, வடமேற்கு, தென்மேற்கு ஆகும். இவற்றை கொண்டு வாஸ்துவில் அடிப்படையாக கருதப்படும் ஆறு மிக முக்கியமான பொதுவான விதிகள்.

## வாஸ்துவின் பொதுவான விதிகள்:

1. மனை மற்றும் அதனுள் கட்டப்படும் கட்டிடம் இரண்டும் சதுரம் அல்லது செவ்வகமாக இருக்க வேண்டும்.

2. வடக்கு  மற்றும் கிழக்கில் அதிக காலி இடம் இருத்தல் அவசியம்.

3. தலை வாசல் என்றுமே உச்சத்தில் தான் அமைக்கப்பட வேண்டும்.

4. தெருத் தாக்கம் (தெருக்  குத்து) இருந்தால் கண்டிப்பாக உச்சமாக தான் இருக்க வேண்டும்.

5. உட்புறம் மற்றும் வெளிப்புறம் போடப்படும் படிக்கட்டுகள் அமைக்கப்படும் முறையை அறிவது அவசியம்.

6. வடகிழக்கு பள்ளமாகவும், கனமில்லாமலும், தென்மேற்கு உயரமாகவும், கனமாகவும் இருத்தல் அவசியம்.

வாஸ்து சாஸ்திரத்தைப் பற்றி இன்னும் பல அறிய நூல்கள் உள்ளன. சிற்ப நகரம், சிற்ப ரத்தினம், மயமதம், காஸ்யபம், ஸர்வார்த்த மனுசாரம், வாஸ்து வித்யா போன்ற நூல்கள் சிலை வடிப்பது, ஆலயம் அமைப்பது, வீடு கட்டுவது போன்ற அறிய கலைகளை பற்றி விவரிக்கின்றன.

# வாஸ்து புருஷன் புராணக் கதை

வாஸ்து புருஷன் தூங்கும் நேரம், விழிக்கும் நேரம் என்று நாம் பஞ்சாங்கத்தில் பார்த்து இருக்கிறோம். இந்த வாஸ்து புருஷன் யார் என்ற கேள்வி பலருக்கு உண்டாகலாம். இதற்கு ஒரு புராணக் கதை உள்ளது.

அந்தகாசுரன் தன் தவத்தினாலும் அதனால் பெற்ற வரத்தினாலும் தன்னை ஒருவரும் ஒன்றும் செய்ய முடியாது என்று நினைத்து தேவர்களுக்குத் தொந்தரவு கொடுக்கத் தொடங்கினான். தேவர்களும் சிவனிடம் சென்று முறையிட்டு, தங்களைக் காக்கும் படி கேட்டுக் கொண்டனர். சிவபெருமானும் அந்தகாசுரனுடன் கடும் போர் புரிந்தார்.

அப்போது அவர் முகத்தில் இருந்து வியர்வைத் துளிகள் கீழே பூமியில் விழ, அந்தத் துளியில் இருந்து ஒரு பூதம் போல் பெரிய மனித உருவம் தோன்றியது. தோன்றிய போதே பசி பசி என்று அலைந்தது. அப்போது சிவபெருமான் அந்தகாசுரனைக் கொன்று அவனைக் கீழே போட்டார். பின் அந்த உடலைத் தின்று பசியாற்றிக் கொள்ளும் படி சொன்னார்.

அதைத் தின்ற பின்னும் அந்த உருவம் பசி என்று அலைந்து, தேவலோகத்தில் புகுந்து தொந்தரவு செய்தது. அதனின் தொந்தரவு தாங்காமல் அவனைக் கீழே தள்ளி, குப்புறப் படுக்க வைத்து, அதன் மேல் 45 தேவர்கள் அமர்ந்தனர். துன்புறுத்தினர். அந்த உருவமும் ப்ரும்மாவை வேண்டித் தன்னைக் காக்கும் படி சொன்னது.

பிரும்மாவும் அதன் முன் தோன்றிக் காத்து அருளினார். பின் அந்த உருவத்திற்கும் ஒரு பதவி அளித்தார். அதாவது புதுக் கட்டடம், கட்டுபவர் உனக்குப் பூஜை செய்து படையல் படைத்த பின் தான் வீடு கட்டத் தொடங்க வேண்டும் என்றார். அப்படி செய்யவில்லை என்றால் நான் அவர்களுக்குத் தொந்தரவு கொடுப்பேன் என்றது அந்த மனித உருவம். பிரும்மாவும் அதற்குச் சம்மதித்து அதன் பெயரை வாஸ்து புருஷன் என்று வைத்தார்.

வாஸ்து புருஷன் வருடத்தில் எட்டு நாட்கள் தான் விழித்து இருப்பர். மற்ற நாட்களில் தூங்குவதாகச் சொல்லப் படுகிறது. அந்த எட்டு நாட்களிலும் ஒன்றரை மணி நேரம் தான் விழித்து இருப்பார். அதில் முதலில் விழித்தவுடன் காலைக் கடன் செய்வார். பின் குளித்து வருவார். பின் பூஜை செய்வார். அதற்குப் பிறகு போஜனம், போஜனத்திற்குப் பின் தாம்பூலம் போடுவார். இப்போது அவர் தாம்பூலம் போடும் நேரத்தில் வாஸ்து பூஜை, பூமி பூஜை செய்ய, வீடு மளமளவென்று ஏறி, ஒரு பிரச்னையும் இல்லாமல் முடிவு அடையும்.

வாஸ்து புருசனைக் கீழே தள்ளி அவன் மேல் 45 தேவர்கள் அவன் முதுகில் அமர்ந்த இடம் தான் வாஸ்து மண்டலம் என்பது ஆகும். இது சதுரமாக இருக்கும். பிரும்மா வாஸ்துவைக் காப்பாற்றியதால் வீட்டின் நடுப் பாகம் பிருமாவுக்காகக் கொடுக்கப்பட்டு, அதற்கு பிரும்மஸ்தானம் என்ற பெயரும் சூட்டப்பட்டது.

## வாஸ்து புருஷன் விழிக்கும் நாட்கள்:

சித்திரை 10ஆம் தேதி 
வைகாசி 21ஆம் தேதி 
ஆடி 11ஆம் தேதி 
ஆவணி 6ஆம் தேதி 
ஐப்பசி 11ஆம் தேதி 
கார்த்திகை 8ஆம் தேதி 
தை 12ஆம் தேதி 
மாசி 22ஆம் தேதி 

வாஸ்து புருஷன் தினமும் தூங்கும் போது அசைந்து அசைந்து ஒரு இடம் இல்லாமல் மாறுகிறார். இதனால் சில மாதங்களில் அவர் படுக்கும் திசை மாறி விடுகிறது.

இரவில் 12ல் இருந்து 3 வரை கிழக்கை நோக்கி இருக்கிறார்.
காலை 3ல் இருந்து 6 வரை தெற்கை நோக்கி இருக்கிறார்.
காலை 6 முதல் 9 வரை மேற்கில் இருக்கிறார்.
காலை 9 முதல் 12 வரை வடக்கில் இருக்கிறார்.

வாஸ்து பூஜை சரியாகச் செய்த பின் கட்டடம் கட்ட  ஆரம்பிக்க, எல்லாம் நலம் தான். வாஸ்து புருஷன் என்பவர் குறிப்பிட்ட மாதங்களில் குறிப்பிட்ட திசையில் சிரசு வைத்து இருப்பர். கால் வைத்து இருப்பார் என்று வாஸ்துவில் கூறப்பட்டு உள்ளது. பூமியின் சுழற்சியை மையமாக வைத்தே வாஸ்து புருஷன் கணிக்கப்படுகிறது.

சூரியன் உதிக்கும் திசை, மறையும் திசை, உத்தராயணம், தட்சிணாயனம் ஆகியவையும் வாஸ்து புருஷன் குறித்த கணிப்பில் பெரும் பங்கு வகிக்கின்றன. ஒவ்வொரு கிரகங்களுக்கும் பாவனை இயக்கம் என்ற கணிப்பு மேற்கொள்ளப்படும். ஜாதகம் எழுதும் போது கூட சிலர் இராசி, நவாம்சம் ஆகியவற்றுடன் பாவனை இயக்கத்தையும் கணித்து கூறுவர்.

வாஸ்துவில் வாஸ்து புருஷன் என்பது கூற பாவனை இயக்கத்தைப் போன்றதே. இயற்கையை ஒன்றி வாஸ்து புருஷன் கணிக்கப்பட்டு உள்ளதால், அதை வைத்து வீடு காட்டும் போது சில செயல்களை செய்தால் சிறப்பாக இருக்கும் எனக் கூறப்படுகிறது.

வாஸ்து புருசனைப் பின்பற்றி குறிப்பிட்ட செயல்களை மேற்கொள்ளும் போது இயற்கையின் ஒத்துழைப்புடன் வீட்டை கட்டி முடிக்க முடியும். எந்த இடையூறும் வராது.

## வாஸ்து புருஷன் பணிகள்:

வாஸ்து புருஷன் விழித்து இருக்கும் நேரங்களில் அவர் செய்யக் கூடிய பணிகள் 
1. பல் துலக்குகிறார்.
2. நீராடுகிறார்.
3. பூஜை செய்கிறார்.
4. உணவு உண்கிறார்.
5. தாம்பூலம் தரிக்கிறார்.
