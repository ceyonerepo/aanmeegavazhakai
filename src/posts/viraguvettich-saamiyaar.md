---
title : 'விறகுவெட்டிச் சாமியார்'
publishedDate: 'Wed Apr 06 2022 13:32:56 GMT+0530'
category: 'akbar-beerbaal-stories'
slug: 'viraguvettich-saamiyaar'
thumbnail: '../thumbnails/akbar-beerbaal.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - akbar beerbal stores
    - siru kadhaigal
    - moral stories
    - naneri kathaigal
    - tamil short stores
    - stories in tamil
keywords: 
    - akbar beerbal stories
    - akbar beerbal
    - beerbal
    - tamil tories
    - moral stories in tamil
    - tamil kathaigal
---
 
# அக்பர் பீர்பால் கதைகள் - தமிழ் நன்னெறிக் கதைகள்

# விறகுவெட்டிச் சாமியார்

## ஜலாலுத்தீன்  முகம்மது அக்பர்:

மொகலாயப் பேரரசர்களில் பெருஞ் சிறப்பு வாய்ந்தவர் ஜலாலுத்தீன் முகம்மது அக்பர் ஆவார். அவருடைய பாட்டனார் பாபர் என்பவர் இப்ராஹிம் லோடியை வென்று மொகலாயப் பேரரசை 1526 - ல் தோற்றுவித்தார். அவர் நான்கு ஆண்டு காலம் தான் அரசாட்சி செய்தார். அதற்குப் பிறகு அக்பருக்குத் தந்தையான உமாயூன் என்பவர் 1530 ல் அரியணை ஏறினார். பத்தாண்டு காலம் தொடர்ந்தார் போல் அரசாட்சி நடத்துவதற்கு அவர் பட்டபாடு கொஞ்ச நஞ்சமல்ல. அந்தச் சமயத்தில் சூர் வம்சத்தைச் சேர்ந்த ஷெர்கான் என்பவன் உமாயூனுக்கு விரோதமாகக் கிளம்பி அவரை முறியடித்து விட்டு அரசைக் கைப்பற்றிக் கொண்டான்.

ஷெர்கானால் தோற்கடிக்கப்பட்ட உமாயூன் நாடு நகரம், சொத்து சுகம் அனைத்தையும் இழந்து, பகைவர்க்கு அஞ்சி அமர்கோட் ராணாவிடம் தஞ்சம் புகுந்தார். அங்கு தான் ஜலாலுத்தீன் முகம்மது அக்பர் 1542 ல் பிறந்தார்.

உமாயூன் பதினைந்து ஆண்டுகளுக்குப் பிறகு பெரும் படையுடன் 1555 ல் ஷெர்ஷாவின் மேல் படை எடுத்தார். அப்பொழுது அக்பருக்கு வயது பதின்மூன்று. உமாயூன் தன்னுடைய படையை இரு பிரிவுகளாகப் பிரித்து ஒரு பிரிவுக்கு சிறுவன் அக்பரையும் அவனுக்குத் துணையாக தளபதி பைராம்கானையும் அனுப்பினார். இம்முறை உமாயூன் வெற்றிவாகை சூடினார். இழந்த நாட்டை திரும்பப் பெற்று மீண்டும் 1555 ல் பட்டம் சூட்டிக் கொண்டார். அதற்கு பிறகு ஓராண்டு காலம் தான் உமாயூன் உயிரோடு வாழ்ந்தார். அவர் இறந்ததும்  பதினான்கு வயதுச் சிறுவனான அக்பர் 1556 ல் அரியணை ஏறினார். அவருக்குத் துணையாக தளபதி பைராம்கான்  ஆரம்பத்தில் துணை புரிந்தார்.

அக்பர் தன்னுடைய ஆட்சிக் காலத்தின் போது இந்து முஸ்லீம் ஒற்றுமைக்காக மிகவும் பாடுபட்டார். இந்துக்கள் பால் சக்கரவர்த்தி அன்பு காட்டியதற்கு காரணம் பீர்பால் தான் என்று நினைக்கத் தோன்றுகிறது. 

பீர்பாலுடன் சேர்த்து மொத்தம் ஒன்பது அறிஞர்கள் அக்பரின் அரசவையை அலங்கரித்தார்கள். இவர்களை "நவரத்தினங்கள்" என்று குறிப்பிடுவார்கள். ராஜா பீர்பால், ராஜா மான்சிங், ராஜா தேடர்மால், ஹகீம் ஹிமாம், முல்லா தோபியாஸா, பைஜி, அபுல்பசல், ரஹீம், மியான் தான்சேன் முதலியவர்கள் தான் இந்த நவரத்தினங்கள்.

மதங்களினால் தான் மக்கள் வேற்றுமை அடைந்து ஒருவருக்கு ஒருவர் வீணே சச்சரவு இட்டு கொள்கிறார்கள் என்று கருதிய அக்பர் அனைவரையும் ஒன்று சேர்க்கக் கருதி ஒரு புதிய மதத்தை தோற்றுவித்தார்.  இப்புதிய மதத்திற்கு "தீன்இலாஹி" என்பதாகும். 

அக்பர் தம்முடைய 63 வது வயதில் 1605 ல் "தான் பெற்ற மகனே தனக்கு விரோதியாக மாறி விட்டானே" என்ற மன வருத்தத்துடன் உயிர் துறந்தார். அவருடைய விருப்பத்திற்கிணங்க அவர் இறந்த போது எந்த மதத்தினருக்கு உரிய துதிப் பாடல்களும் அவருக்கு ஓதப்படவில்லை. அவருடைய சடலம் ஆடம்பரம் ஏதுமின்றி கல்லறையில் புதைக்கப் பெற்றது.

## ராஜா பீர்பால் 

ராஜா பீர்பாலுடைய இயற்பெயர் மகேஷ்தாஸ் என்பது ஆகும். சிலர் பிரம்மதாஸ் என்றும் குறிப்பிடுகிறார்கள். பீர்பால் என்றால் வீரர்களில் பலம் பொருந்தியவன் என்பதாகும். இந்தப் பெயரை இவருக்குச் சூட்டியவரும் அக்பர் சக்கரவர்த்தி தான்.

பீர்பால் கோதாவரி நதிக் கரையிலுள்ள மார்ஜால் என்ற ஊரில் பிறந்ததாகச் சிலர் கூறுகிறார்கள். அப்துல் காதர் பதௌனி என்ற சரித்திராசிரியர் இவர் கால்பி என்ற ஊரில், ஓர் ஏழைப் பிராமணக் குடும்பத்தில் 1541 ல் பிறந்ததாகக் கூறுகிறார்.

பீர்பால் இளமைப் பருவத்திலேயே கவிதைகள் புனைந்து பாடுவதில் வல்லவராக விளங்கினார். காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதரின் திருமகளை மணம் செய்து கொண்ட பிறகு தான் ஓரளவு சுகமாக வாழ்ந்து வந்தார். மாமனார் இறந்ததும் அவரே சமஸ்தானத்து தலைமைப் பண்டிதரானார். அவர் அறிவுத்திறனை அறிந்த மக்கள் அவரைப் புகழ்ந்தனர். புதிய பண்டிதரின் புகழ் சமஸ்தானமெங்கும் பரவியது.

காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதராக இருந்த பீர்பால் என்ன காரணத்தினாலோ அந்தப் பதவியைத் துறந்து விட்டு ஆக்ராவுக்கு வந்து சேர்ந்தார். அங்கு ராம்சந்த் என்ற ஒரு செல்வரின் உதவியால் புரோகிதத் தொழிலைச் செய்து வந்தார்.

பீர்பால் ஆக்ராவில் இருக்கும் போது அவரது நகைச்சுவைப் பேச்சையும், அறிவுத் திறனையும் கண்ட ஆக்ரா மக்கள் அவரைப் புகழ்ந்தனர். இது அக்பரது செவிகளிலும் விழுந்தது. அக்பரது இந்துக்களிடம் அளவு கடந்த அன்பு. அக்பருடைய அரசவையில் கல்வியில் சிறந்த அறிவாளிகள் பலர் இருந்தனர். ஆனால் நகைச்சுவையாகப் பேசிச் சிரிக்க வைக்க ஒரு விகடகவி இல்லை.

ஒரு நாள் ஆக்ரா வீதியில் பாட்டுப் பாடி பிச்சை எடுத்துக்  கொண்டு இருந்த ஏழை பிராமணனான பீர்பாலை அக்பர் பார்க்க நேர்ந்தது. பீர்பாலின் பாடலும், வேடிக்கைப் பேச்சும் அக்பரின் மனதைக் கவர்ந்து விட்டன. அப்போதே பீர்பாலுடன் அவர் நட்புக் கொண்டு விட்டார். அதன் பின்னர் அவர் அக்பரிடம் வேலைக்கு அமர்ந்ததே ஒரு வேடிக்கையான கதை.

அரசவையில் பீர்பாலுடைய விகடப் பேச்சுகளும், நகைச்சுவையான உரையாடல்களும் அக்பரின் உள்ளத்தைப் பெரிதும் கவர்ந்தன. இதன் காரணமாக அக்பரின் அரசவையில் பீர்பால் சிறப்பு மிக்க ஓர் இடத்தைப் பெற்று விட்டார்.

# விறகுவெட்டிச் சாமியார்

ஒரு முறை மெக்காவில் இருந்து முகம்மதிய மதகுரு ஒருவர் ஆக்ரா  வந்து இருந்தார். அக்பர் அவரை வரவேற்று உபசரித்தார். அந்த மதகுரு பல நாட்கள் அரண்மனையில் தங்கி இருந்தார். அவர் ஊருக்குப் புறப்படும் போது மாமன்னர் அவருக்கு நிறையப் பரிசுகளும், பொன்னும், பொருளும் காணிக்கையாக கொடுத்து அனுப்பினார்.

அவர் சென்ற பிறகு அக்பர் பீர்பாலைப் பார்த்து, "பீர்பால், உங்கள் மதத்திலும் இம்மாதிரி குருமார்கள் உண்டா? அவர்களுக்கு நீங்கள் காணிக்கை தருவது உண்டா?" என்று கேட்டார். "எங்களுக்கும் குருமார்கள் உண்டு. ஆனால், அவர்கள் பொன்னும், பொருளும் விரும்ப மாட்டார்கள். எல்லாவற்றையும் துறந்த பற்று அற்றவர்களாக இருப்பார்கள்" என்றார் பீர்பால். "அப்படியா! அப்படியானால் அப்படிப்பட்டவரை நான் பார்க்க வேண்டுமே!" என்றார் அக்பர்.

பொன்னும், பொருளும் விரும்பாத குருவை பீர்பால் எங்கு தேடிப் பிடிப்பார்? அவர் எங்காவது ஒரு காட்டில் கண்மூடி தவம் செய்து கொண்டு இருப்பார். "அவரை எவ்விதம் கூட்டி வந்து அக்பருக்கு காட்டுவது?" என்று சிந்தனை செய்த பீர்பால் ஒரு முடிவுக்கு வந்தவராய் அக்பருடைய விருப்பத்தை நிறைவேற்ற ஒப்புக் கொண்டார்.

வெளியே வரும் போது விறகுவெட்டி ஒருவன் எதிரே வந்து கொண்டு இருந்தான். அவனை தனியே கூட்டிச் சென்றார் பீர்பால். "இதோ பார், நாளைக்கு ஒரு நாள் மட்டும் நீ சாமியார் வேடம் போட்டுக் கொண்டு நான் சொல்லுகிறபடி நடந்தால் உனக்கு நிறைய பொருள் தருவேன்" என்று விறகு வெட்டியிடம் கூறினார் பீர்பால். "ஆபத்தில்லாத சங்கதியாக இருந்தால் நீங்கள் என்ன சொன்னாலும் கேட்கிறேன்" என்றான் விறகு வெட்டி.

"ஆபத்து ஒன்றுமில்லை. நாளைய தினம் சாமியார் வேடம் போட்டுக் கொண்டு இந்த ஊற்சசாவடியில் உட்கார்ந்து கொண்டிரு. உன்னைப் பார்க்க அரண்மனையில் இருந்து பெரிய பெரிய அதிகாரிகளும், மந்திரிகளும் வருவார்கள். அவ்வளவு ஏன், என் அரசரே வருவார். யார் வந்து உன்னை என்ன கேட்டாலும் நீ பேசக் கூடாது, இவ்வளவு தான் நீ செய்ய வேண்டியது" என்றார் பீர்பால்.

"இதில் என்ன துன்பம் இருக்கிறது? நீங்கள் சொன்ன மாதிரியாகவே நடந்து கொள்வேன்" என்றான் விறகுவெட்டி. "மிக எச்சரிக்கையாய் இருக்க வேண்டும். யார் என்ன கேட்டாலும் வாயைத் திறக்கக் கூடாது. இது தான் முக்கியம்" என்றார் பீர்பால். அடுத்த நாள் காலை ஊர்ச்சாவடியில் விறகுவெட்டி, சாமியார் வேடத்தில் உட்கார்ந்து கொண்டு கண்களை மூடிய படியே தியானத்தில் இருப்பதைப் போல பாவனை செய்து கொண்டு இருந்தான்.

இந்த நேரத்தில் பீர்பால் அரசரிடம், "அரசே, எங்களுடைய குருநாதரைப் பார்க்க வேண்டும் என்று சொன்னீர்களே! அவர் இந்த ஊர்ச்சாவடியில் வந்து தங்கி இருக்கிறார்!" என்றார். "அப்படியா! இன்று மாலை அவரைப் போய்ப் பார்ப்போம்" என்றார் அக்பர்.

பீர்பால் அரசரிடம் கூறிய செய்தி அரண்மனை பூராவும் பரவி விட்டது. குருதேவரைப் பார்க்க அரண்மனையில் இருந்து பெரிய பெரிய அதிகாரிகளும், அமைச்சர்களும் ஊர்ச்சாவடியை நோக்கிப் படை எடுத்தனர். விறகுவெட்டிச் சாமியார் தன்னை நோக்கி வரும் அரண்மனைப் பட்டாளத்தைக் கண்டார். அவர் உடல் நடுங்கியது. கண்களை இறுக மூடிக் கொண்டார்.

அதிகாரிகள் வந்து அவர் காலடியில் வீழ்ந்தனர். அப்போதும் கண்களைத் திறக்கவில்லை விறகுவெட்டிச் சாமியார். மாலையில் அக்பரும், பீர்பாலும் குருதேவரைப் பார்க்கச் சாவடிக்கு வந்தனர். அவருடைய எளிமைக் கோலம் சக்கரவர்த்திக்கு மிகவும் பிடித்து விட்டது.

"ஐயனே, இவ்வளவு சிறந்த பக்தியுள்ள தாங்கள் இந்தச் சாவடியில் இருக்கக் கூடாது. அரண்மனைக்கு வர வேண்டும்" என்றார் அக்பர். விறகுவெட்டிச் சாமியார் வாயே திறக்கவில்லை.

நாடாளும் மாமன்னரான தம்மை மதித்து குருதேவர் பேசாதது அக்பருக்கு மனவருத்தத்தை அளித்தது. ஆயினும் கோபத்தை அடக்கிக் கொண்டு, "அரண்மனையில் இருந்து தங்கப் பல்லக்கு அனுப்புகிறேன், அதிலேயே வந்து விடுங்கள்" என்றார். அப்போதும் விறகுவெட்டிச் சாமியார் வாயையே திறக்கவில்லை!

"தாங்கள் ஒரு முறை அரண்மனைக்கு வருகை தந்தால் தங்களுக்கு பொன்னும், பொருளும் நிறையாக காணிக்கை தருவேன்" என்றார். இதற்கும் விறகுவெட்டிச் சாமியார் மசியவில்லை!

தம்மை மதித்து தாம் கேட்ட கேள்விக்கு குருதேவர் பதில் அளிக்காதது அக்பருக்கு மிக மிகக் கோபத்தை உண்டாக்கியது. கடைசியில் கோபத்துடன், "கடைசியாகச் சொல்கிறேன், தாங்கள் அரண்மனைக்கு வருகை தந்தால், தங்களுக்கு நிறைய பரிசுகள் தருவேன். ஒரு கிராமத்தையே தங்களுக்கு இனாமாகத் தருவேன். என்ன சொல்கிறீர்கள்?" என்றார். இதற்கும் விறகுவெட்டிச் சாமியார் வாயையே திறக்கவில்லை!

அவர் கோபத்துடன் பீர்பாலைப் பார்த்து, "பீர்பால், உன்னுடைய குருதேவர் மிகமிக கர்வம் பிடித்தவர். நாடாளும் மாமன்னரான என்னை மதித்து ஒரு வார்த்தை கூடப் பேசவில்லை. எவ்வளவு பரிசுகள் கொடுப்பதாகச் சொன்னாலும் அதை ஏற்றுக் கொள்ள ஒப்பவில்லை. அவர் கொடுத்து வைத்தது அவ்வளவு தான். வா, போவோம்" என்று பீர்பாலை அழைத்துக் கொண்டு வெளியே சென்றார் அக்பர்.

வெளியே வந்ததும், பீர்பால் அரசரிடம், "அரசே, நான் தான் முதலிலேயே சொன்னேனே, அவர் எந்தப் பொருள் மீதும் ஆசைப்படாதவர் என்று. தாங்கள் அதை மறந்து விட்டு நான் அதைத் தருகிறேன், இதைத் தருகிறேன் என்று அவருக்கு ஆசை வார்த்தைகள் கூறினீர்கள். அதனால் தான் அவர் தங்கள் மீது கோபம் கொண்டு தங்களிடம் பேசவில்லை" என்றார்.

அப்படியா சங்கதி! ஆமாம், நான் அவரிடம் அப்படிச் சொல்லி இருக்கக் கூடாது தான். அது சரி, பீர்பால்! உங்கள் குருதேவர் எதற்குமே பேச மாட்டாரோ? என்று கேட்டார். எப்பொழுதாவது ஒரு முறை பேசுவார். அது அபூர்வம். நாங்கள் அவரைப் பார்த்து கும்பிட்டு விட்டு வந்து விடுவோம். அவ்வளவு தான். அவரும் எங்களை இதைக் கொண்டு வா, அதைக் கொண்டு வா என்று கூற மாட்டார், என்றார் பீர்பால்.

"உங்கள் குருதேவர் மிகவும் நல்லவர் தான். ஒரு கிராமத்தையே இனாமாகக் கொடுப்பதாகச் சொன்னேன். அப்போது கூட அரண்மனைக்கு வருவதாக இசைவு தரவில்லையே! இம்மாதிரி ஆசைகளைத் துறந்த துறவிகளை பார்ப்பது மிகமிக அபூர்வம்" என்றார் அக்பர். "ஆம்! ஆம்!" என்று சொல்லி விட்டு அரசரை அரண்மனையில் சேர்ப்பித்து விட்டு ஊர்ச்சாவடிக்குத் திரும்பினார் பீர்பால். விறகுவெட்டிச் சாமியாரை தன்னுடன் வருமாறு அழைத்துக் கொண்டு தனி இடம் ஒன்றிற்குச் சென்றார்.

"உன் வேடம் மிக நன்றாக இருந்தது. சொன்ன படியே நடந்து கொண்டாய். இந்தா பணமுடிப்பு. இதில் நூறு மொகராக்கள் இருக்கின்றன. இதை எடுத்துச் சென்று சுகமாக இரு" என்று பணமுடிப்பு ஒன்றை விறகுவெட்டியிடம் கொடுத்தார் பீர்பால். "எனக்கு! நூறு மொகராக்களா?" என்று வாயைப் பிளந்தான் விறகுவெட்டி.

"ஆமாம். ஒரு முறை நீ அரண்மனைக்கு வந்து சென்றால் பொன்னும், பொருளும் பரிசாகத் தருவதுடன், ஒரு கிராமத்தையே உனக்குப் பரிசாகத் தருவதாக உன்னிடம் அரசர் கூறினார். நீ அதற்குக் கொஞ்சம் கூட அசைந்து கொடுக்காமல் அவற்றை உதறித் தள்ளி விட்டாய். இன்னொருவனாக இருந்தால் என் பேச்சையும் உதறித் தள்ளி விட்டு இந்நேரம் அரசர் பின்னால் சென்று அரண்மனையில் உல்லாசமாக இருந்து இருப்பான். நீயோ எல்லாவற்றையும் உதறித் தள்ளிவிட்டாய்" என்றார் பீர்பால்.

"எனக்கு கூட முதலில் ஆசையாகத் தான் இருந்தது. சரி என்று சொல்லிவிடலாமா என்று தான் பார்த்தேன். ஆனாலும். பல்கலைக் கடித்துக் கொண்டு நீங்கள் சொன்ன மாதிரியாகவே ஊமையாக நடித்து விட்டேன். ஆனால் இப்பொழுது வருத்தமாக இருக்கிறது. அவர் கூப்பிட்ட உடனே போய் இருந்தால் ஒரு கிராமம் இனாமாகக் கிடைத்து இருக்கும் அல்லவா?" என்று பெருமூச்சு விட்டான் விறகுவெட்டி.

அவன் பேராசையைக் கண்ட பீர்பால், "அது தான் தவறு. நீ போயிருந்தால் உன் குட்டு வெளிப்பட்டு விடும். உன் வேடம் கலைந்து விடும். முதலில் உனக்கு மரண தண்டனை தான் பரிசாகக் கிடைத்து இருக்கும். அதிகம் ஆசைப்படாதே! கிடைத்த வரையில் திருப்தியாகப் போய்ச் சேர்" என்று சொல்லி அவனை அனுப்பி வைத்தார் பீர்பால்.

