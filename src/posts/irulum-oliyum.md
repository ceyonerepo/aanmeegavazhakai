---
title : 'இருளும் ஒளியும்'
publishedDate: 'Wed Apr 06 2022 13:32:56 GMT+0530'
category: 'akbar-beerbaal-stories'
slug: 'irulum-oliyum'
thumbnail: '../thumbnails/akbar-beerbaal.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - akbar beerbal stores
    - siru kadhaigal
    - moral stories
    - naneri kathaigal
    - tamil short stores
    - stories in tamil
keywords: 
    - akbar beerbal stories
    - akbar beerbal
    - beerbal
    - tamil tories
    - moral stories in tamil
    - tamil kathaigal
---
 
# அக்பர் பீர்பால் கதைகள் - தமிழ் நன்னெறிக் கதைகள்

# இருளும் ஒளியும்

## ஜலாலுத்தீன்  முகம்மது அக்பர்:

மொகலாயப் பேரரசர்களில் பெருஞ் சிறப்பு வாய்ந்தவர் ஜலாலுத்தீன் முகம்மது அக்பர் ஆவார். அவருடைய பாட்டனார் பாபர் என்பவர் இப்ராஹிம் லோடியை வென்று மொகலாயப் பேரரசை 1526 - ல் தோற்றுவித்தார். அவர் நான்கு ஆண்டு காலம் தான் அரசாட்சி செய்தார். அதற்குப் பிறகு அக்பருக்குத் தந்தையான உமாயூன் என்பவர் 1530 ல் அரியணை ஏறினார். பத்தாண்டு காலம் தொடர்ந்தார் போல் அரசாட்சி நடத்துவதற்கு அவர் பட்டபாடு கொஞ்ச நஞ்சமல்ல. அந்தச் சமயத்தில் சூர் வம்சத்தைச் சேர்ந்த ஷெர்கான் என்பவன் உமாயூனுக்கு விரோதமாகக் கிளம்பி அவரை முறியடித்து விட்டு அரசைக் கைப்பற்றிக் கொண்டான்.

ஷெர்கானால் தோற்கடிக்கப்பட்ட உமாயூன் நாடு நகரம், சொத்து சுகம் அனைத்தையும் இழந்து, பகைவர்க்கு அஞ்சி அமர்கோட் ராணாவிடம் தஞ்சம் புகுந்தார். அங்கு தான் ஜலாலுத்தீன் முகம்மது அக்பர் 1542 ல் பிறந்தார்.

உமாயூன் பதினைந்து ஆண்டுகளுக்குப் பிறகு பெரும் படையுடன் 1555 ல் ஷெர்ஷாவின் மேல் படை எடுத்தார். அப்பொழுது அக்பருக்கு வயது பதின்மூன்று. உமாயூன் தன்னுடைய படையை இரு பிரிவுகளாகப் பிரித்து ஒரு பிரிவுக்கு சிறுவன் அக்பரையும் அவனுக்குத் துணையாக தளபதி பைராம்கானையும் அனுப்பினார். இம்முறை உமாயூன் வெற்றிவாகை சூடினார். இழந்த நாட்டை திரும்பப் பெற்று மீண்டும் 1555 ல் பட்டம் சூட்டிக் கொண்டார். அதற்கு பிறகு ஓராண்டு காலம் தான் உமாயூன் உயிரோடு வாழ்ந்தார். அவர் இறந்ததும்  பதினான்கு வயதுச் சிறுவனான அக்பர் 1556 ல் அரியணை ஏறினார். அவருக்குத் துணையாக தளபதி பைராம்கான்  ஆரம்பத்தில் துணை புரிந்தார்.

அக்பர் தன்னுடைய ஆட்சிக் காலத்தின் போது இந்து முஸ்லீம் ஒற்றுமைக்காக மிகவும் பாடுபட்டார். இந்துக்கள் பால் சக்கரவர்த்தி அன்பு காட்டியதற்கு காரணம் பீர்பால் தான் என்று நினைக்கத் தோன்றுகிறது. 

பீர்பாலுடன் சேர்த்து மொத்தம் ஒன்பது அறிஞர்கள் அக்பரின் அரசவையை அலங்கரித்தார்கள். இவர்களை "நவரத்தினங்கள்" என்று குறிப்பிடுவார்கள். ராஜா பீர்பால், ராஜா மான்சிங், ராஜா தேடர்மால், ஹகீம் ஹிமாம், முல்லா தோபியாஸா, பைஜி, அபுல்பசல், ரஹீம், மியான் தான்சேன் முதலியவர்கள் தான் இந்த நவரத்தினங்கள்.

மதங்களினால் தான் மக்கள் வேற்றுமை அடைந்து ஒருவருக்கு ஒருவர் வீணே சச்சரவு இட்டு கொள்கிறார்கள் என்று கருதிய அக்பர் அனைவரையும் ஒன்று சேர்க்கக் கருதி ஒரு புதிய மதத்தை தோற்றுவித்தார்.  இப்புதிய மதத்திற்கு "தீன்இலாஹி" என்பதாகும். 

அக்பர் தம்முடைய 63 வது வயதில் 1605 ல் "தான் பெற்ற மகனே தனக்கு விரோதியாக மாறி விட்டானே" என்ற மன வருத்தத்துடன் உயிர் துறந்தார். அவருடைய விருப்பத்திற்கிணங்க அவர் இறந்த போது எந்த மதத்தினருக்கு உரிய துதிப் பாடல்களும் அவருக்கு ஓதப்படவில்லை. அவருடைய சடலம் ஆடம்பரம் ஏதுமின்றி கல்லறையில் புதைக்கப் பெற்றது.

## ராஜா பீர்பால் 

ராஜா பீர்பாலுடைய இயற்பெயர் மகேஷ்தாஸ் என்பது ஆகும். சிலர் பிரம்மதாஸ் என்றும் குறிப்பிடுகிறார்கள். பீர்பால் என்றால் வீரர்களில் பலம் பொருந்தியவன் என்பதாகும். இந்தப் பெயரை இவருக்குச் சூட்டியவரும் அக்பர் சக்கரவர்த்தி தான்.

பீர்பால் கோதாவரி நதிக் கரையிலுள்ள மார்ஜால் என்ற ஊரில் பிறந்ததாகச் சிலர் கூறுகிறார்கள். அப்துல் காதர் பதௌனி என்ற சரித்திராசிரியர் இவர் கால்பி என்ற ஊரில், ஓர் ஏழைப் பிராமணக் குடும்பத்தில் 1541 ல் பிறந்ததாகக் கூறுகிறார்.

பீர்பால் இளமைப் பருவத்திலேயே கவிதைகள் புனைந்து பாடுவதில் வல்லவராக விளங்கினார். காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதரின் திருமகளை மணம் செய்து கொண்ட பிறகு தான் ஓரளவு சுகமாக வாழ்ந்து வந்தார். மாமனார் இறந்ததும் அவரே சமஸ்தானத்து தலைமைப் பண்டிதரானார். அவர் அறிவுத்திறனை அறிந்த மக்கள் அவரைப் புகழ்ந்தனர். புதிய பண்டிதரின் புகழ் சமஸ்தானமெங்கும் பரவியது.

காலிஞ்சார் சமஸ்தானத்து தலைமைப் பண்டிதராக இருந்த பீர்பால் என்ன காரணத்தினாலோ அந்தப் பதவியைத் துறந்து விட்டு ஆக்ராவுக்கு வந்து சேர்ந்தார். அங்கு ராம்சந்த் என்ற ஒரு செல்வரின் உதவியால் புரோகிதத் தொழிலைச் செய்து வந்தார்.

பீர்பால் ஆக்ராவில் இருக்கும் போது அவரது நகைச்சுவைப் பேச்சையும், அறிவுத் திறனையும் கண்ட ஆக்ரா மக்கள் அவரைப் புகழ்ந்தனர். இது அக்பரது செவிகளிலும் விழுந்தது. அக்பரது இந்துக்களிடம் அளவு கடந்த அன்பு. அக்பருடைய அரசவையில் கல்வியில் சிறந்த அறிவாளிகள் பலர் இருந்தனர். ஆனால் நகைச்சுவையாகப் பேசிச் சிரிக்க வைக்க ஒரு விகடகவி இல்லை.

ஒரு நாள் ஆக்ரா வீதியில் பாட்டுப் பாடி பிச்சை எடுத்துக்  கொண்டு இருந்த ஏழை பிராமணனான பீர்பாலை அக்பர் பார்க்க நேர்ந்தது. பீர்பாலின் பாடலும், வேடிக்கைப் பேச்சும் அக்பரின் மனதைக் கவர்ந்து விட்டன. அப்போதே பீர்பாலுடன் அவர் நட்புக் கொண்டு விட்டார். அதன் பின்னர் அவர் அக்பரிடம் வேலைக்கு அமர்ந்ததே ஒரு வேடிக்கையான கதை.

அரசவையில் பீர்பாலுடைய விகடப் பேச்சுகளும், நகைச்சுவையான உரையாடல்களும் அக்பரின் உள்ளத்தைப் பெரிதும் கவர்ந்தன. இதன் காரணமாக அக்பரின் அரசவையில் பீர்பால் சிறப்பு மிக்க ஓர் இடத்தைப் பெற்று விட்டார்.

# இருளும் ஒளியும்

ஒரு முறை அக்பர் சக்கரவர்த்தி பீர்பாலை கடுமையாகக் கோபித்துக் கொண்டு, என் முன் நில்லாதே! இனி ஒரு தடவை உன்னைப் பார்த்தேன் என்றால் உனக்கு மரண தண்டனை தான் விதிப்பேன், என்றார்.

அப்படியானால் இந்த ஊரிலேயே இருக்கக் கூடாது என்கிறீர்களா? என்றார் பீர்பால். ஆமாம், உடனே இந்த நாட்டை விட்டு வெளியேறி விட வேண்டும், என்று விட்டுக் கொடுக்காமல் கூறினார் சக்கரவர்த்தி.

சரி, இப்பொழுதே இந்த நாட்டை விட்டுப் போய் விடுகிறேன். நீங்களாகக் கூப்பிடாத வரையில் இந்த மண்ணை மிதிக்க மாட்டேன், என்று சபதம் செய்து விட்டு ஆக்ராவை விட்டு வெளியேறி விட்டார் பீர்பால்.

நாட்கள் பல சென்றன. பீர்பால் இல்லாத குறையை அப்பொழுது தான் உணர்ந்தார் அக்பர் சக்கரவர்த்தி. பீர்பாலின் பிரிவு அவருக்கு மன வருத்தத்தைத் தந்தது. தாம் அவசரப்பட்டு அவரை நாட்டை விட்டுத் துரத்தி விட்டதற்காகப் பெரிதும் வருந்தினார். பீர்பாலைத் தேடி அழைத்து வருமாறு பல திசைகளுக்கும் ஆட்களை அனுப்பினார் அக்பர். ஒரு விதப் பயனும் இல்லை. பீர்பாலைத் தேடிச் சென்றவர்கள் எல்லாம் சோர்ந்த முகத்துடன் திரும்பி வந்தனர்.

கடைசியில் சக்கரவர்த்திக்கு ஒரு யோசனை உதித்தது. பாதி வெளிச்சத்திலும், பாதி இருளிலும் எவன் ஒருவன் வருகிறானோ அவனுக்கு ஆயிரம் மொகராக்கள் பரிசு அளிக்கப்படும் என்ற செய்தியை தண்டோரா மூலம் எல்லா இடங்களுக்கும் தெரியப்படுத்தச் சொன்னார்.

சக்கரவர்த்தியின் உத்தரவு படி, ஒரே சமயத்தில் பாதி ஒளியிலும், பாதி இருளிலும் எவன் வருகிறானோ அவனுக்கு ஆயிரம் மொகராக்கள் பரிசு அளிக்கப்படும் என்ற செய்தி தண்டோரா மூலம் அறிவிக்கப்பட்டது.

இந்தச் செய்தி பீர்பால் தங்கி இருந்த கிராமத்திலும் பரவியது. பீர்பால் அந்தக் கிராமத்தில் மாறுவேடம் அணிந்து கொண்டு தலைமறைவாக வசித்து வந்தார். அவரை ஓர் ஏழை மனிதன் மிக அன்புடன் ஆதரித்து வந்தான்.

தன்னை அன்புடன் ஆதரிக்கும் அந்த ஏழைக்கு எந்த விதத்தில் ஆவது உதவ வேண்டும் என்று தீர்மானித்த பீர்பால் அந்த ஏழையிடம், ஐயா அரசரின் அறிவிப்பைக் கேட்டீர்களா? ஒரே சமயத்தில் பாதி இருளிலும், பாதி ஒளியிலும் எவன் வருகிறானோ அவனுக்கு ஆயிரம் மொகராக்கள் பரிசளிப்பதாகத் தண்டோரா மூலம் அறிவித்து இருக்கிறாரே! என்றார் பீர்பால்.

இது என்ன நடக்கிற காரியமா? ஒரே சமயத்தில் பாதி வெளிச்சத்திலும், பாதி இருளிலும் போக முடியுமா என்ன! நம் அரசர் எப்பொழுதுமே இப்படி தான்! என்றான் அந்த ஏழை மனிதன். நான் சொல்லுகிற படி செய்தால் உமக்கு ஆயிரம் மொகராக்கள் பரிசாகக் கிடைக்கும், என்றார் பீர்பால். ஏழை மனிதன் வியப்புடன் பீர்பாலைப் பார்த்தான்.

நம் வீட்டில் இருக்கும் கயிற்றுக் கட்டிலைத் தூக்கி உமது தலையில் வைத்துக் கொண்டு செல்லும். அதன் மூலம் உம்மீது பாதி இருளும், பாதி ஒளியும் படிந்து இருக்கும். அரசரிடம் சென்று காட்டினால் உமக்கு ஆயிரம் மொகராக்கள் பரிசு அளிப்பார், என்றார் பீர்பால்.

பீர்பால் கூறியவாறு அந்த ஏழை மனிதனும் தன் வீட்டில் இருந்த கட்டிலைத் தூக்கி தலை மேல் வைத்துக் கொண்டு அரண்மனைக்கு வந்தான். 

சக்கரவர்த்தி கட்டிலுடன் வந்தவனைப் பார்த்து என்ன? வென்று கேட்க அவன் தான் ஒரே சமயத்தில் பாதி இருளிலும், பாதி ஒளியிலும் வந்த கதையைக் கூறினான். அவனுடைய செய்கைக்காக அவனைப் பாராட்டிய அக்பர், இந்த மாதிரியான யோசனை பீர்பால் ஒருவருக்கே தோன்றும். இவனைக் கேட்டால் பீர்பாலின் இருப்பிடம் தெரிந்து விடும், என்று நினைத்த அக்பர், உம்முடைய சொந்த யோசனையின் பேரில் தான் இவ்விதம் வந்தீரா? இல்லை, எவராவது உமக்கு இந்த யோசனையை சொல்லிக் கொடுத்தார்களா? என்று கேட்டார்.

ஏழை மனிதன் அக்பர் கேட்ட இந்தக் கேள்வியினால் திடுக்கிட்டுப் போய் விட்டான். அவன் திகைப்பதைக் கண்ட அக்பர், கவலைப்படாதே! எப்படியும் உனக்குச் சொன்ன மாதிரி ஆயிரம் மொகராக்களை கொடுத்து விடுகிறேன். ஆனால் நீ மட்டும் உண்மையைக் கூறி விட வேண்டும், என்றார்.

எங்கள் ஊருக்கு ஒரு பெரியவர் புதிதாக வந்து இருக்கிறார். அவர் என் வீட்டில் தான் தங்கி இருக்கிறார். அவர் தான் எனக்கு இந்த யோசனையை சொல்லிக் கொடுத்தார், என்றான் அந்த ஏழை மனிதன்.

உடனே அக்பர் சக்கரவர்த்தி தாம் கூறியவாறு அந்த ஏழை மனிதனுக்கு ஆயிரம் மொகராக்களைக் கொடுத்து விட்டு அவனுடன் ஓர் ஆளை அனுப்பி பீர்பாலை கூட்டி வரச் செய்தார்.
