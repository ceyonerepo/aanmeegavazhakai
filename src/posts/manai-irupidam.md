---
title : 'மனை இருப்பிடம்'
publishedDate: 'Fri Apr 08 2022 14:00:48 GMT+0530'
category: 'vasthu'
slug: 'manai-irupidam'
thumbnail: '../thumbnails/vaasthu-house.jpg'
thumbnailCredit: 'Photo by Pixabay'
tags: 
    - vasthu
    - aanmeegam
    - nambikai
    - vaasthu saasthiram
keywords:
    - vasthu tamil
    - vasthu in tamil
    - manaiyadi shastra vastu in tamil
---

# வாஸ்து சாஸ்திரம் 
# மனை இருப்பிடம்

## வாஸ்து சாஸ்திரம் என்றால் என்ன?

1. நமது வீடு எப்படி இருக்க வேண்டும்?
2. வீடு காட்டும் முறைகள் 
3. எப்படி வீடு இருந்தால் என்ன பலன் கிடைக்கும்
என்பதை விளக்கி சொல்வது வாஸ்து சாஸ்திரம் ஆகும்.

தனக்கு என்று ஒரு வீடு வேண்டும் என்ற ஆசை எல்லோருக்கும் இருக்கிறது. கூழோ கஞ்சியோ நம் வீட்டில் குடித்தால் அதன் திருப்தியே தனி தான். ஆனால் வீடு கட்டும் போது பலர் முக்கியமான வாஸ்து கூட பார்க்காமல் கட்டி விட்டு பின் அவதிப் படுகிறார்கள். ஆபத்து வந்த பின் காப்பதை விட வரும் முன்னே காப்பது நல்லது அல்லவா!

## வாஸ்து:

வாஸ்து என்பது மனையடி சாஸ்திரத்தின் இதயம் போன்றது ஆகும். வாஸ்து எனப்படும் சமஸ்கிருத சொல்லுக்கு வீடு கட்டக் கூடிய மனை என்று பொருள் படும். இது தனி சாஸ்திரமாக சிலரால் கருதப்படுகிறது. இந்த சாஸ்திரத்தின் நாயகனாக தெய்வாம்சம் பொருந்திய ஒரு பெண் கருதப்படுகிறது.

உயிரற்ற ஜடப் பொருட்களான செங்கல், சிமெண்ட், மணல், கம்பிகள், வெட்டப்பட்ட மரம் போன்ற வஸ்துகளை சரியான விகிதத்தில் கலந்து நிலத்தின் மேல் கட்டிடமாக எழுப்பி உயிரோட்டம் கொடுப்பதே வாஸ்து.

வாஸ்து உண்மையில்லை, வாஸ்து ஏமாற்று வித்தை, வாஸ்து பொய் என்று சமூக அக்கறையோடு பதிவு செய்யப்படும் குற்றச்சாட்டுகளுக்கு என் பதில், 

வாஸ்து என்பது உண்மை, வாஸ்து என்பது அறிவியல், வாஸ்து என்பது மத சம்மந்தப்பட்ட விஷயம் அல்ல. வாஸ்துவில் யந்திரம், மந்திரம், பூஜை, தாயத்து மற்றும் கண் கட்டு வித்தைகளுக்கு வேலை இல்லை.

பாம்பு கடிக்கு விஷமுறிவு மருந்து மற்றொரு பாம்பின் விஷம் தான் என்பது உண்மையாக இருப்பின் வீட்டில் உள்ள பிரச்சனைகளுக்கு தீர்வு அந்த வீட்டிலேயே தான் உள்ளது என்பது தான் நான் கண்டா உண்மை.

## வாஸ்து விதிகள்:

வாஸ்து சாஸ்திரம் என்பது ஒவ்வொரு மனித வாழ்வின் வெற்றிக்கும் அடித்தளமாக விளங்கக் கூடிய ஒன்று ஆகும். வாஸ்து விதிகளின் படி ஒரு வீடோ, தொழிற்சாலையோ அமைக்கப்படும் போது அங்கு இருக்கக் கூடிய அனைவருக்கும் எப்பொழுதும் நல்ல ஆற்றலே இருக்கும். என்றும் மன அமைதி, உடல் நலம், செல்வ செழிப்போடு காணப்படுவர். வாஸ்து என்றால் நான்கு திசை மற்றும்  நான்கு மூலைகள் கொண்டே கருதப்படுகின்றன. அவை வடக்கு, கிழக்கு, தெற்கு, மேற்கு. மேலும் நான்கு மூலைகள் வடகிழக்கு, தென்கிழக்கு, வடமேற்கு, தென்மேற்கு ஆகும். இவற்றை கொண்டு வாஸ்துவில் அடிப்படையாக கருதப்படும் ஆறு மிக முக்கியமான பொதுவான விதிகள்.

## வாஸ்துவின் பொதுவான விதிகள்:

1. மனை மற்றும் அதனுள் கட்டப்படும் கட்டிடம் இரண்டும் சதுரம் அல்லது செவ்வகமாக இருக்க வேண்டும்.

2. வடக்கு  மற்றும் கிழக்கில் அதிக காலி இடம் இருத்தல் அவசியம்.

3. தலை வாசல் என்றுமே உச்சத்தில் தான் அமைக்கப்பட வேண்டும்.

4. தெருத் தாக்கம் (தெருக்  குத்து) இருந்தால் கண்டிப்பாக உச்சமாக தான் இருக்க வேண்டும்.

5. உட்புறம் மற்றும் வெளிப்புறம் போடப்படும் படிக்கட்டுகள் அமைக்கப்படும் முறையை அறிவது அவசியம்.

6. வடகிழக்கு பள்ளமாகவும், கனமில்லாமலும், தென்மேற்கு உயரமாகவும், கனமாகவும் இருத்தல் அவசியம்.

வாஸ்து சாஸ்திரத்தைப் பற்றி இன்னும் பல அறிய நூல்கள் உள்ளன. சிற்ப நகரம், சிற்ப ரத்தினம், மயமதம், காஸ்யபம், ஸர்வார்த்த மனுசாரம், வாஸ்து வித்யா போன்ற நூல்கள் சிலை வடிப்பது, ஆலயம் அமைப்பது, வீடு கட்டுவது போன்ற அறிய கலைகளை பற்றி விவரிக்கின்றன.

# மனை இருப்பிடம்

மனையின் அமைவிடம் என்பது வாஸ்துவில் மிக மிக முக்கியமான அம்சம். மனையில் தென் மேற்கு மூலை வெட்டுப் படாமலும் வட மேற்கு மூலை நீளாமலும் இருத்தல் அவசியம். பெரும்பாலும் அனைத்து முலைகளும் 90 டிகிரி இருப்பது பாதுகாப்பானது.

வீடு கட்டும் போதும் நிலத்தின் எந்த இடத்தில் கட்டிடம் அமைய வேண்டும் என்பதில் மிக மிக கவனமாக இருக்க வேண்டும். கிழக்கிலும் வடக்கிலும் அதிக இடம் விட வேண்டும் என்பது அனைவருக்கும் தெரிந்தது. ஆனால் எவ்வளவு இடம் விட வேண்டும் என்பதில் தான் வாஸ்துவின் வெற்றி இருக்கிறது. காரணம் ஒரு கட்டிடத்தில் காற்றோட்டம், வெப்ப ஓட்டம், நீரோட்டம், காந்த ஓட்டம் ஆகிய நான்கும் சரியான விகிதத்தில் கிடைப்பதற்கு வீட்டை சுற்றி விடப்படும் இடம் தான் முக்கிய காரணமாக அமையும்.

அடுத்ததாக வீட்டின் சுவாச மையமான பிரம்ம மையத்தின் பலன் முழுமையாக கிடைக்க வீட்டை சுற்றி விடப்படும் இடமே மிக முக்கிய காரணியாக அமைகிறது. 

ஒரு கட்டிடத்தின் அகலம் வடக்கு தெற்காக சுமார் 40 அடிகள் இருந்தால் தெற்கில் 3 அடிகளும் வடக்கில் 8 அடிகளும் இடைவெளி இருத்தல் வேண்டும். அப்போது தான் அந்த கட்டிடத்தில் காற்றோட்டம், வெப்ப ஓட்டம், நீரோட்டம், காந்த ஓட்டம் ஆகிய நான்கும் சரியான விகிதத்தில் சுழல்வதற்கும் பிராண சக்தி எனப்படும் காஸ்மிக் எனர்ஜியின் மின்காந்த அலை புள்ளி கட்டிடத்தின் மையத்தில் குவிவதற்கும் போதிய இடம் கிடைக்கும்.

உயிர் வாழ்வதற்கு அடிப்படையாக இருக்கும் காற்றும் சூரிய ஒளியும் நீள்வட்ட சுழல் வடிவில் சுழலும் தன்மை கொண்டது. அவ்வாறு சுழலும் காற்றும் சூரிய ஒளியும் சரியான வேகத்திலும் பாதையிலும் சுழர்வதற்கு போதிய இட வசதி கட்டிடத்தை சுற்றிலும் இருப்பது அவசியம்.

அடுத்து இந்த அளவிற்கு இடம் விடப்பட்டால் மட்டுமே அடுத்த வீட்டில் இருந்து வெளியேறும் எதிர்மறை சக்திகள், மனித உடல் காந்தத்தின் நச்சு கழிவுகள், கட்டிடத்திற்குள் வராமல் தடுக்க முடியும். கட்டிடங்கள் நெருக்கமாக இருக்கும் போது அடுத்த கட்டிடத்தில் இருக்கும் மனிதர்களின் எதிர்மறை எண்ண அலைகள், நச்சு காற்றாலைகள், தேக காந்த வெப்ப அலைகள் ஆகியவை எளிதில் கட்டிடத்தில் நுழைய வாய்ப்பாகி விடும்.

வடக்கு - சுபிட்சம், குபேரன்
தெற்கு என்பது மனிதர்கள். 
மனிதர்கள் தெற்கே தலை வைத்து வடக்கே கால் நீட்டிப் படுத்தால் சுபிட்சத்திற்குக் குறை இருக்காது. காரணம் என்ன? அங்கே நேர் எதிரே குபேரன் சுபராக இருந்து சுப தன்மை பெறுகிறார்கள்.
தெற்கு - எமதர்மராஜன் 
உடல் - சந்திரன் 
ஆன்மா – சூரியன்

இதில் எடுத்துச் செல்லும் ஆற்றல் எமதர்ம ராஜனுக்கு உண்டு. அதாவது தர்மத்தை நிலை நாட்டும் ஆற்றல் எமதர்மனுக்கே இருக்கிறது. மனிதர்களின் உள்ளே ஆன்மா புகுந்து கொண்டு எமதர்மராஜனை அதிகாரியாகக் கொண்டு விளையாடுகிறது.

வடக்கு குபேரன் (சனி பகவான் வீடு). இங்கு ஏன் சனியைக் கொடுத்தார்கள்? நீ குபேரனாக வேண்டுமானால் சனியைப் போல் உழைக்க வேண்டும். அனைத்து நிலைகளிலும் முன்னேற்றமடைய சனி பகவான் போல் உழைக்க, கர்மா நன்றாக இருக்க வேண்டும்.

கிழக்கு = இந்திரன்
மேற்கு = வருணன்

அதாவது மனிதர்களின் இருபுறமும் இந்திரன், வருணன். எதிரே குபேரன். நமக்கு மேலே அக்னி, நமக்கு கீழே நிருதி. மேல் அக்கினி. கீழ் நிருதி. நிருதி என்பது இருண்ட சூழ்நிலை (கர்ப்பத்தில் குழந்தை இருப்பது போல்). இந்த இடத்தில் ஆண் - பெண் உடல் உறவு கொள்ளக் கூடாது. இங்கு அக்கினியில் உடல் உறவு கொள்ளக் கூடாது.

தென்மேற்கு நிருதி - இங்கு கர்பாதானம் கூடாது. இருண்ட சூழ்நிலை. பிறக்கும் குழந்தைகளுக்குத் துன்பம் நேரிடும்.

## வீட்டின் மூலைகள்:

### கன்னி மூலை:
இங்கு சப்த கன்னிகள் வாசம் செய்கிறார்கள். வீட்டின் கன்னி மூலையில் அடைப்பு வேண்டும். திறப்பு கூடாது. தப்பு மற்றும் துவேசம் ஏற்படும்.

### நிருதி (மீனம்):
 அசுத்தம் கூடாது. கழிவு சேமிப்புக்கு குழி (செப்டிக் டேங்க்) அமைக்கக் கூடாது. இங்கு வேம்பு மரம் வைத்தல் குடும்பத்தில் குற்றங்கள் எதுவும் நிகழாது.

### வாயு (தனுசு):
இப்பகுதி திறந்து இருக்க வேண்டும். காற்று வந்து போக வேண்டும். கழிவுகளை வைக்க வேண்டும். தெற்கே தெய்வங்களை வைத்து வடக்கு முகமாக (சிவசக்தி சொரூபம்) வைத்து வணங்கி வந்தால் வாழ்க்கை வளமாக இருக்கும்.

### வடக்கு (மகரம், கும்பம்):
வடக்கும், கிழக்கும் எப்பொழுதும் சுமை இல்லாமல் இருக்க வேண்டும். கனமான பொருட்கள் இப்பகுதியில் இருந்தால் ஏதோ மன வருத்தம் இருந்து கொண்டே இருக்கும்.

### அக்னி மூலை (மிதுனம்):
வாயுவில் இருந்து அக்னி மூலைக்கு காற்று போக வேண்டும். அதே போல கன்னியில் இருந்து ஈசானம் போக வேண்டும். இந்த அக்னி வாயு மூலை இணையும் இடத்தில் படுக்கை அறை இருக்க வேண்டும்.

### வடமேற்கு (வாயு) (தனுசு) முதல் தென்மேற்கு (அக்னி) (மிதுனம்):
இங்கு காற்று தள்ள வேண்டும். காற்றின் வேகம் கர்ப்பா தானத்திற்குத் தள்ள வேண்டும். விந்துவை தவசு என்ற வில் மைதூனம் (மிதுனம்) என்ற ராசிக்குக் கடக்கிறது. தனுசு மன்மதன் அதில் தான் குருவைக் கொடுத்து இருக்கிறார்கள். தனுசு (குரு) முதல் மிதுனம் (புதன்) ஆகிறது. இது கற்பனையல்ல. பொதுவாக தனுசுவிலே தான் பூலோக நட்சத்திரம் மூலம் இருக்கிறது. தனுசுவில் 3 டிகிரி அதாவது பாலியை வீதிகளுக்கான புவி ஈர்ப்பு சக்தி இருக்கிறது. மூலம் இல்லை என்றால் ஒன்றும் இல்லை. இது தனுசு ராசியின் அமைப்பு.

## வாடகை வீடு:

வாடகை வீட்டின் சொந்தக்காரர் எப்படி இருக்கிறார்களோ அப்படித்தான் குடியிருப்பவர்களும் இருப்பார்கள். வீட்டின் உரிமையாளரைப் பொறுத்தே வாடகைக்கு இருப்பவர்களின் நிலையும் இருக்கும். அவர்கள் நன்றாக இருந்தால் இவர்களும் நன்றாக இருப்பார்கள்.

## அப்பார்ட்மெண்ட்:

பூமியில் இருந்து 40 அடிக்கு மேல் ஒரு கட்டிடம் இருந்தால் அங்கு எவ்வித வாஸ்து தோஷமும் இல்லை. 10 அடிக்கு மேல் வாஸ்து தோஷம் இல்லை.

## முடிவுரை:

வாஸ்து சாஸ்திரம் மற்றும் தர்ம சாஸ்திரம், ஜோதிட சாஸ்திரம் இணைந்து தான் ஒரு வீட்டைப் பற்றி முடிவு எடுக்க வேண்டும். வாஸ்து நூல்களில் இடையே சிறிய வித்தியாசங்கள் காணப்படுகின்றது. ஆலய வாஸ்து, கிரக வாஸ்துவை விட சிறிது மாறுபட்டது எந்த ஒரு வீடு சம்பந்தப்பட்ட முடிவு ஆயினும் அவசரம் இல்லாமலும் அடுத்தவர்களுக்கு எந்தப் பாதிப்பும் இல்லாமலும் அமைவது அவசியமாகும். பொதுவாக நல்ல வழிகாட்டி உதவியுடன் மாற்றங்களைச் செய்து கொள்வது நன்மை தரும். மேலும் அடுக்கு மாடி மற்றும் கூட்டுக் குடும்பங்களுக்கு மாற்றங்கள் செய்யும் போது சற்று கவனத்துடன் செய்ய வேண்டும். சில இடங்களில் மாடிப்படி, வாசல் படி எண்ணிக்கை கணக்குப் பார்க்கும் வழக்கம் உண்டு. இவ்வாறு பார்க்க வேண்டாம் என்று சொல்லும் வாஸ்து நிபுணர்களும் உண்டு. ஆக பலன்களை ஆராய்ந்து முடிவு எடுக்க இறைவனைப் பிரார்த்தனை செய்வோம்.
