---
title : 'செடி மறைவில் திருடன்'
publishedDate: 'Tue Mar 08 2022 12:21:33 GMT+0530'
category: 'tenali-raman-stories'
slug: 'chedi-maraivil-thirudan'
thumbnail: '../thumbnails/tenali-raaman.jpg'
thumbnailCredit: 'Photo by Anna Shvets from Pexels.com'
tags: 
    - thenali ramang
    - siru kadhaigal
keywords: 
    - tenali raman
    - tenali raman stories
    - tenali rama krishna
    - tenaliraman
    - tenali ramakrishna stories
    - tenali raman stories in tamil
    - tenali raman short stories
---
 
# தெனாலி ராமன்
தெனாலி ராமன் என்றாலே அனைவருக்கும் நினைவிற்கு வருவது அவருடைய நகைச்சுவை பேச்சு தான். பேச்சு நகைச்சுவையாக இருக்குமே தவிர அவரது சிந்தனையும் திறன்களும் அளவிட முடியாத அளவுக்கு உயர்ந்தவையாகவே இருந்து இருக்கிறது. அவரது எந்த ஒரு நகைச்சுவை  பேச்சும் யாரையும் காயப்பட வைத்தது  இல்லை. மேலும் அவரது ஒவ்வொரு நகைச்சுவை கதைகளிலும் ஏதேனும் ஒரு உள் அர்த்தம் மற்றும் நமது அன்றாட வாழ்க்கையோடு தொடர்புடைய பல சிந்தனைகளை சுமந்தே வந்து உள்ளன. அத்தகைய கதைகளை நாம் படிக்கும் முன்னர் அத்தகைய தெனாலி ராமனை பற்றி இப்பகுதியில் சிறிது தெரிந்து கொள்வோமா!
 
## தெனாலி ராமன் பிறப்பு
 
தெனாலி ராமன் ஆந்திர மாநிலம் கிருஷ்ணா மாவட்டத்தில் கார்லபதி என்கிற கிராமத்தில் ராமையா - லட்சுமி அம்மாள் தம்பதியரின் மகனாக ஏழை பிராமண குடும்பத்தில் பிறந்தவர். தெனாலியில் உள்ள இராமலிங்க சுவாமியின் நினைவாக இராமலிங்கன் என்றே பெயரிடப்பட்டார். இவர் பிறந்து மூன்றாம் நாள் இவருடைய தந்தையார் மரணமடைய குடும்பம் வறுமையில் வாடியது. இவருடைய தாயார் இவரை எடுத்து கொண்டு தெனாலியில் இருந்து அவருடைய சகோதரனுடைய வீட்டுக்கு வந்து சேர்ந்தார். தாய் மாமன் ஆதரவில் தான் இராமலிங்கம் வளர்ந்தார்.
 
## தெனாலியின் கோமாளித் தனங்கள்
 
உரிய பருவத்தில் பள்ளியில் சேர்ந்தாலும் படிப்பில் கவனம் செல்லவில்லை. மற்றவர்களை சிரிக்க வைக்கும் ஆற்றல் அவரிடம் இயற்கையாகவே இருந்தது. அதனால் அகடவிகட கோமாளித் தனங்களில் தான் அவருடைய அறிவும் ஆற்றலும் ஜொலித்தன. கமலா என்கிற பெண்ணை மணந்தார் தெனாலி ராமன்.

## தெனாலி விகடகவி ஆக உயர்ந்தது 
இவர் அரசவை விகடகவியாக உயர்ந்தது குறித்து பல கதைகள் நிலவுகின்றன. 
இவருடைய ஊருக்கு வந்த துறவி ஒருவர் இவருடைய தைரியம் மற்றும் நகைச்சுவை உணர்வில் கவரப்பட்டு காளி தேவியிடம் வரம் பெறத்தக்க மந்திரம் சொல்லி தந்ததாகவும் அதன் அருளால் காளி  தேவியின் தரிசனம் பெற்ற தெனாலி ராமன் அவளையும் தன் நகைச்சுவை அறிவால் சிரிக்க வைத்து அவளாலேயே விகடகவி என்று வாழ்த்தப்பட்டதாகவும் சொல்கிறார்கள்.
அதில் நம்பிக்கை வரப் பெற்ற தெனாலி ராமன் விஜயநகரம் சென்று தன்னுடைய சாமர்த்தியத்தால் அரச தரிசனம் பெற்று தன்  அறிவுக் கூர்மை மற்றும் நகைச்சுவையால் அரசரின் அன்புக்கும் நம்பிக்கைக்கும் பாத்திரமாகி அரசவையில் முக்கிய இடம் பெற்று நல்வாழ்வு வாழ்ந்தார்.
 
## தெனாலி சைவரா? வைணவரா?
இவர் சைவரா வைணவரா என்பதில் சரித்திர ஆசியர்களிடையே குழப்பம் நிலவுகிறது. ஏனென்றால் இவருடைய பெயர் சைவப் பெயர். இவர்  எழுதிய நூலாகிய "பாண்டுரங்க மகாத்மியம்" விஷ்ணுவைப் பற்றியதாக இருப்பதோடு அவருக்கு சமர்ப்பிக்கப்பட்டு உள்ளதாகவும் உள்ளது.
அது தவிர "உத்பாதராதைய சரித்திரம்", "கடிகாச்சல மகாத்மியம்" ஆகிய நூல்களை இயற்றி உள்ளார். முன்னது உத்பாதர் என்கிற துறவியை பற்றியது. பிந்தையது தமிழகம் வேலூருக்கு அருகில் இருக்கும் நரசிம்மர் கோவிலை பற்றியது.
 
## தெனாலியின் பட்டங்கள்
தெனாலி "விகடகவி" என்றும் "குமார பாரதி" என்றும் அனைவராலும் பாராட்டப்பட்டு அன்போடு அழைக்கப்பட்டார்.

## தெனாலியின் சிறப்புகள் 
இந்திய மொழிகளில் இவரைப் பற்றிய பாடக் குறிப்புகள் இல்லாத மொழியே கிடையாது என்னும் அளவுக்குப் பிரபலமானவர்.இவருடைய கதையை "தி அட்வென்சர்ஸ் ஆஃப் தென்னாலி ராமன்" என்கிற பெயரில் கார்ட்டூன் நெட் ஒர்க் தொலைக்காட்சி நிறுவனம் கி.பி.2001இல் படமாக்கப்பது.
 
## தெனாலியின் காலம்
தெனாலி ராமன் வாழ்த்த காலம் கி.பி.1509 முதல் கி.பி.1575 வரை ஆகும். தமிழ் நகைச்சுவை உலகில் மிகவும் புகழ் பெற்ற தெனாலி ராமகிருஷ்ணா என்பவர் விஜயநகரத்தை ஆண்ட கிருஷ்ணா தேவராயரின் அவையை அலங்கரித்த எட்டு அரசவைப்  புலவர்களுள் (அஷ்டதிக்கஜங்கள்) ஒருவர் ஆவார்.
 
இப்படிப்பட்ட புகழ் பெற்ற புலவரான தெனாலி ராமனின் கதைகளில் "உயிர் தப்பியது" என்னும் கதையை இப் பகுதியில் நாம் பார்க்கலாம்.


இப்படிப்பட்ட புகழ் பெற்ற புலவரான தெனாலி ராமனின் கதைகளில் "செடி மறைவில் திருடன்" என்னும் கதையை இப் பகுதியில் நாம் பார்க்கலாம்.


## செடி மறைவில் திருடன்

தெனாலி ராமன் தனது வீட்டு தோட்டத்திற்கு கால் அலம்புவதற்காக சென்றான். தண்ணீர் செம்புடன் தெனாலி ராமன் தனது தோட்டத்திற்கு சென்ற போது அங்கு செடி மறைவில் ஒரு திருடன் ஒளிந்து கொண்டு இருப்பதைக்  கண்டான். அவன் தனியாக அந்த திருடனை பிடிக்க மிகவும் அஞ்சினான். தெனாலி ராமன் உடனே யோசிக்க ஆரம்பித்தான். உடனே அவன் மூளை வேலை செய்ய ஆரம்பித்தது.

திருடன் எங்கு பதுங்கி இருந்தானோ அந்தச் செடியின் அருகே சென்றான். செம்புத் தண்ணீர் முழுவதையும் வாய் கொப்பளித்துத் திருடன் முகத்தில் விழுமாறு துப்பினான். அத்துடன் தெனாலி ராமன் விடவில்லை. மனைவியைக்  செம்புத் தண்ணீர் கொண்டு வருமாறு கூறினான். மனைவியும் அவ்வாறே தெனாலி ராமன் சொன்னதை கேட்டு செம்பு நிறையத் தண்ணீர் கொண்டு வந்து கொடுத்தாள். 

தெனாலி ராமன் அவனது மனைவி கொண்டு வந்த செம்புத் தண்ணீரை வாங்கி கொண்டான். மீண்டும் அந்தச் செம்புத் தண்ணீர் முழுவதையும் பழைய படி வாய் கொப்பளித்துத் திருடன் முகத்தில் துப்பினான். மறுபடியும் தெனாலி ராமன் தனது மனைவியைக் கூப்பிட்டு இன்னொரு முறை செம்பு நிறைய தண்ணீர் கொண்டு வருமாறு பணித்தான். தெனாலி ராமனின் மனைவியும் மூன்றாவது முறையாகச் செம்பில் தண்ணீர் கொண்டு வந்து கொடுத்தாள். 

மூன்றாவதாக அந்த செம்புத் தண்ணீரையும் வாங்கி கொண்டான் தெனாலி ராமன். பிறகு அவன் பழைய படி வாய் கொப்பளித்து திருடன் முகத்தில் விழுமாறு செடியின் மேல் துப்பினான்.

தெனாலி ராமனின் மனைவி என்றும் இல்லாத வழக்கமாக இன்று தனது கணவர் நாலைந்து முறை செம்புத் தண்ணீர் முழுவதையும் கொப்பளித்துத் துப்புவதைக் கண்டதும் அவள் மிகவும் பயந்து போய் விட்டாள். எனவே அவள் தனது பயத்தை வெளிக்காட்ட தெரியாமல் கோபம் வந்து விட்டது தெனாலி ராமனின் மனைவிக்கு. உடனே அவள் தெனாலி ராமனை நோக்கி கோபமாக "இன்று உங்களுக்கு என்ன ஆயிற்று? என்று கேட்டாள்.

உடனே தெனாலி ராமன் ஒரு பதிலும் சொல்லாமல் தன் வாயில் இருந்த தண்ணீரைக் கொப்பளித்து அவள் முகத்தில் துப்பினான். அவ்வளவு தான்! வந்தது வினை! தெனாலி ராமன் என்ன எதிர்பார்த்தானோ அது நடந்து விட்டது.

உடனே தெனாலி ராமனின் மனைவி அங்கு இருந்த அனைவரையும் கூப்பிட கத்தி கூச்சல் இட்டு கூப்பாடு போட்டு விட்டாள். "ஐயையோ! எல்லோரும் இங்கே வாருங்களேன்! என் கணவராகிய தெனாலி ராமன், இவருக்கு பைத்தியம் பிடித்து விட்டது போல் இருக்கிறது. என் முகத்தில் காறித் துப்புகிறார்". என்று "ஓ" என்று கதறி அழுது ஊரைக் கூட்டி விட்டாள் தெனாலி ராமனின் மனைவி.

உடனே அவள் அழுகை சத்தத்தை கேட்டு அங்கு இருந்த அக்கம் பக்கத்தினர் அனைவரும் தெனாலி ராமனின் வீட்டிற்கு வந்து விட்டனர். அனைவரும் தெனாலி ராமனைப் பார்த்து கேள்வி கேட்டுக் கொண்டே இருந்தனர்.

"என்ன ராமா இது? ஏன் இவள் முகத்தில் காறித் துப்பினாய்?" என்று அண்டை அயலார் தெனாலி ராமனைப் பார்த்துக் கேட்டனர். "ஐயா, இவளைத் திருமணம் செய்து கொண்டு பத்து ஆண்டுகளுக்கு மேல் ஆகிறது. இவள் விரும்புகிற ஆடை அணிகளை எல்லாம் வாங்கி கொடுத்து இவள் மகிழ்ச்சியுடன் இருப்பதற்காக எவ்வளவோ பாடு படுகிறேன். அவ்வாறு இருக்கும் போது ஒரே ஒரு தடவை இவள் முகத்தில் காறித் துப்பியதற்கு இவ்வளவு பெரிய கூச்சல் போட்டு ஊரையே கூடி விட்டாளே! என்று அங்கு வந்தவர்களிடம் தெனாலி ராமன் முறையிட்டான்.

அத்தோடு தெனாலி ராமன் விட வில்லை. மேலும் அங்கு இருந்தவர்களிடம் முறையிட தொடங்கினான். இதோ பாருங்கள் இந்தச் செடியின் மறைவில் ஒருவர் உட்கார்ந்து இருக்கிறார். அவரை எனக்கு முன் பின் தெரியாது. அவர் முகத்தில் எத்தனையோ முறை செம்புத் தண்ணீரை வாய் கொப்பளித்து உமிழ்ந்தேன். அவர் வாயை திறக்கவே இல்லை. அவர் வாயை திறந்து இருக்க வேண்டுமே! இன்னும் எத்தனை தடவை துப்பினாலும் பேசாமல் இருக்க கூடிய அவர் பொறுமைசாலியா, ஒரு தடவை துப்பியதற்கு இந்த பாடு படுத்துகிறாள் எனது மனைவி இவள் பொறுமைசாலியா? என்று தெனாலி ராமன் கூறினான்.

தெனாலி ராமன் சொல்லிக் கொண்டு இருக்கும் போதே எல்லோரும் செடி மறைவில் பதுங்கி இருந்த திருடனை பார்த்து விட்டனர். அந்த திருடனை எல்லாரும் சேர்ந்து வெளியே இழுத்து நையப் புடைத்து அனுப்பினர். 

பிறகு தெனாலி ராமனின் மனைவி தனது கணவன் ஏன் இவ்வாறு செய்தார் என்பதை புரிந்து கொண்டு தெனாலி ராமனிடம் மன்னிப்பு கேட்டாள். தெனாலி ராமன் அதெல்லாம் ஒன்றும் தேவை இல்லை என்று தனது மனைவியை ஆறுதல் படுத்தினான்.

## கதையின் நீதி:

ஒரு இக்கட்டான சூழ்நிலை வரும் போது தனது மூளையை நன்கு திறமையோடு பயன்படுத்தி அந்த சிக்கலை பதட்டம் இன்றி தீர்க்க வேண்டும் என்பதை தான் இந்த கதை நமக்கு உணர்த்துகிறது. எந்த சூழ்நிலையானாலும் நன்கு திறமையோடு முடிவு எடுக்க தெனாலி ராமனை மிஞ்சுவதற்கு யாரும் இல்லை.
