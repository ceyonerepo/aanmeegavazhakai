import React from 'react'
import { Link } from 'gatsby'
import '../styles/navbar.scss'
import Divider from '@mui/material/Divider';
import {Accordion, AccordionDetails, AccordionSummary} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';


function Navbar(props) {
  return (
    <div className='navbar'>
      <div className='logo-wrapper'>
        <img src="/logo.png" className="siteLogo" alt="site logo"></img>
      </div>
      <div className='nav-list-wrapper'>
        <div className="nav-list-title">விருப்பமான​ தலைப்பை தேர்ந்து எடுக்கவும்</div>
        <Divider />
        <ul>
          <Link to="/">
            <li className="navItem">முகப்பு</li>
            <Divider component="li"/>
          </Link>
          <Link to="/category/life">
            <li className="navItem">ஆன்மிகம்</li>
            <Divider component="li"/>
          </Link>
          <Accordion className='nav-list-accordion'>
            <AccordionSummary className='accordion-title'
              expandIcon={<ExpandMoreIcon />}
            >தமிழ் கதைகள்</AccordionSummary>
            <AccordionDetails>
              <Link to="/category/tenali-raman-stories">
                <li className="navItem">தெனாலி ராமன் கதைகள்</li>
                <Divider component="li" />
              </Link>
              <Link to="/category/tenali-raman-stories">
                <li className="navItem">நீதிக் கதைகள்</li>
                <Divider component="li" />
              </Link>
              <Link to="/category/tenali-raman-stories">
                <li className="navItem">பஞ்சதந்திர கதைகள்</li>
              </Link>
            </AccordionDetails>
          </Accordion>
          <Divider component="li"/>
          <Link to="/category/kanavu-palangal">
            <li className="navItem">கனவு பலன்கள்</li>
            <Divider component="li" />
          </Link>
        </ul>
      </div>
    </div>
  )
}

export default Navbar
