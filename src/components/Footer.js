import React from 'react';
import {Link} from 'gatsby';
import '../styles/Footer.scss'

function Footer() {
    return (
        <footer>
            <div className="footer-container">
               <div className="copyright-content">
                   Copyright © 2022 tamil360.net
               </div>
               <Link to="/privacy-policy">
                   <div>Privacy Policy</div>
               </Link>
            </div>
        </footer>
    )
}


export default Footer;