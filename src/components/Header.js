import React from 'react'
import {useState} from 'react';
import AppBar from '@mui/material/AppBar';
import '../styles/header.scss';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import Drawer from '@mui/material/Drawer';
import Navbar from './navbar';
import { Link } from 'gatsby';

export default function Header() {
    const [open, setDrawerOpen] = useState(false);

    const handleClick = () => {
        setDrawerOpen(!open);
    }

    const menuIcon = <IconButton className='header-icon' onClick={handleClick}><MenuIcon sx={{ color: 'white' }}/></IconButton>
    const closeIcon = <IconButton className='header-icon' onClick={handleClick}><CloseRoundedIcon /></IconButton>

    return (
        <div className='header-container'>
            <AppBar position="static" className='page-header'>
                <div className='logo-container'>
                    {open ? closeIcon : menuIcon}
                    <Link to="/" className='site-name'>
                        TAMIL 360
                    </Link>
                </div>
                <Drawer
                    anchor='top'
                    open={open}
                    onClose={handleClick}
                >
                    <Navbar />       
                </Drawer>
            </AppBar>
        </div>
    )
}
