import React from 'react'
import '../styles/sidebar.scss'

export default function Sidebar( {children}) {
    return (
        <div className='sidebar-container'>
            {children}
        </div>
    )
}
