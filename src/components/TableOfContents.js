import React from 'react'
import {Link} from 'gatsby';
import '../styles/tableOfContents.scss'
import Paper from '@mui/material/Paper';

export default function TableOfContents({items}) {
    return (
        <Paper elevation={3}>
            <div className='table-of-contents-wrapper'>
                <div className='title'>பொருளடக்கம்</div>
                {items && items.map(item => {
                    return <Link key={item.title} to={item.url}>{item.title}</Link>
                })}
            </div>
        </Paper>
    )
}
