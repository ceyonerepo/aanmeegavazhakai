import React from 'react';
import { GatsbyImage } from "gatsby-plugin-image";
import '../styles/Hero.scss'
import {Link} from 'gatsby';
import { StaticImage } from "gatsby-plugin-image"

export default function Hero ({title, image, publishedDate, category, thumbnailCredit}) {

  function getThumbnailCredit() {
    var tCredit = 'Photo by Vincent M.A. Janssen from Pexels';

    if(thumbnailCredit) {
        return thumbnailCredit
    }

    return tCredit;
  }

  function getThumbnailImage() {
    if(image) {
      return (<GatsbyImage
                  image={image.childImageSharp.gatsbyImageData}
                  imgClassName="card-img"
                  className="img-wrapper"
                  alt={title}
                ></GatsbyImage>)
    }

    return (
      <StaticImage src='../../static/fallback-image.jpg' alt={title} />
    );
  }
  return (
    <>
      <div className='hero-wrapper'>
        {getThumbnailImage()}
        <div className='post-content-wrapper'>
          <h1 className='post-title'>
            {title}
          </h1>
          <div className='post-subtitle'>
            <span className='published-date'>{publishedDate}</span>
            {category && <span className='post-category'>{category}</span>}
          </div>
        </div>
      </div>
      <div className='thumbnail-credit-wrapper'>{getThumbnailCredit()}</div>
    </>
  )
}