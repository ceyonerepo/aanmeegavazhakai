import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';
import Layout from '../Layouts/commonLayout';
import ImageCard from './imageCard';
import '../styles/home.scss';
import Divider from '@mui/material/Divider';
import SEO from './SEO';
import {getFormattedDate} from '../../utils'

export default function Home({data}) {
    var { aanmeegam, kadhaigal, kanavuPalangal } = useStaticQuery(
        graphql`
          {
            aanmeegam: allMdx(
              limit: 8
              sort: {fields: frontmatter___publishedDate, order: DESC}
              filter: {frontmatter: {category: {eq: "life"}}}
            ) {
              edges {
                node {
                  frontmatter {
                    title
                    slug
                    category
                    publishedDate
                    thumbnail {
                      childImageSharp {
                        gatsbyImageData(layout: FULL_WIDTH)
                      }
                    }
                  }
                  timeToRead
                }
              }
              totalCount
            },
            kanavuPalangal : allMdx(
              limit: 8
              sort: {fields: frontmatter___publishedDate, order: DESC}
              filter: {frontmatter: {category: {eq: "kanavu-palangal"}}}
            ) {
              edges {
                node {
                  frontmatter {
                    title
                    slug
                    category
                    publishedDate
                    thumbnail {
                      childImageSharp {
                        gatsbyImageData(layout: FULL_WIDTH)
                      }
                    }
                  }
                  timeToRead
                }
              }
              totalCount
            },
            kadhaigal : allMdx(
              limit: 8
              sort: {fields: frontmatter___publishedDate, order: DESC}
              filter: {frontmatter: {category: {eq: "tenali-raman-stories"}}}
            ) {
              edges {
                node {
                  frontmatter {
                    title
                    slug
                    category
                    publishedDate
                    thumbnail {
                      childImageSharp {
                        gatsbyImageData(layout: FULL_WIDTH)
                      }
                    }
                  }
                  timeToRead
                }
              }
              totalCount
            },
          }
        `
      )
    return (
        <Layout>
          <SEO description='home page' image='test' title='Home page'/>
            <div className='post-list-container'>
              <span className='post-category-title'>ஆன்மிகம்</span>
              <ImageCard posts={aanmeegam.edges} />
            </div>
            <Divider />
            <div className='post-list-container'>
              <span className='post-category-title'>தெனாலி ராமன் கதைகள்</span>
              <ImageCard posts={kadhaigal.edges} />
            </div>
            <Divider />
            <div className='post-list-container'>
              <span className='post-category-title'>கனவு பலன்கள்</span>
              <ImageCard posts={kanavuPalangal.edges} />
            </div>
            
        </Layout>
    )
}