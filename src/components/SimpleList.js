import React from 'react';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { Link } from 'gatsby';
import { getSrc } from 'gatsby-plugin-image';
import '../styles/simpleList.scss'
import { getFormattedDate } from '../../utils';

export default function SimpleList({posts, heading}) {
    return (
        <div className='simple-list-wrapper'>
            <div className='simple-list-heading'>{posts && heading}</div>
                <div className='posts-list'>
                    {posts && posts.map(post => {
                        var {title, thumbnail, category, slug, publishedDate} = post.node.frontmatter;
                        var {timeToRead} = post.node
                        return (
                            <div key={slug}>
                                <Link to={'/' + category + '/' + slug}>
                                    <Card elevation={3} sx={{margin : 1}}>
                                        <div className='simple-list'>
                                            <CardMedia
                                                component="img"
                                                image={thumbnail ? getSrc(thumbnail) : '/fallback-image.jpg'}
                                                sx={{maxWidth: 150, borderRadius: 1}}
                                            ></CardMedia>
                                            <CardContent
                                                sx={{padding: 0, paddingLeft: 2}}
                                            >
                                                <div className='post-title'>{title}</div>
                                                <div className='post-subtitle'>{getFormattedDate(publishedDate)}</div>
                                                <div className='post-subtitle'>{timeToRead + ' Mins Reading'}</div>
                                            </CardContent>
                                        </div>
                                    </Card>
                                </Link>
                            </div>
                        )
                    })}
                </div>
        </div>
    )
}