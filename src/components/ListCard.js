import React from 'react'
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardHeader from '@mui/material/CardHeader';
import '../styles/listCard.scss'
import { Link } from 'gatsby';

export default function ListCard({title, publishedDate, imageURL, category, slug}) {
    return (
        <Link to={'/' + category + '/' + slug} className='category-card-link'>
            <Card elevation={4}>
                <CardHeader
                    className="list-card-header"
                    title={title}
                    subheader={publishedDate}
                    sx={{fontFamily: 'inherit'}}
                />
                <CardMedia
                    component="img"
                    image={imageURL ? imageURL : '/fallback-image.jpg'}
                    alt={title}
                />
            </Card>
        </Link>
    )
}
