import React from 'react';
import { Card, CardContent, CardMedia} from '@mui/material';
import { Link } from 'gatsby';
import { getSrc } from 'gatsby-plugin-image';
import '../styles/imagecard.scss';
import { getFormattedDate } from '../../utils';

export default function ImageCard ({posts}) {
  return (
      <div className='image-card-wrapper'>
        {
            posts && posts.map(post => {
                var {title, thumbnail, category, slug, publishedDate} = post.node.frontmatter;
                var {timeToRead} = post.node;
                return (
                    <div className='image-card' key={slug}>
                        <Link to={category + '/' + slug}>
                            <Card elevation={4} sx={{margin : 1, borderRadius: 4, padding: 1}}>
                                <CardMedia
                                    component="img"
                                    image={thumbnail ? getSrc(thumbnail) : '/fallback-image.jpg'}
                                    sx={{maxWidth: 450}}
                                ></CardMedia>
                                <CardContent>
                                    <div className='post-subtitle-container' style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}>
                                        <div className='post-publishedDate'>{getFormattedDate(publishedDate)}</div>
                                        <div className='post-readTime'>{timeToRead + ' Mins Reading'}</div>
                                    </div>
                                    <div className='post-title'>{title}</div>
                                </CardContent>
                            </Card>
                        </Link>
                    </div>
                )
            })
        }
      </div>
    )
}