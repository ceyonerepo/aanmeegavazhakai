const path = require("path")

exports.onCreateWebpackConfig = ({ stage, loaders, actions }) => {
  if (stage === "build-html") {
      actions.setWebpackConfig({
          module: {
              rules: [
                  {
                      test: /bad-module/,
                      use: loaders.null(),
                  },
              ],
          },
      })
  }
}

exports.createPages = async ({ graphql, actions, reporter }) => {
  // Destructure the createPage function from the actions object
  const { createPage } = actions
  const result = await graphql(`
    query {
      allMdx {
        edges {
          node {
           frontmatter {
            slug
            category
           }
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild('🚨  ERROR: Loading "createPages" query')
  }
  // Create blog post pages.
  const posts = result.data.allMdx.edges
  // you'll call `createPage` for each result
  var categories = [];
  posts.forEach(({ node }, index) => {
    categories = categories.concat(node.frontmatter.category);
  })

  var solidCategories = [...new Set(categories)];

  solidCategories.forEach(category => {
    createPage({
      path: '/category/' + category,
      component: path.resolve(`./src/templates/category-page.js`),
      context: {
        category: category,
      },
    })
  });

  posts.forEach(({ node }, index) => {
    createPage({
      path: `/${node.frontmatter.category}/${node.frontmatter.slug}`,
      component: path.resolve(`./src/templates/post-page-template.js`),
      context: { slug: node.frontmatter.slug, category: node.frontmatter.category },
    })
  })
}