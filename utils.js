export function getFormattedDate(date) {
    var currentDate = new Date(date);
    return currentDate.getDate() + ' . ' + currentDate.getMonth() + 1 + ' . ' + currentDate.getFullYear();
}