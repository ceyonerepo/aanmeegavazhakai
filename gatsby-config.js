require("dotenv").config({
    path: `.env.${process.env.NODE_ENV}`,
})

module.exports = {
    siteMetadata: {
        title: 'Tamil 360',
        siteUrl: `https://tamil360.net/`,
        description: `Tamil blog which contains the articles about tamil culture and tamil tradition, life of tamilians.`,
        author: `Mano Subburaj`,
        baseURL: `https://tamil360.net/`,
        logo: '/logo.png',
    },
    plugins: [
        {
            resolve: `gatsby-plugin-google-analytics`,
            options: {
              // The property ID; the tracking code won't be generated without it
              trackingId: process.env.GOOGLE_ANALYTICS_UA_KEY,
              // Defines where to place the tracking script - `true` in the head and `false` in the body
              head: true,
              // Setting this parameter is optional
              anonymize: true,
              // Setting this parameter is also optional
              respectDNT: true,
            },
          },
        `gatsby-plugin-sass`,
        `gatsby-plugin-sharp`, 
        `gatsby-transformer-sharp`,
        `gatsby-plugin-image`,
        `gatsby-transformer-remark`,
        `gatsby-plugin-sitemap`,
        {
          resolve : 'gatsby-plugin-sitemap',
          options : {
            output : '/sitemap'
          }
        },
        {
            resolve: `gatsby-plugin-mdx`,
            options: {
                extensions: [`.mdx`, `.md`],
                gatsbyRemarkPlugins: [
                    {
                        resolve: `gatsby-remark-autolink-headers`,
                        options: {
                            offsetY: `100`,
                            icon: `<svg aria-hidden="true" height="20" version="1.1" viewBox="0 0 16 16" width="20"><path fill-rule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg>`,
                            className: `heading-anchor`,
                            enableCustomId : true,
                            maintainCase: true,
                            removeAccents: true,
                            isIconAfterHeader: true,
                            elements: [`h2`, `h4`],
                        },
                    },
                    {
                        resolve: `gatsby-remark-images`,
                        options: {
                            maxWidth: 900,
                        },
                    },
                ],
            },
        },
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "posts",
                path: `${__dirname}/src/posts/`,
            },
        },
        {
            resolve: "gatsby-source-filesystem",
            options: {
                name: "pages",
                path: `${__dirname}/src/pages/`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
              name: `thumbnails`,
              path: `${__dirname}/src/thumbnails/`,
            },
        },
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
              name: `gatsby-starter-default`,
              short_name: `starter`,
              start_url: `/`,
              background_color: `#064c31`,
              theme_color: `#064c31`,
              display: `minimal-ui`,
              icon: `static/logo.png`, // This path is relative to the root of the site.
            },
          },
          {
            resolve: `gatsby-plugin-feed`,
            options: {
              query: `
                {
                  site {
                    siteMetadata {
                      title
                      description
                      siteUrl
                      site_url: siteUrl
                    }
                  }
                }
              `,
              feeds: [
                {
                  serialize: ({ query: { site, allMarkdownRemark } }) => {
                    return allMarkdownRemark.edges.map(edge => {
                      return Object.assign({}, edge.node.frontmatter, {
                        date: edge.node.frontmatter.publishedDate,
                        url: site.siteMetadata.siteUrl + '/' + edge.node.frontmatter.category + '/' + edge.node.frontmatter.slug,
                        guid: site.siteMetadata.siteUrl + '/' + edge.node.frontmatter.category + '/' + edge.node.frontmatter.slug,
                        enclosure: edge.node.frontmatter.thumbnail && {
                          url: site.siteMetadata.siteUrl + edge.node.frontmatter.thumbnail.childImageSharp.fluid.src,
                      },
                        custom_elements: [
                            { 
                              "content:encoded": edge.node.html, 
                            }
                        ],
                      })
                    })
                  },
                  query: `
                    {
                      allMarkdownRemark(
                        sort: { order: DESC, fields: [frontmatter___publishedDate] },
                      ) {
                        edges {
                          node {
                            html
                            frontmatter {
                              title
                              publishedDate
                              category
                              slug
                              thumbnail {
                                childImageSharp {
                                  fluid {
                                    src
                                  }
                                }
                              }
                              thumbnailCredit                              
                            }
                          }
                        }
                      }
                    }
                  `,
                  output: "/rss.xml",
                  title: "tamil360.net RSS Feed",
                },
              ],
            },
          },
    ]
}